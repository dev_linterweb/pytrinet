#ifndef _PY_DYNAMIC_ENGINE_H_
#define _PY_DYNAMIC_ENGINE_H_

#include <Python.h>
#include "dynengine.h"

typedef struct {
    PyObject_HEAD
    lsDynEngine * engine;
    PyObject * pyEngineArray;
    std::thread loadThread;
} PyDynamicEngine;

extern PyTypeObject DynamicEngineType;

#endif