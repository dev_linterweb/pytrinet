import React from 'react';

class BackendSelect extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      backend:false
    }
  }

  componentDidMount() {
    this.setState({backend:this.props.api.registerBackendChange(this)})
  }

  componentWillUnmount() {
    this.props.api.unregisterBackendChange(this)
  }

  onBackendChange( backend ) {
    this.setState({backend:backend})
  }

  render() {
    const api = this.props.api
    return this.state.backend ? <select onChange={ e => api.setBackend( e.target.value ) }>
      {this.props.api.backends.map(
        backend => <option value={backend} >{backend}</option>
      )}
    </select> : ''
  }
}

export default BackendSelect;
