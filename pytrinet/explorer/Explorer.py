from io import StringIO
from threading import Thread
import time
import traceback
from pytrinet.Configurable import Configurable
from pytrinet.wordStreamer.Tokenizer import Tokenizer

class Explorer(Configurable):

    started:bool = False
    stopRequest:bool = False
    thread:Thread = None
    count:int = 0
    project = None
    autoSave:str = None
    maxRetry:int = 3
    useTimestamps:bool = False

    parameters = (
        ('autoSave','100'),
    )

    def __init__( self, project, config, id ):
        super().__init__( config )
        self.project = project
        self.id = id

    def log(self,text) : 
        self.project.log('explorer'+('' if not self.id else str(self.id)),text)

    def explore(self):
        self.count = 0
        self.retry = 0
        while not self.stopRequest and self.retry < self.maxRetry :
            try :
                while not self.stopRequest : 
                    if self.doExplore() :
                        self.count += 1
                        if self.autoSave and int(self.autoSave) and self.count >= int(self.autoSave) :
                            self.count = 0
                            self.project.save()
                    else :
                        time.sleep(10)
                    self.retry = 0
            except Tokenizer.BufferOverflowException :
                self.log("Buffer overflow exception while exploring. Dropping this one and continuing.")
                pass
            except Exception :
                self.retry += 1
                self.log(traceback.format_exc())

        self.started = False # When dying, the exploring thread sets the flag started to False
        self.stopRequest = False

    def doExplore(self) -> bool: # To be implemented by subclass: do one exploring step
        return False

    def start(self):
        if ( self.started ) : return
        self.log("Starting explorer ...")
        self.started = True
        self.thread = Thread(target=Explorer.explore, args=(self,))
        self.thread.start()

    def stop(self,sync=False):
        if not self.started : return
        self.stopRequest = True
        if sync : self.thread.join()

    # Sets the state of the explorer at the beginning of parsing all
    def reset(self):
        pass

    def push( self, url, body, meta:dict = None, lang:str = None, metaWords:tuple=None, stamp=0 ):
        try :
            if isinstance( body, str ) : body = StringIO(body) # not streaming
            self.log("Passive push of {}".format(url))
            self.project.pushDocument( url, body, meta, lang, metaWords, self, stamp )

            self.count += 1
            if self.autoSave and int(self.autoSave) and self.count >= int(self.autoSave) :
                self.count = 0
                self.project.save()
            
        except Tokenizer.BufferOverflowException :
            self.log("Buffer overflow exception while exploring. Dropping this one and continuing.")
        except Exception :
            self.log(traceback.format_exc())
