#ifndef __LIST_H_
#define __LIST_H_

/** \file list.h Classes pour manipuler les vecteurs (listes) de mots et articles.
 Definition des classes servant a manipuler les vecteurs (listes) de mots et articles.
 Il existe deux types de listes : 
 - compactList :  de taille minimale en m&eacute;moire mais difficilement manipulable et tres statiques,
 - listIndex : ce sont les "copies de travail" des listCompact, sur lesquelles de nombreuses
   operations sont disponibles.
 \sa \ref listvect
*/

#include "lintersearch.h"
#include <iostream>

/** Valeurs pour les conversions de codes de scores vers scores r&eacute;els
 \sa \ref scores
*/
extern intIndex scoreMapNull[], scoreMapVocSpe[], scoreMapQuery[], scoreMapArticleVocSpe[];


template <class Tpointer> class compactList;

template <class Tpointer> class compactIterator;

class listIndex {

public:
  listIndex(); ///< Cr&eacute;ation d'une liste de taille maximale 0
  listIndex( long _maxLength ); ///< Cr&eacute;ation d'une liste de taille maximale <_maxLength>
  listIndex( const intIndex *s ); /**< Récupération d'une liste à partir de données sérialisées 
                                   par la méthode listIndex::serialize **/
  ~listIndex(); ///< Destructeur - ne d&eacute;salloue pas la liste ! Ne pas oublier d'utiliser free() avant.
  void free(); ///< Lib&egrave;re la m&eacute;moire utilis&eacute;e par la liste.
  void clear() { size = 0; } ///< Vide la liste tout en conservant la m&ecirc;me taille maximale.
  void grow( long _maxLength ); ///< Make the maxLength of this list at least equal to _maxLength
  intIndex appendable() { return maxLength - size; }
   ///< Indicateur : peut-on rajouter un &eacute;l&eacute;ment &agrave; cette liste ? et combien
  intIndex* serialize() const;
   ///< Alloue et remplit une chaine de données séralisant cette listIndex (valeurs sans scores)
  void dup( listIndex l );
   ///< Alloue cette liste et la remplit avec les donn&eacute;es de <l> (clonage)
  listIndex dup() { listIndex rt; rt.dup(*this); return rt; }
   ///< Alloue une nouvelle liste, remplie des éléments de celle-ci (clonage)
  long length() const { return size; }
   ///< Longueur de cette liste
  void append( intIndex element, intIndex score );
   ///< Ajoute l'&eacute;l&eacute;ment <element> de score <score> &agrave; la fin de cette liste (pas de test de doublons)
  void append( listIndex l );
  ///< Ajoute les elements de la liste l a la fin de cette liste (pas de test de doublons)
  void appendWithAverageCut( intIndex element, intIndex &maxScore );
   ///< Ajoute l'&eacute;l&eacute;ment element à la fin de la liste sans test de doublon, avec suppression aléatoire de points si débordement. Les scores sont utilisés en interne pour gérer la répartition homogène des éléments.
  void push( intIndex elt, intIndex score );
   /**< Ajoute l'&eacute;l&eacute;ment <elt> de score <score> &agrave; cette liste si il n'existe
     pas encore, sinon ajoute <score> au score d&eacute;j&agrave; attribu&eacute; &agrave; <elt> */
  void cut( intIndex sz ) { if ( size > sz ) size = sz; }
   ///< Ne retient que les <sz> premiers &eacute;l&eacute;ments de cette liste. La liste devient de taille <sz>.
  void averageCut( intIndex sz );
   /**< Ne retient que <sz> &eacute;l&eacute;ments de cette liste. La liste devient de taille <sz>. Les &eacute;l&eacute;ments
     retenus sont s&eacute;lectionn&eacute;s de mani&egrave;re homog&egrave;ne le long de la liste.
   */
  void _switch( intIndex i1, intIndex i2 );
  void cutUnderScore( intIndex score );
   ///< Retire tous les éléments de score strictement inférieur à <score>
  void cutZero();
   /**< Coupe la liste au niveau du premier &eacute;l&eacute;ment de score 0. En g&eacute;n&eacute;ral utilis&eacute; sur une liste
     tri&eacute;e par score d&eacute;croissant ce qui revient &agrave; retirer les &eacute;l&eacute;ments de score 0.
   */
  void removeZero();
   /** Retire les &eacute;l&eacute;ments de score 0 sur une liste qui n'a pas &eacute;t&eacute; pr&eacute;alablement tri&eacute;e par ordre 
     d&eacute;croissant.
    */
  void setElement( intIndex i, intIndex c );
   /**< Ins&egrave;re l'&eacute;l&eacute;ment <c> en position <i> dans la liste */
  void setCount( intIndex i, intIndex c );
   /**< Attribue le score <c> &agrave; l'&eacute;l&eacute;ment en position <i> dans la liste */
  void sortedSetCount( intIndex i, intIndex c );
   /**< Je euh ... je sais plus &agrave; quoi ça sert ... */
   
  intIndex count( intIndex i ) const;
   /**< Retourne le score de l'&eacute;l&eacute;ment en position <i> dans la liste */
  intIndex element( intIndex i ) const;
   /**< Retourne l'&eacute;l&eacute;ment situ&eacute; en position <i> dans la liste */
  intIndex findDownScore( intIndex score );
   /**< Sur une liste tri&eacute;e par scores d&eacute;croissant : retourne la position du premier score inf&eacute;rieur &agrave;
    <score> */
  void insertSortedCounts( intIndex elt, intIndex score );
   /**< Dans une liste tr&eacute;e par scores d&eacute;croissant : ins&egrave;re l'&eacute;l&eacute;ment <elt> de score <score> en conservant
    l'ordre d&eacute;croissant sur les scores. Si la liste est pleine, le dernier &eacute;l&eacute;ment de 
    la liste est &eacute;ject&eacute; */
  void insertSortedCountsInv( intIndex elt, intIndex score );
  /**< Pareil que insertSortedCounts, avec scores croissants **/
  void sortCounts();
   ///< Trie la liste par scores d&eacute;croissants
  void sortCountsInv();
   ///< Trie la liste par scores croissants
  void sortElements();
   ///< Trie la liste par num&eacute;ro d'&eacute;l&eacute;ments croissants
  bool  operator == ( const listIndex &l ) const;
   /**< Op&eacute;rateur de test d'&eacute;galit&eacute; entre deux listes. Deux listes sont &eacute;gales si elles contiennent 
    les m&ecirc;mes &eacute;l&eacute;ments dans le m&ecirc;me ordre, ind&eacute;pendemment de leurs scores. */
  long contains( const listIndex &l ) const;
   /**< Test : cette liste contient-elle la lsite <l>. Une liste en contient une autre si les &eacute;l&eacute;ments
    de la seconde apparaissent dans la premi&egrave;re dans le m&ecirc;me ordre. Ind&eacute;pendemment de leurs scores. Si oui, renvoit la position de la dernière occurence à 
partir de la fin de la chaine contenante. (sert au dmoz à savoir si ça se trouve en fin de titre) */
  bool contains( intIndex elt ) const;
  /**< Tests whether [elt] is an element of this list **/
  void removeElement( intIndex elt );
   /**< Retire l'&eacute;l&eacute;ment <elt> */
  template <class Tpointer>
  void fromCompactScoreMap( compactIterator<Tpointer> l, intIndex *scoreMap = NULL );
   /**< Remplir cette liste avec le contenu de la compactList <l> tout en convertissant les codes
    de scores avec le mapping de <scoreMap>. Cette liste doit avoir &eacute;t&eacute; allou&eacute;e pr&eacute;alablement 
    avec une longueur suffisante. \sa \ref scores */
  template <class Tpointer>
  void fromCompactFusion( compactList<Tpointer> l, listIndex mod );
   /**< Remplir cette liste avec le contenu de la compactList <l> tout en 
     - rajoutant les &eacute;l&eacute;ments de <mod> de score diff&eacute;rent de intIndexInvalid. Si l'&eacute;l&eacute;ment existe d&eacute;j&agrave; dans <l>, 
       le plus grand des deux scores lui est attribu&eacute;.
     - supprimant les &eacute;l&eacute;ments qui apparaissent dans <mod> avec un score intIndexInvalid.
     <mod> doit &ecirc;tre tri&eacute;e par &eacute;l&eacute;ments croissants.
    */
  template <class Tpointer>
  bool fromCompactRemove( compactList<Tpointer> l, listIndex mod );
   /**< Identique à fromCompactFusion, sauf que tous les éléments de mod sont présupposés à intIndexInvalid.
        Renvoit true si aucun élément n'a réellement été retiré **/
  void reunion( listIndex l1, listIndex l2 );
    /**< Alloue cette liste et la remplit avec l'union pond&eacute;r&eacute;e de l1 et l2. */
  void reunionMax( listIndex l1, listIndex l2 );
    /**< Alloue cette liste et la remplit avec l'union de l1 et l2, les éléments communs à l1 et l2
         reçoivent le plus grand des deux scores. */
  void diff( listIndex l );
   /* Retire de cette liste les éléments présents dans l (différence) */
  void intersect( listIndex l );
    /**< Intersecte cette liste avec <l>. Les &eacute;l&eacute;ments conservent le score qu'ils avaient dans cette liste. */
  void collapse();
    /**< Sur une liste tri&eacute;e par &eacute;l&eacute;ments croissants : supprime les doublons. */
  void     checkOverflow( intIndex sz );
   ///< Fonction de debuggage

  void debug( const char *text ) const;
   ///< Affiche la liste sur la sortie standard

  void load( std::istream & in );
   ///< Charge une listIndex depuis un flux
  void save( std::ostream & out );
   ///< Sauvegarde une listIndex dans un flux

  
  intIndex maxLength; ///< Taille maximale de cette liste
  intIndex size; ///< Taille actuelle de cette liste
protected:
  intIndex *list; ///< La liste en elle-m&ecirc;me : un intIndex pour l'&eacute;l&eacute;ment, un intIndex pour le score.

  void alloc( long _maxLength );
  intIndex getSortedLength();
  void setSortedLength(intIndex i);
};


inline  void listIndex::append( intIndex element, intIndex score ) {

	if ( size < maxLength ) {
	  list[2*size] = element;
	  list[2*size+1] = score;
	  size++;
	}
}

inline void listIndex::setElement( intIndex i, intIndex c ) {

  list[2*i] = c;
}

inline void listIndex::setCount( intIndex i, intIndex c ) {

  list[2*i+1] = c;
}

inline intIndex listIndex::count( intIndex i ) const {

  return list[2*i+1];	
}

inline intIndex listIndex::element( intIndex i ) const {

  return list[2*i];	
}

inline long listIndex::contains( const listIndex &l ) const {

  intIndex i, j;
  for ( i = 0, j = 0 ; j < l.length() ; j++ ) {
    while (( i < length() )&&( element(i) != l.element(j) )) i++;
    if ( i >= length() ) return 0;
  }
  return length()-i;
}

inline bool listIndex::contains( intIndex elt ) const {
 
  for ( intIndex i = 0 ; i < length() ; i++ ) if ( element(i) == elt ) return true;
  return false;
}

inline void listIndex::removeElement( intIndex elt ) {

  intIndex i = 0;
  for ( ; (i < length())&&(elt!=element(i)) ; i++ ) ;
  if ( i >= length() ) return;
  for ( i++ ; i < length() ; i++ ) {
    setElement( i-1, element(i) );
    setCount( i-1, count(i) );
  }
  size--;
}

class sListIndex : public listIndex {
  ///< Special lists with engine number in the high weight word of scores
  
  public:
    sListIndex() : listIndex() {}
    sListIndex( long _maxLength ) : listIndex( _maxLength ) {}
    void fusion( listIndex l, intIndex id );
    void sortCounts();
    void append( intIndex element, uint16_t score, uint16_t id );
    
    uint16_t count( intIndex i ) { return list[2*i+1] & 0xffff; }
    uint16_t id( intIndex i ) { return list[2*i+1] >> 16; }
};

template <class Tpointer>
inline void listIndex::fromCompactScoreMap( compactIterator<Tpointer> lit, intIndex *scoreMap ) {

  intIndex i;
  for ( i = 0 ; (i<maxLength) && lit.remains() ; ++lit, ++i ) {
    lit.getElement( list[2*i], list[2*i+1] );
    if ( scoreMap ) list[2*i+1] = scoreMap[list[2*i+1]];
  }
  size = i;
}

template <class Tpointer>
inline bool listIndex::fromCompactRemove( compactList<Tpointer> l, listIndex mod ) {

  bool bRemoved = false;
  compactIterator<Tpointer> lit = l.iterator();
  intIndex i = 0;
  intIndex iMod = 0;
  intIndex le, ls; // Element/score courrant de l'itérateur lit. le vaut intIndexInvalid si ( non lit.remains() )
  if ( lit.remains() ) lit.getElement(le,ls); else { size=0; return false; }
  intIndex mode = mod.length() ? mod.element(0) : intIndexInvalid;

  while ( 1 ) { // Comme on ne fait que retirer, on ne teste pas le débordement de maxLength
   while ( le > mode ) {
    iMod++;
    if ( iMod < mod.length() ) mode = mod.element(iMod);
    else mode = intIndexInvalid;
   }
   if ( le < mode ) {
    list[2*i] = le;
    list[2*i+1] = ls;
    i++;
   } else bRemoved = true;
   ++lit; if ( lit.remains() ) lit.getElement(le,ls); else break;
  }
  size = i;
  return bRemoved;
}

template <class Tpointer>
inline void listIndex::fromCompactFusion( compactList<Tpointer> l, listIndex mod ) {
// Prend <l>, lui ajoute les éléments de <mod> de score != intIndexInvalid,
// lui retire les éléments de <mod> de score intIndexInvalid,
// Attention, fonction optimisée avec des trucs bizarres dedans

  compactIterator<Tpointer> lit = l.iterator();
  intIndex i = 0;
  intIndex iMod = 0;
  intIndex le, ls = 0; // Element/score courrant de l'itérateur lit. le vaut intIndexInvalid si ( non lit.remains() )
  if ( lit.remains() ) lit.getElement(le,ls); else le = intIndexInvalid;
  if ( mod.length() ) 
   while ( i < maxLength ) { // boucle iMod < mod.length()

    while (( mod.count(iMod) == (intIndex)intIndexInvalid )&&( mod.element(iMod) < le )) {
      iMod++; if ( iMod >= mod.length() ) goto _end_fusion; }
     // Parce qu'il y a _en moyenne_ beaucoup plus de intIndexInvalid, et beaucoup moins d'éléments dans l que 
     // dans mod.

    intIndex mode = mod.element(iMod);
    if ( le < mode ) { // cas : on prend l'élément de l
      list[2*i]=le; list[2*i+1]=ls;
      i++; ++lit;
      if ( lit.remains() ) lit.getElement(le,ls);
      else le = intIndexInvalid;
    } else { // cas : on prend l'élément de mod
      intIndex mods = mod.count(iMod);
      if ( mods != intIndexInvalid ) {
        list[2*i] = mode;
        list[2*i+1] = mods;
        i++;
        mod.setCount( iMod, intIndexInvalid ); // On remet la liste à intIndexInvalid au passage ...
      } else goto _is_equal_le_mode;
      if ( le==mode ) {
       _is_equal_le_mode:
        ++lit; if ( lit.remains() ) lit.getElement(le,ls); else le = intIndexInvalid; }
      iMod++; if ( iMod >= mod.length() ) break;
    }
   }
 _end_fusion:

  if ( le != intIndexInvalid )
   while ( i < maxLength ) { // boucle iMod >= mod.length()
    list[2*i]=le; list[2*i+1]=ls;
    i++; ++lit;
    if ( lit.remains() ) lit.getElement(le,ls);
    else break;
   }

  size = i;
}

#endif
