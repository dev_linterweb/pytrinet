import React from 'react';
import './Project.css';
import Editable from './editable'
import ControlledInput from './controlledInput';

class Project extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        unfolded:false,
        newFieldValue:'',
        newField:''
      }
    }

    onAction( method, action, what, e ) {
        if ( e ) e.stopPropagation()
        this.props.onAction( method, this.props.project.name, action, what )
    }

    onUpdateValue(section,key,value) {
        this.props.onAction( 'post', this.props.project.name, 'set', {
            section:section,
            key:key,
            value:value
        } )
    }

    onNewField(section) {
        console.log(this.state.newField+' '+this.state.newFieldValue)
        this.props.onAction( 'post', this.props.project.name, 'set', {
            section:section,
            key:this.state.newField,
            value:this.state.newFieldValue
        } )
    }

    renderSection(section,i) {
        let data = this.props.project[section]
        let rows = ( !data )? '' :
            Object.entries( data ).map( (entry)=>{
                const [key,value] = entry
                return <tr key={key}><td>{key}</td><td>
                    <Editable value={value} onSubmit={this.onUpdateValue.bind(this,section,key)}/>
                </td></tr>
            })
        return <table key={i}>
            <thead><tr><th colSpan="2">{section}</th></tr></thead>
            <tbody>{rows}
            <tr><td><ControlledInput type='text' property='newField' container={this} onSubmit={()=>{}}/></td>
            <td><ControlledInput type='text' property='newFieldValue' container={this} onSubmit={this.onNewField.bind(this,section)}/></td></tr>
            </tbody></table>
    }

    renderDetails() {
        let details = ''
        switch ( this.state.unfolded ) {
            case 'explorer':
                details = [ this.renderSection('explorer',0) ]
                for ( let i = 1 ; ; i++ ) {
                    let name = 'explorer'+i
                    if ( name in this.props.project ) {
                        details.push( this.renderSection(name,i) )
                    } else break;
                }
        }
        return <tr><td colSpan="2">{details}</td></tr>
    }

    render() {
        let exploring
        if ( this.props.project.stopping ) {
            exploring = <td className="project">Stopping ... </td>
        } else {
            exploring = this.props.project.exploring ?
                <td className="project">Exploring &nbsp;
                    <button onClick={this.onAction.bind(this,'get','stop',false)}>Stop</button>
                </td> :
                <td className="project">Stopped &nbsp;
                    <button onClick={this.onAction.bind(this,'get','start',false)}>Start</button>
                </td>
        }
        return <React.Fragment>
            <tr onClick={e=>{this.setState({unfolded:this.state.unfolded?false:'explorer'})}}>
                <td className="project left">{this.props.project.name}</td>
                
                {exploring}
                <td className="project">
                    <button onClick={this.onAction.bind(this,'get','save',false)}>Save</button>
                    <button onClick={this.onAction.bind( this,'post','removeOrphans',{} )}>removeOrphans</button>
                </td>
            </tr>
            {this.state.unfolded ? this.renderDetails() : ''}
        </React.Fragment>
    }
}

export default Project;