#include "pyDynamicEngine.h"

#include <structmember.h>

#include "pyDynamicEngineArray.h"
#include "dynengine.h"
#include "charPtrArray.h"
#include "pyScoredList.h"

static PyMemberDef dynamicEngine_members[] = {
    {NULL}
};


static void
PyDynamicEngine_dealloc(PyDynamicEngine *self) {
    Py_XDECREF(self->pyEngineArray);
    if ( self->engine ) delete self->engine;
    Py_TYPE(self)->tp_free((PyObject *) self);
}

static PyObject *
PyDynamicEngine_new(PyTypeObject *type, PyObject *args, PyObject *kwds) {
    PyDynamicEngine *self;
    self = (PyDynamicEngine *) type->tp_alloc(type, 0);
    self->engine = NULL;
    self->pyEngineArray = NULL;
    return (PyObject *) self;
}


static int
PyDynamicEngine_init(PyDynamicEngine *self, PyObject *args, PyObject *kwds) {

    static const char * kwConst[] = {"path", "lang", "engineArray", "isClassifier", NULL};
    static CharPtrArray kwlist( kwConst );
    PyObject *path_obj = NULL, *lang_obj = NULL, *engineArray = NULL;
    const char *path, *lang;
    int isClassifier = false;

    if ( ! PyArg_ParseTuple(args, "O&OO|i", PyUnicode_FSDecoder, &path_obj, &lang_obj, &engineArray, &isClassifier) )
        return -1;

    if ( path_obj != Py_None ) {
        path = PyUnicode_AsUTF8( path_obj );
    } else {
        return -1;
    }

    if ( lang_obj != Py_None ) {
        lang = PyUnicode_AsUTF8( lang_obj );
    } else {
        return -1;
    }

    if ( Py_TYPE( engineArray ) != & DynamicEngineArrayType ) {
        return -1;
    }

    Py_INCREF(engineArray);
    Py_XDECREF(self->pyEngineArray);
    self->pyEngineArray = engineArray;

    Py_BEGIN_ALLOW_THREADS

        if ( ! self->engine ) self->engine = new lsDynEngine();

        self->engine->setClassifier( isClassifier );

        self->loadThread = std::thread( [=]() {
            self->engine->init( path, lang, ((PyDynamicEngineArray*)engineArray)->engineArray );
        });

    Py_END_ALLOW_THREADS

       // asyncInit, self, path, lang, ((PyDynamicEngineArray*)engineArray)->engineArray );

    // self->engine->init( path, lang, ((PyDynamicEngineArray*)engineArray)->engineArray );

    return 0;
}

static PyObject *
PyDynamicEngine_joinLoad(PyDynamicEngine *self) {
    
    Py_BEGIN_ALLOW_THREADS
        if ( self->loadThread.joinable() ) self->loadThread.join();
    Py_END_ALLOW_THREADS

    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_save(PyDynamicEngine *self) {

    Py_BEGIN_ALLOW_THREADS
        self->engine->save();
    Py_END_ALLOW_THREADS

    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_convert(PyDynamicEngine *self) {

    Py_BEGIN_ALLOW_THREADS
        self->engine->convert();
    Py_END_ALLOW_THREADS

    Py_RETURN_NONE;
}


static PyObject *
PyDynamicEngine_addUrl(PyDynamicEngine *self, PyObject *args) {

    PyObject * callableBody ;
    unsigned long stamp = 0;

    if (
        ( !PyArg_ParseTuple(args, "O|k", &callableBody, &stamp ) )
        ||
        ( !PyCallable_Check( callableBody ) )
    ) {
        std::cout << "Expected one callable (addUrl)" << std::endl;
        PyErr_SetString(PyExc_TypeError, "Expecting one callables");
        return NULL;
    }

    lsWordStream body( callableBody );
    listIndex wordList = self->engine->pageFlatList(body, self->engine->getnWordInsert() );

    intIndex idx;
    Py_BEGIN_ALLOW_THREADS
        idx = self->engine->storeNewUrl( wordList, stamp );
    Py_END_ALLOW_THREADS

//    intIndex idx = self->engine->addUrl( body, stamp );

    if ( PyErr_Occurred() ) {
        return NULL;
    }
    if ( idx == intIndexInvalid ) {
        Py_RETURN_NONE;
    }
    return PyLong_FromLong(idx);
}

static PyObject *
PyDynamicEngine_updateUrl(PyDynamicEngine *self, PyObject *args) {

    PyObject * callableBody ;
    unsigned long id;
    unsigned long stamp = 0;

    if (
        ( !PyArg_ParseTuple(args, "kO|k", &id, &callableBody, &stamp ) )
        ||
        ( !PyCallable_Check( callableBody ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Expecting an id (integer) a callable and an optional integer");
        return NULL;
    }

    lsWordStream body( callableBody );
    listIndex wordList = self->engine->pageFlatList(body, self->engine->getnWordInsert() );

    Py_BEGIN_ALLOW_THREADS
        self->engine->updateUrl( (intIndex)id, wordList, stamp );
    Py_END_ALLOW_THREADS

    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_fusionTitleUrl(PyDynamicEngine *self, PyObject *args) {

    PyObject * callableBody ;
    unsigned long id;
    unsigned long stamp = 0;

    if (
        ( !PyArg_ParseTuple(args, "kO|k", &id, &callableBody, &stamp ) )
        ||
        ( !PyCallable_Check( callableBody ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Expecting an id (integer) a callable and an optional integer");
        return NULL;
    }

    lsWordStream body( callableBody );
    listIndex wordList = self->engine->pageFlatList(body, self->engine->getnWordInsert() );

    Py_BEGIN_ALLOW_THREADS
        self->engine->fusionTitle( (intIndex)id, wordList, stamp );
    Py_END_ALLOW_THREADS

    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_removeUrl(PyDynamicEngine *self, PyObject *args) {
    
    unsigned long id;
    if (
        ( !PyArg_ParseTuple(args, "k", &id ) ) 
    ) {
        PyErr_SetString(PyExc_TypeError, "Expecting an id (integer)");
        return NULL;
    }
    Py_BEGIN_ALLOW_THREADS
    self->engine->removeUrl( (intIndex)id );
    Py_END_ALLOW_THREADS

    Py_RETURN_NONE;
}


static PyObject *
PyDynamicEngine_setFractIndex(PyDynamicEngine *self, PyObject *args) {
    
    unsigned long fractIndex;
    if (
        ( !PyArg_ParseTuple(args, "k", &fractIndex ) ) 
    ) {
        PyErr_SetString(PyExc_TypeError, "Expecting an id (integer)");
        return NULL;
    }
    self->engine->setFractIndex(fractIndex);
    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_countDocumentsByMetawords(PyDynamicEngine *self, PyObject *args) {

    const char * prefix;

    if (
        ( !PyArg_ParseTuple(args, "s", &prefix ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Wrong parameters in method countDocumentsByMetawords. Expecting (prefix:str).");
        return NULL;
    }

    PyObject * rt = PyDict_New();

    std::map<intIndex,intIndex> counter;

    Py_BEGIN_ALLOW_THREADS
        self->engine->countDocumentsByMetawords( prefix, counter );
    Py_END_ALLOW_THREADS

    for ( std::map<intIndex,intIndex>::iterator iter = counter.begin(); iter != counter.end(); ++iter ) {
        intIndex id = iter->first;
        lsBufferConst word = self->engine->wordMap->getWord(id);
        if ( word.getSize() ) {
            PyObject * key = PyUnicode_FromStringAndSize( word.getBuffer(), word.getSize() );
            PyObject * value = PyLong_FromLong( iter->second );
            PyDict_SetItem( rt, key, value );
            Py_DECREF(value);
            Py_DECREF(key);
        }
    }

    return rt;
}

static PyObject *
PyDynamicEngine_query(PyDynamicEngine *self, PyObject *args) {

    const char * phrase;
    int bReorder = 0;

    if (
        ( !PyArg_ParseTuple(args, "sp", &phrase, &bReorder ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Wrong parameters in method query");
        return NULL;
    }

    listIndex results;

    Py_BEGIN_ALLOW_THREADS

        lsSearchParams params;
        listIndex wQuery;

        std::cout << "Querying " << phrase << std::endl;

        results = self->engine->query( phrase, wQuery, NULL, false, bReorder, params );

        wQuery.free();

    /*    std::string sVocSpe;
        for ( guint i = 0 ; i < vocSpe.length() ; i++ ) {
            if ( i>0 ) sVocSpe += ',';
            sVocSpe += self->engine->wordMap->getWord( vocSpe.element(i) );
        }
        std::cout << sVocSpe << std::endl;

        vocSpe.free();
    */

    Py_END_ALLOW_THREADS


    return PyScoredList_FromListIndex(results);
    /*Py_BuildValue(
        "Os", 
        PyScoredList_FromListIndex(results), 
        PyUnicode_FromString( sVocSpe.c_str() ) 
    );*/
}


static PyObject *
PyDynamicEngine_queryConnex(PyDynamicEngine *self, PyObject *args) {

    unsigned long id;
    int bReorder = 0;

    if (
        ( !PyArg_ParseTuple(args, "kp", &id, &bReorder ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Wrong parameters in method queryConnex");
        return NULL;
    }

    listIndex results;

    Py_BEGIN_ALLOW_THREADS

        lsSearchParams params;

        std::cout << "Querying connex " << id << std::endl;

        results = self->engine->queryConn( (intIndex)id, bReorder, params );

    Py_END_ALLOW_THREADS


    return PyScoredList_FromListIndex(results);
    /*Py_BuildValue(
        "Os", 
        PyScoredList_FromListIndex(results), 
        PyUnicode_FromString( sVocSpe.c_str() ) 
    );*/
}


static PyObject *
PyDynamicEngine_syncExtension(PyDynamicEngine *self) {
    Py_BEGIN_ALLOW_THREADS
        self->engine->flushExtension();
        self->engine->waitExtension();
    Py_END_ALLOW_THREADS
    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_flushExtension(PyDynamicEngine *self) {
    Py_BEGIN_ALLOW_THREADS
        self->engine->flushExtension();
    Py_END_ALLOW_THREADS
    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_infos(PyDynamicEngine *self) {
    return Py_BuildValue("(l)", self->engine->getNArticles() );
}

static PyObject *
PyDynamicEngine_classify(PyDynamicEngine *self, PyObject *args) {

    PyObject * e;
    unsigned long id;

    if (
        ( !PyArg_ParseTuple(args, "Ok", &e, &id ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Wrong parameters in method classify");
        return NULL;
    }

    if ( ! PyObject_IsInstance( e, reinterpret_cast<PyObject*>(&DynamicEngineType) ) ) {
        PyErr_SetString(PyExc_TypeError, "classify : first parameter must be a PyDynamicEngine");
        return NULL;
    }

    if ( !self->engine->isClassifier() ) {
        PyErr_SetString(PyExc_TypeError, "classify : must be called on an engine classifier flag on");
        return NULL;
    }

    listIndex rt;
    Py_BEGIN_ALLOW_THREADS

        PyDynamicEngine * pyEngine = reinterpret_cast<PyDynamicEngine*>(e);
        lsDynEngine * engine = pyEngine->engine;

        {   ReadLock( engine->articles ); 
            rt = self->engine->classify( engine->getArticle( id ) );
        }
    Py_END_ALLOW_THREADS

    PyObject * tuple = PyTuple_New( rt.length() );
    for ( intIndex i = 0 ; i < rt.length() ; i++ ) {
        PyTuple_SetItem( tuple, i, PyLong_FromLong( rt.element(i) ) );
    }

    rt.free();
    return tuple;
}


static PyObject *
PyDynamicEngine_useClassifier(PyDynamicEngine *self, PyObject *args) {

    PyObject * e;

    if (
        ( !PyArg_ParseTuple(args, "O", &e ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Wrong parameters in method useClassifier");
        return NULL;
    }

    if ( ! PyObject_IsInstance( e, reinterpret_cast<PyObject*>(&DynamicEngineType) ) ) {
        PyErr_SetString(PyExc_TypeError, "useClassifier : first parameter must be a PyDynamicEngine");
        return NULL;
    }

    PyDynamicEngine * pyEngine = reinterpret_cast<PyDynamicEngine*>(e);
    lsDynEngine * engine = pyEngine->engine;

    if ( ! engine->isClassifier() ) {
        PyErr_SetString(PyExc_TypeError, "useClassifier : first parameter must be a DynamicEngine with flag classifier on");
        return NULL;
    }

    self->engine->useClassifier( engine );
    Py_RETURN_NONE;
}


static PyObject *
PyDynamicEngine_debug(PyDynamicEngine *self, PyObject *args) {

    const char * command;
    const char * arg;

    if (
        ( !PyArg_ParseTuple(args, "|ss", &command, &arg ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Wrong parameters in method debug");
        return NULL;
    }

    long nArticles = self->engine->getNArticles();
    long nWords = self->engine->getNWords();

    if ( !command ) {
        std::cout << "== Articles (" << nArticles << ")" << std::endl;
        for ( long i = 0 ; i < nArticles ; i++ ) {
            self->engine->debugLArticle(i);
        }
        std::cout << std::endl;

        std::cout << "== Words (" << nWords << ")" << std::endl;
        for ( long i = 0 ; i < nWords ; i++ ) {
            self->engine->debugMapWord(i);
            self->engine->debugLWord(i);
        }
        std::cout << std::endl;

        std::cout << "== Extension" << std::endl;
        self->engine->debugExtension();
        std::cout << std::endl;

    } else if (( strcmp( command, "article" ) == 0 )&& arg ) {
        int id = atoi(arg);
        self->engine->debugLArticle(id);

    } else if (( strcmp( command, "articleTop" ) == 0 )&& arg ) {
        int id = atoi(arg);
        self->engine->debugLArticleTop(id);

    } else if (( strcmp( command, "word" ) == 0 )&& arg ) {
        int id = atoi(arg);
        self->engine->debugMapWord(id);
        self->engine->debugLWord(id);
    } else if (( strcmp( command, "status" ) == 0 )) {
        std::cout << nArticles << " articles, " << nWords << " words" << std::endl;
    }
    Py_RETURN_NONE;
}


static PyObject *
PyDynamicEngine_notifyQuit(PyDynamicEngine *self) {
    self->engine->notifyQuit();
    Py_RETURN_NONE;
}

static PyObject *
PyDynamicEngine_getMetaWords(PyDynamicEngine *self, PyObject *args) {
    
    long id;

    if (
        ! PyArg_ParseTuple(args, "k", &id )
    ) {
        PyErr_SetString(PyExc_TypeError, "Wrong parameters in method getMetaWords");
        return NULL;
    }

    std::vector<lsBufferConst> metaWords;

    Py_BEGIN_ALLOW_THREADS
        mapWord *wordMap = self->engine->wordMap;
        articleIndexDynamic* articles = self->engine->dynArticles();
        {   ReadLock _(wordMap);
            ReadLock __(articles);

            dyCompactList l = articles->access(id);
            dyCompactIterator it = l.iterator();
            for ( ; it.remains() ; ++it ) {
                intIndex idWord = it.element();
                lsBufferConst word = wordMap->getWord( idWord );
                if ( word.contains(':') ) {
                    metaWords.push_back(word);
                }
            }
        }
    Py_END_ALLOW_THREADS

    PyObject * rt = PyList_New( metaWords.size() );
    for ( size_t i = 0 ; i < metaWords.size() ; i++ ) {
        lsBufferConst buf = metaWords[i];
        PyList_SetItem( rt, i, PyUnicode_FromStringAndSize( buf.getBuffer(), buf.getSize() ) );
    }

    return rt;
}

static PyObject *
PyDynamicEngine_removeOrphans(PyDynamicEngine *self, PyObject *args) {

    PyObject * callable ;

    if (
        ( !PyArg_ParseTuple(args, "O", &callable ) )
        ||
        ( !PyCallable_Check( callable ) )
    ) {
        PyErr_SetString(PyExc_TypeError, "Expecting one callable");
        return NULL;
    }

    intIndex n = self->engine->getNArticles();

    PyObject * _args = PyTuple_New(1);
    for ( intIndex i = 0 ; i < n ; i++ ) {
        if ( self->engine->getArticle( i ).length() ) {
            PyTuple_SetItem( _args, 0, PyLong_FromUnsignedLong( i ) );
            PyObject * rt = PyObject_CallObject( callable, _args );
            if (rt == NULL) return NULL; /* Pass error back */
            if ( ! PyObject_IsTrue( rt ) ) {
                std::cout << "Removing article " << i << std::endl;
                self->engine->removeUrl( (intIndex)i );
            }
            Py_DECREF(rt);
        }
    }
    Py_DECREF(_args);

    Py_RETURN_NONE;
}


static PyMethodDef dynamicEngine_methods[] = {
    {"countDocumentsByMetawords", (PyCFunction) PyDynamicEngine_countDocumentsByMetawords, METH_VARARGS,
     "Returns a map (meta word)->(number of documents) providing the number of documents where \
     each meta word appear."
    },
    {"useClassifier", (PyCFunction) PyDynamicEngine_useClassifier, METH_VARARGS,
     "Use a classifier engine when adding or updating a document."
    },
    {"classify", (PyCFunction) PyDynamicEngine_classify, METH_VARARGS,
     "Call only on a classifier type engine. Takes another engine and an id in this other engine. Classifies the corresponding document of other engine, wrt this classifier set of documents."
    },
    {"joinLoad", (PyCFunction) PyDynamicEngine_joinLoad, METH_NOARGS,
     "Loading operation is asynchronous. Use this to join the loading thread."
    },
    {"save", (PyCFunction) PyDynamicEngine_save, METH_NOARGS,
     "Synchronize the search engine with its disk storage"
    },
    {"convert", (PyCFunction) PyDynamicEngine_convert, METH_NOARGS,
     "Synchronize the search engine using 'compactListConvert' format for compact lists: used to migrate to new file versions"
    },
    {"addUrl", (PyCFunction) PyDynamicEngine_addUrl, METH_VARARGS,
     "Inserts a document in the search engine. Returns its id."
    },
    {"updateUrl", (PyCFunction) PyDynamicEngine_updateUrl, METH_VARARGS,
     "Inserts a document in the search engine at the position of an existing document. The old document is dropped."
    },
    {"fusionTitleUrl", (PyCFunction) PyDynamicEngine_fusionTitleUrl, METH_VARARGS,
     "Keeps old document but adds title words of given new document."
    },
    {"removeUrl", (PyCFunction) PyDynamicEngine_removeUrl, METH_VARARGS,
     "Removes a document from the search engine."
    },
    {"setFractIndex", (PyCFunction) PyDynamicEngine_setFractIndex, METH_VARARGS,
     "Sets the proportion of indexed words in documents (4=half, 8=all). Default:6."
    },
    {"query", (PyCFunction) PyDynamicEngine_query, METH_VARARGS,
     "Queries the search engine with a phrase"
     },
    {"queryConnex", (PyCFunction) PyDynamicEngine_queryConnex, METH_VARARGS,
     "Queries the search engine with a document id"
     },
    {"syncExtension", (PyCFunction) PyDynamicEngine_syncExtension, METH_NOARGS,
     "Forces synchronization of the newly modified documents with the index and wait until it to be fully synchronized"
    },
    {"flushExtension", (PyCFunction) PyDynamicEngine_flushExtension, METH_NOARGS,
     "Forces synchronization of the newly modified documents with the index. This function does not wait. Consider using syncExtension if you need the indexes to be synced right now."
    },
    {"notifyQuit", (PyCFunction) PyDynamicEngine_notifyQuit, METH_NOARGS,
     "Puts the engine in a safe state for the process to be terminated or killed. It does not actually quits. After notifyQuit, engine is no more working properly. Calling the save method would dead-lock."
    },
    {"infos", (PyCFunction) PyDynamicEngine_infos, METH_NOARGS,
    "Returns informations, mainly the count of documents"
    },
    {"debug", (PyCFunction) PyDynamicEngine_debug, METH_VARARGS,
    "Print debug on stdout"
    },
    {"getMetaWords", (PyCFunction) PyDynamicEngine_getMetaWords, METH_VARARGS,
    "Takes the id of a document, and returns the list of its meta-words (as strings)"
    },
    {"removeOrphans", (PyCFunction) PyDynamicEngine_removeOrphans, METH_VARARGS,
    "Takes a function that takes a numerical id and returns False when the id does not exist in the external map. This will remove such id's from the search engine."},
    {NULL}  /* Sentinel */
};

PyTypeObject DynamicEngineType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "trinet._DynamicEngine",
    .tp_basicsize = sizeof(PyDynamicEngine),
    .tp_itemsize = 0,
    .tp_dealloc = (destructor) PyDynamicEngine_dealloc,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
    .tp_doc = PyDoc_STR("Dynamic Trinet search engine instance"),
    .tp_methods = dynamicEngine_methods,
    .tp_members = dynamicEngine_members,
    .tp_init = (initproc) PyDynamicEngine_init,
    .tp_new = PyDynamicEngine_new,
};
