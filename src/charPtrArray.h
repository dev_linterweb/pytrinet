#ifndef _CHAR_PTR_ARRAY_H_
#define _CHAR_PTR_ARRAY_H_

/**
 * @brief Efficiently converts a const char ** NULL terminated array of C strings into 
 * a char ** NULL terminated array of C strings.
 */
class CharPtrArray {

public:
    CharPtrArray( const char ** strings );
    ~CharPtrArray() {}

    char ** get() { return ptrs; }

private:
    char * text;
    char ** ptrs;
};

#endif