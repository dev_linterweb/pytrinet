#ifndef _WORD_STREAM_H_
#define _WORD_STREAM_H_

#include <Python.h>
#include "lintersearch.h"

class lsWordStream {

public:
    /** Takes a python callable. The callable
     * must return tuples [word,score] listing words from a document,
     * until the end of the document is reached. Then the callable must return
     * Py_None
     */
    lsWordStream( PyObject *_o ) {
        o = _o;
        eostream = false;
        Py_INCREF(o);
    }
    ~lsWordStream() {
        Py_DECREF(o);
    }

    intIndex nextWord();

    const char *getWord() const {
        return word;
    }

private:
    PyObject *o;
    bool eostream;
    char word[256];
};

#endif