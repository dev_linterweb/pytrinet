import io

# Takes a TextIOBase and let you read some head characters
# Then serves the content as if head was not read.

class TextIODelayed(io.TextIOBase):

    def __init__(self, text:io.TextIOBase ):
        self._text = text
        self._curs = 0
        self._head = ''

    # Drop n character from head
    def dropHead(self,n) :
        self._curs += n

    def getHead(self, n) :
        self._head = self._text.read(n)
        return self._head

    def readable(self):
        return True

    def read(self, n=None):
        if self._curs >= len(self._head) :
            return self._text.read(n)

        if n == None :
            rt = self._head[ self._curs : ]
            self._curs = len(self._head)
            return rt
        
        head = self._head[ self._curs : self._curs+n ]
        self._curs += len(head)
        tail = self._text.read(n-len(head)) if n-len(head)>0 else ''
        return head+tail

    def readline(self): # unused, so not implemented. Head is lost if you use it.
        return self._text.readline()

    def readall(self):
        if self._curs < len(self._head) :
            return self._head[ self._curs : ] + self._text.read()
        return self._text.read()
 