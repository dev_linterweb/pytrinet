#ifndef _SHRINKER_H_
#define _SHRINKER_H_

#include "lintersearch.h"

class lsShrinker { // ref counted object
  
  intIndex *trewrite;
  intIndex len;
  intIndex targetLen;
  
  public:
    lsShrinker( intIndex _len );
    ~lsShrinker();
    lsShrinker & operator = ( const lsShrinker &s );
    lsShrinker( const lsShrinker &s ) { *this = s; }
    
    void removeAll();
    void keep( intIndex id );
    void build();
    
    intIndex length() const { return len; }
    intIndex targetLength() const { return targetLen; }
    
    intIndex rewrite( intIndex orig ) const { return (orig<len)?trewrite[orig]:~0; }
    void debug();
};

#endif
