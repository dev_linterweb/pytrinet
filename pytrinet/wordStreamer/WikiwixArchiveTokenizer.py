
 # Parses text from Wikiwix Archive "JArchive 1", which store a json version of the page DOM tree.

import re
from nltk.tokenize import RegexpTokenizer

from pytrinet.wordStreamer.Tokenizer import Tokenizer

class WikiwixArchiveTokenizer(Tokenizer):
    
    class GrowingList(list):
        def __setitem__(self, index, value):
            if index >= len(self):
                self.extend([None]*(index + 1 - len(self)))
            list.__setitem__(self, index, value)

    pattDomBegin = 'let wikiwix_dom ='

    def __init__( self ):
        self.tagLevels = {'script':0,'body':0,'p':0,'title':0}
        self.inDom = False
        self.tags = self.GrowingList()
        self.tokenizer = RegexpTokenizer(r'[\w0-9]+')
        self.words = []

    def getScoreAt( self, level ) :
        inBody = False
        inParag = False
        for i in range(0,level) :
            if self.tags[i] == 'script':
                return 0
            if self.tags[i] == 'title':
                return -1
            if self.tags[i] == 'body':
                inBody = True
            if self.tags[i] == 'p':
                inParag = True
        if inBody :
            if inParag : return 2
            else : return 1
        else : return 0

    def tokenize( self, text: str, flush: bool ): # flush means we're done, no more text is coming afterwards
        self.words = []
        self.pushText(text)
        if not self.inDom :
            pos = self.buffer.find( self.pattDomBegin, self.curs )
            if pos < 0 :
                self.seekToEnd( len(self.pattDomBegin) )
            else :
                self.curs = pos+len(self.pattDomBegin)
                self.inDom = True
        if self.inDom :
            while True :
                line = self.getLine()
                if not line : break
                if line :
                    m = re.search(r'^( *)\{"(tag|text)":"([^"]+)"',line)
                    if m :
                        level = len(m.group(1))
                        if m.group(2)=='tag' :
                            self.tags[level] = m.group(3)
                        else :
                            score = self.getScoreAt(level)
                            for token in self.tokenizer.tokenize( m.group(3).replace('\\n',' ') ) :
                                self.words.append( (token,score) )
        return self.words