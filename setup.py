""" from distutils.core import setup, Extension
import glob

module1 = Extension('trinet',
       sources = ['trinetmodule.cpp']+glob.glob('src/*.cpp'),
       extra_compile_args=['-std=c++17','-O0'])


setup (name = 'trinet',
       version = '1.0',
       description = 'Trinet search engine for Python',
       ext_modules = [module1])
 """
from setuptools import Extension, setup
import glob

setup(
    name='pytrinet',
    version='0.0.1',
    ext_modules=[
        Extension(
            name='trinet',
            sources=['trinetmodule.cpp']+glob.glob('src/*.cpp'),
            extra_compile_args=['-std=c++17'],
            extra_link_args=['-lstdc++fs']
        ),
    ],
    packages=['pytrinet','pytrinet.explorer','pytrinet.wordStreamer']
)
