#include "config.h"
#include "dyCompactList.h"
#ifdef HAVE_MALLINFO
#include <malloc.h>
#endif
#include <unistd.h>

guchar *dyChunk::entry( guint i ) {

  return chunkStore + i*atomSize;
} 

void *dyChunk::operator new( size_t num_bytes, guint _atomSize ) {

  guint chunkSize = DY_CHUNK_SIZE;
  if ( _atomSize > 256 ) {
    chunkSize *= 2; 
  }
  if ( _atomSize > 2048 ) {
    chunkSize *= 2;
  }
  guint nAtom = ( 8 * ( chunkSize-num_bytes-sizeof(gulong) ) / ( 8*_atomSize + 1 ) );
  dyChunk *dc = (dyChunk*)std::malloc( nAtom * _atomSize + nAtom/8 + num_bytes + sizeof(gulong) );
  dc->chunk = ((guchar*)dc)+num_bytes;
  dc->chunkStore = dc->chunk + nAtom/8 + sizeof(gulong);
  dc->follow = NULL;
  dc->atomSize = _atomSize;
  dc->nAtom = nAtom;
  dc->freeFirst = 0;
  memset( dc->chunk, 0, nAtom/8 + sizeof(gulong) );
//  lsStatistics::inc( "dyChunks storage", dc->nAtom * dc->atomSize + dc->nAtom/8 + sizeof(dyChunk) );
  return dc;
}

void dyChunk::operator delete( void * data ) {
 
  if ( data ) {
   dyChunk *dc = (dyChunk*)data;
   // lsStatistics::dec( "dyChunks storage", dc->nAtom * dc->atomSize + dc->nAtom/8 + sizeof(dyChunk) );
//   delete dc->follow;
   std::free( dc );
  }
}

bool dyChunk::getMarked( guint i ) {
 
//  if ( i >= nAtom ) DEBUG_BREAK;
  return ( chunkMark[i/(8*sizeof(gulong))] & ( (gulong)1 << i%(8*sizeof(gulong)) ) ) != 0;
}

void dyChunk::mark( guint i ) {
 
//  if ( getMarked( i ) ) DEBUG_BREAK;
  chunkMark[i/(8*sizeof(gulong))] = chunkMark[i/(8*sizeof(gulong))] | ( (gulong)1 << i%(8*sizeof(gulong)) );
}

void dyChunk::unmark( guint i ) {
  
//  if ( ! getMarked( i ) ) DEBUG_BREAK;
  chunkMark[i/(8*sizeof(gulong))] = chunkMark[i/(8*sizeof(gulong))] & ~( (gulong)1 << i%(8*sizeof(gulong)) );
}

void dyChunk::whereIs( guchar *ptr, guint16 &n, guint16 &i, guint16 &j, guint16 this_n, bool withFollower, guint16 this_i ) {

  if (( chunk <= ptr )&&( ptr < chunkStore  )) {
    j = (guint16)intIndexInvalid;
    i = this_i;
    n = this_n;
    return;
  }
  if (( chunkStore <= ptr )&&( ptr < chunkStore + atomSize * nAtom )) {
    n = this_n;
    i = this_i;
    j = (guint)(( ptr - chunkStore )/atomSize);
//    if ( withFollower &&( (guint64)(ptr-(j*atomSize+chunkStore)) >= atomSize-sizeof( guchar* ) ) ) j = (guint)-2;
    return;
  }
  if (( n == (guint16)intIndexInvalid )&& follow ) follow->whereIs( ptr, n, i, j, this_n, withFollower, this_i+1 );
}

void dyChunk::adjustFreeFirst() {
 
  guint i = freeFirst/(8*sizeof(gulong));
  guint n = nAtom/(8*sizeof(gulong))+1;
  while (( i < n )&&( !~chunkMark[i] )) i++;
  if ( i < n ) {
    guint ii = 0;
    gulong thisMark = chunkMark[i];
    while ( thisMark & 1 ) { ii++; thisMark = thisMark >> 1; }
    freeFirst = i * 8*sizeof(gulong) + ii;
    if ( freeFirst >= nAtom ) freeFirst = intIndexInvalid;
  } else freeFirst = intIndexInvalid;
}
 
bool dyChunk::empty() const {
  if ( *chunkMark ) return false; // quick massive filter
  guint n = nAtom/(8*sizeof(gulong));
  for ( guint i = 1 ; ( i < n ) ; i++ ) if ( chunkMark[i] ) return false;
  return ( !( chunkMark[n] & ( ((gulong)1 <<( nAtom%(8*sizeof(gulong)) ))-1 ) ) );
}

guchar *dyChunk::alloc() {
  dyChunk *dc = this;
  do {
   if ( dc->freeFirst != intIndexInvalid ) {
    guint i = dc->freeFirst;
    dc->mark(i);
    dc->adjustFreeFirst();
    return dc->entry(i);
   }
   if ( dc->follow ) dc = dc->follow;
   else {
     dc = dc->follow = new ( atomSize ) dyChunk; 
   }
  } while ( 1 );
  return NULL;
}

void  dyChunk::free( guchar* ptr ) {
 if (( chunkStore <= ptr )&&( ptr < chunkStore + atomSize * nAtom )) {
   guint i = (guint)(( ptr - chunkStore )/atomSize);
   unmark(i);
   if ( i < freeFirst ) freeFirst = i;
   if ( empty() ) {
    dyPointer::chainByAtomSize(atomSize)->remove( this ); // from here we can be dead
   }
 } else if ( follow ) follow->free( ptr );
}

void  dyChunk::remove( dyChunk *chunk ) { // removes chunk from the chain if it's not in first position
  if ( follow ) {
    if ( follow == chunk ) {
      std::cout << "removing chunk " << atomSize << std::endl;
      dyChunk *newFollow = follow->follow; delete follow; follow = newFollow;
    }
    else follow->remove(chunk);
  }
}

guchar** dyChunk::getFollower( guchar *ptr ) {

  return (guchar**)(ptr + atomSize-sizeof( guchar* ));
}

gsize dyChunk::getnChunks() {
  return 1 + ( follow ? follow->getnChunks() : 0 );
}

dyChunk  **dyPointer::lowChunk = NULL;
dyChunk  **dyPointer::hiChunk = NULL;
guint     dyPointer::lowMax = 0;
guint     dyPointer::hiMin = 0;
guint     dyPointer::hiMax = 0;

lsLockable*   dyPointer::lowMutex = NULL;
lsLockable*   dyPointer::hiMutex = NULL;
lsLockable   dyPointer::globalMutex;

guint64    dyPointer::totalSize = 0;
static guint undefined_memoryLimit = 0;
guint*      dyPointer::memoryLimit = &undefined_memoryLimit;
dyPointer::handlerOverload * dyPointer::ho = NULL;
bool   dyPointer::inOverload = false;


void   dyPointer::globalinit( guint _hiMin, guint _hiMax ) {

  hiMin = _hiMin;
  hiMax = _hiMax;
  lowMax = (1<<hiMin) - 1;
  lowChunk = new dyChunk*[lowMax];
  lowMutex = new lsLockable[lowMax];
  hiChunk = new dyChunk*[(hiMax-hiMin)+1];
  hiMutex = new lsLockable[(hiMax-hiMin)+1];
  for ( guint i = 1 ; i <= lowMax ; i++ ) {
     lowChunk[i-1] = new ( INDEX_CODE_SIZE*i ) dyChunk;
  }
  for ( guint i = hiMin ; i <= hiMax ; i++ ) {
     hiChunk[i-hiMin] = new ( INDEX_CODE_SIZE*(1<<i) + sizeof( guchar* ) ) dyChunk;
  }
  totalSize = 0;
}

dyChunk *dyPointer::chainByAtomSize( guint atomSize ) {
  
  guint i = atomSize / INDEX_CODE_SIZE;
  if ( i <= lowMax ) return lowChunk[i-1];
  guint ipow = ( atomSize - sizeof( guchar* ) )/INDEX_CODE_SIZE;
  i = 0;
  while ( !( ipow & 1 ) ) { ipow = ipow >> 1; i++; }
  return dyPointer::hiChunk[i-dyPointer::hiMin];
  // sure, a non elligible atomSize value would cause real problems
}

void   dyPointer::globalfree() {

  for ( guint i = 1 ; i < lowMax ; i++ ) {
    delete lowChunk[i-1];
  }
  delete lowChunk;
  delete [] lowMutex;
  for ( guint i = hiMin ; i <= hiMax ; i++ ) {
    delete hiChunk[i-hiMin];
  }
  delete hiChunk;
  delete [] hiMutex;
}

void dyPointer::whereIs( guchar *ptr, guint16 &n, guint16 &i, guint16 &j ) {

  n = guint16invalid;
  for ( guint _n = 0 ; ( _n < lowMax )&&( n == guint16invalid ) ; _n++ ) lowChunk[_n]->whereIs( ptr, n, i, j, _n, false );
  for ( guint _n = 0 ; ( _n <= hiMax-hiMin )&&( n == guint16invalid ) ; _n++ ) hiChunk[_n]->whereIs( ptr, n, i, j, _n+lowMax, true );
}

#define MEM_CHECK_HANDLE( msg ) { std::cout << msg << " at ( " << n << ", " << i << ", " << j << " ) ( " << n2 << ", " << i2 << ", " << j2 << " )\n"; }

void dyPointer::check_access( guchar* ptr, gsize len ) {
  
  if ( !len ) return;
  guchar *ptr2 = ptr+len-1;
  guint16  n, i, j, n2, i2, j2;
  whereIs( ptr, n, i, j );
  whereIs( ptr2, n2, i2, j2 );
  if (( n2 == (guint16)intIndexInvalid )||( n == (guint16)intIndexInvalid )) MEM_CHECK_HANDLE( "Access outside chunk area" );
  if (( j == (guint16)intIndexInvalid )||( j2 == (guint16)intIndexInvalid )) MEM_CHECK_HANDLE( "Access on allocation table" );
  if (( j == (guint16)-2 )||( j2 == (guint16)-2 )) MEM_CHECK_HANDLE( "Access on inter-chunk" );
  if (( j != j2 )||( i != i2 )||( n != n2 )) MEM_CHECK_HANDLE( "Access overlapping two consecutive chunks" );
}

void dyPointer::debug( std::ostream& out ) {

  for ( guint i = 1 ; i < lowMax ; i++ ) {
    { ReadLock lock( &lowMutex[i-1] );
      guint nChunks = lowChunk[i-1]->getnChunks();
      guint nAtoms = lowChunk[i-1]->getnAtom();
      out << "Chunk " << i << ": " << nChunks << " chunks of " << nAtoms << " atoms of size " << lowChunk[i-1]->getAtomSize() << std::endl;
    }
  }
  for ( guint i = hiMin ; i <= hiMax ; i++ ) {
    { ReadLock lock( &hiMutex[i-hiMin] );
      guint nChunks = hiChunk[i-hiMin]->getnChunks();
      guint nAtoms = hiChunk[i-hiMin]->getnAtom();
      out << "Chunk " << i << ": " << nChunks << " chunks of " << nAtoms << " atoms of size " << hiChunk[i-hiMin]->getAtomSize() << std::endl;
    }
  }
}

void dyPointer::alloc( guint16 sz ) {

//  lsStatistics::inc( "dyChunks", (guint32)sz*sizeof(intIndex) );
  guchar **last = &curs;
  guint16 n = hiMax;
  len = sz;


  { WriteLock globalLock( &globalMutex );
    totalSize += len;
  }
  if (( *memoryLimit )&& ho &&( ( mbTotalSize() >= *memoryLimit ) )) ho->handleOverload();
  while ( sz ) {
   if ( sz <= lowMax ) {
    if (( last == &curs )||( sz > (sizeof(guchar*)/INDEX_CODE_SIZE) )) {
     { WriteLock lock(&lowMutex[sz-1]);
       *last = lowChunk[sz-1]->alloc();
     }
    }
    initIterator( len, thislen, n );
    return;
   }
   for ( ; sz < (1<<n) ; n-- ) ;;
   sz -= 1<<n;
   { WriteLock lock( &hiMutex[n-hiMin] );
    *last = hiChunk[n-hiMin]->alloc();
   }
   last = hiChunk[n-hiMin]->getFollower( *last );
  }
  *last = NULL;
  initIterator( len, thislen, n );
  return;
}

void   dyPointer::free() {

  // lsStatistics::dec( "dyChunks", (guint32)len*sizeof(intIndex) );
  { WriteLock globalLock( &globalMutex ); 
    totalSize -= len;
  }
  intIndex sz = len;
  guchar *ptr = curs;
  guchar *next = NULL;
  guint n = hiMax;
  while ( sz ) {
    if ( sz <= lowMax ) {
     if (( !next )||( sz > (sizeof(guchar*)/INDEX_CODE_SIZE) )) {
      { WriteLock lock( &lowMutex[sz-1] );
        lowChunk[sz-1]->free( ptr );
      }
     }
     curs = NULL;
     len = thislen = 0;
     return;
   }
   for ( ; sz < (((intIndex)1)<<n) ; n-- ) ;;
   sz -= 1<<n;
   next = *(hiChunk[n-hiMin]->getFollower( ptr ));
   { WriteLock lock( &hiMutex[n-hiMin] );
     hiChunk[n-hiMin]->free( ptr );
   }
   ptr = next;
  }
  curs = NULL;
  len = thislen = 0;
}

dyPointer::handlerOverload* dyPointer::setHandlerOverload( dyPointer::handlerOverload *handler ) {
  handlerOverload* _ho = ho;
  ho = handler;
  return _ho;
}

void dyPointer::handlerOverload::handleOverload() {
  if ( !dyPointer::inOverload ) { 
    dyPointer::inOverload = true; 
        // need another thread 'cuz here we have been stopped in the middle of ... whatever
    DEBUG_LOG << "overload call : " << mbTotalSize() << " / " << *memoryLimit;
    std::thread( handlerOverload::sHandleOverload, this ).detach();
  }
}

void dyPointer::handlerOverload::sHandleOverload( dyPointer::handlerOverload * ho ) {
  ho->doHandleOverload();
  sleep(2); // we don't authorize cleaning again right now
  dyPointer::inOverload = false;
}
