import trinet
from pytrinet.Configurable import Configurable

# An ArticleMap implements the association between document numeric indexes, which identify documents internally in Trinet core,
# into string urls and/or meta data which are key/value pairs of any kind of meta data to store along with each document.
# The concept of URL here is just a string acting as a unique identifier of the document. It does not need to be an actual URL.
# A url is mapped to a couple (lang,id) : lang identifies the trinet engine in the project, and id is internal to this engine.

class ArticleMap(Configurable):

    def getUrl( self, lang: str, id: int ) -> str:
        pass

    def getMeta( self, lang: str, id: int ) -> dict:
        pass

    def getField( self, lang: str, id: int, field: str, default: str ) :
        pass

    def getId( self, url: str ) -> tuple:  # returns (lang,id) if url exists, or (None,None)
        pass

    def set( self, lang: str, id: int, url: str, meta: dict ):
        pass

    def delete( self, url: str ):
        pass

    def iterator( self ):
        pass

    def get( self, lang: str, id: int ) :
        pass

    def gets( self, lang: str, ids: list ) :
        pass



class SearchResult:

    def __init__( self, map, lang, result ):
        self.map = map
        self.id = result
        self.lang = lang
        self._url = False
        self._meta = False

    def getMap(self):
        if not self._url :
            data = self.map.get(self.lang,self.id)
            self._url = data['url']
            self._meta = data['meta']

    @property
    def url(self):
        self.getMap()
        return self._url

    @property
    def meta(self):
        self.getMap()
        return self._meta

    @property
    def title(self):
        self.getMap()
        return self._meta['title'] if 'title' in self._meta else ''

class SearchResults:

    length: int

    def __init__( self, list, map: ArticleMap, lang: str ):
        self.list = list.getIdTuple() if list else ()
        self.maxScore = list.getMaxScore() if list else 0
        self.map = map
        self.lang = lang
        self.i = 0
        self.length = len( self.list )
        self.maxLength = self.length

    def offsetLimit( self, offset, limit ) :
        self.i = min( offset, self.maxLength )
        self.length = min( limit, self.maxLength )
        return self
    
    def __len__(self):
        return len( self.list )

    def sortBy( self, order ) :
        if order :
            (field,direction) = order.split(':',2)
            direction = int(direction)
            if direction != 1 and direction != -1 :
                raise Exception('Invalid format for order : should be "field:direction" where direction is 1 or -1')

            self.list = self.map.sortBy( self.lang, self.list, field, direction )

        return self

    def __iter__(self):
        self.i = 0
        return self

    def __next__(self):
        if ( self.i >= self.length ) : raise StopIteration
        self.i += 1
        return SearchResult(self.map,self.lang,self.list[self.i-1])
