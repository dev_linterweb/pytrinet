
import datetime
import hashlib
from io import StringIO
import re
import time
import requests
from urllib.parse import quote_plus
from pytrinet.ApiClient import ApiClient
from pytrinet.explorer.Explorer import Explorer
from pymongo import MongoClient
from bson.objectid import ObjectId

class MastodonExplorer(Explorer):

    parameters = (
        ('instance',''), # if not empty, contains the instance url we are currently crawling e.g. https://oc.todon.fr/
                        # if "pickone": asks the explorer to start cycling through instances.
        ('iterator','0'), # Next value of id in mastodon timeline api
        ('autoSave','100'),
        ('apiLimit','50'),
        ('mongoUrl','mongodb://127.0.0.1:27017'),
        ('mongoDatabase','resident'),
        ('method','recent'), # recent: crawl from latest toot to the first toot we already have
            # whole: crawl from the beginning of time to now. This type of crawl let us delete removed toots
        ('exportLinksApi',''), # the url of a pytrinet api to export external links
        ('exportLinksProject','') # the project name of a pytrinet api to export external links
    )

    def __init__( self, project, config, id ):
        super().__init__( project, config, id )
        self.useTimestamps = True
        self.retryCount = 0
        mongodb_client = MongoClient(self.mongoUrl)
        database = mongodb_client[self.mongoDatabase]
        self.collection = database['mastodon']
        self.failures = 0
        self.recentId = 0
        self.instanceJustStarted = False

    def urlApiListAllToots(self) :
        if self.method == 'whole' :
            return '{}/api/v1/timelines/public?local=true&remote=false{}&only_media=false&limit={}'.format(
                self.instance,
                '&min_id='+self.iterator,
                self.apiLimit
            )
        elif self.method == 'recent' :
            return '{}/api/v1/timelines/public?local=true&remote=false{}&only_media=false&limit={}'.format(
                self.instance,
                '&max_id='+self.iterator if self.iterator!='0' else '',
                self.apiLimit
            )
        else :
            raise Exception('Field method has invalid value {}'.format(self.method))

    def getHash( self ) :
        return ( hashlib.new( 'ripemd160', self.instance.encode() ).hexdigest() )[0:24]

    def pickOneInstance(self) :
        with self.project.explorerLock :
            self.failures = 0
            now = int(time.time())

            field = {'recent':'last_crawl','whole':'last_whole_crawl'}[self.method]

            result = self.collection.find_one({
                '$or':[
                    {field:0},
                    {field:{'$exists':False}}
                ],
                'fail_count':{'$lt':20},'last_fail':{'$lt':(now-12*3600)}
            })
            if not result : 
                result = self.collection.find_one({'fail_count':{'$lt':20},'last_fail':{'$lt':(now-12*3600)}},sort=[(field,1)])
            if result :
                self.instance = result['url']
                self.iterator = '0'
                self.recentId = result['recent_id'] if 'recent_id' in result else 0
                self.instanceJustStarted = True
                self.log("Picked instance {}".format(self.instance))
                self.collection.update_one(
                    { '_id':ObjectId(self.getHash()) },
                    { '$set': { field:now } }
                )
            else :
                self.log("No pickable instance yet, waiting ...")
            return bool(result)

    def resetFailureCount(self) :
        self.collection.update_one(
            { '_id':self.getHash() },
            { '$set': { 'fail_count':0 } }
        )

    def storeRecentId(self,id) :
        self.log("Storing recent id for {} : {}".format(self.instance,id))
        self.collection.update_one(
            { '_id':ObjectId(self.getHash()) },
            {
                '$set':{
                    'recent_id':id
                }
            }
        )


    def markFailure(self) :
        self.log("Marking one failure on {}".format(self.instance))
        now = int(time.time())
        self.collection.update_one(
            { '_id':ObjectId(self.getHash()) },
            {
                '$inc':{
                    'fail_count':1
                },
                '$set':{
                    'last_fail':now
                }
            }
        )
        self.instance = 'pickone'

    def doExplore(self) :
        if not self.instance :
            return False

        if self.instance == 'pickone' : 
            if not self.pickOneInstance() : return False

        if not self.iterator : self.iterator = '0'

        listUrl = self.urlApiListAllToots()
        self.log("Getting {}".format(listUrl))
        try :
            r = requests.get( listUrl, timeout=6, headers={'User-Agent': 'https://toots.cat'} )
            r.raise_for_status()
            results = r.json()
            self.log("Got {} toots".format(len(results)))
            if len(results) == 0 :
                self.instance = 'pickone'
                return False

        except Exception :
            if self.failures > 2 :
                self.markFailure()
            else :
                self.failures += 1
            return False

        self.failures = 0
        self.resetFailureCount()
        for result in results :
            # print(json.dumps(result,indent=2))
            id = int(result['id'])

            if self.method == 'recent' and self.instanceJustStarted :
                self.storeRecentId( id )
            self.instanceJustStarted = False

            if id <= self.recentId and self.method == 'recent' :
                self.instance = 'pickone'
                self.log("Got out of recent posts at id {} (marker recent_id)".format(id))
                return True

            url = result['url']
            author = result['account']['username']
            author_display = result['account']['display_name']
            author_avatar = result['account']['avatar']
            media = result['media_attachments']
            image = None
            preview_image = None
            for m in media :
                if m['type'] == 'image':
                    image = m['url']
                    preview_image = m['preview_url']
                    break

            created_at = 0
            try :
                created_at = result['created_at']
                created_at = re.match(r'([0-9]{4}-[0-9]{2}-[0-9]{2})T([0-9]{2}:[0-9]{2})',created_at) if created_at else None
                created_at = "{} {}".format( created_at.group(1), created_at.group(2) ) if created_at else None
                created_at = datetime.datetime.strptime(created_at, '%Y-%m-%d %H:%M') if created_at else None
                created_at = int(datetime.datetime.timestamp( created_at )) if created_at else None
            except Exception as e:
                pass

            self.log("Got id={} ; timestamp={} url={}".format(id,created_at,url))
            if self.method == 'whole' :
                if int(self.iterator) < id :
                    self.iterator = str(id)
            elif self.method == 'recent' :
                if self.iterator == '0' or int(self.iterator) > id :
                    self.iterator = str(id)
            self.log("Content = {}".format(result['content']))
            body = StringIO(
                '<html><body>{}</body></html>'.format(result['content'])
            )

            # Posting external links if any export search engine defined
            if self.exportLinksApi and self.exportLinksProject :
                links = filter(
                    lambda link : not link.startswith(self.instance),
                    re.findall(r"href\s*=\s*\"\s*(https?://[^\"\s]+)\s*\"",result['content'])
                )

                if links :
                    client = ApiClient( self.exportLinksApi ) if self.exportLinksApi != 'self' else False
                    for link in links :
                        try:

                            if client :
                                client.post( self.exportLinksProject, 'push', {
                                    'url':link,
                                    'body':'',
                                    'meta':{'mastodon':url,'created_at':created_at},
                                    'stamp':created_at//32 if created_at else 0
                                } )
                            else :
                                project = self.project.projectArray.getProject(self.exportLinksProject)
                                self.log('Pushing to {} : {}'.format(self.exportLinksProject,link))
                                project.explorers[0].push( 
                                    link, '',
                                    meta = {'mastodon':url,'created_at':created_at},
                                    stamp = created_at//32 if created_at else 0
                                )
                                self.log('Pushed {}'.format(link))
                        except Exception as e:
                            self.log('Could not push mastodon external link {}'.format(link))

            lang = result['language']
            if not lang in self.project.langs :
                self.log('Language {} is not supported by this project'.format(lang))
            elif not result['content'] :
                self.log('Dropping empty content')
            else :
                now = int(time.time())
                meta = {
                    'body':result['content'],
                    'author':author,
                    'dauthor':author_display,
                    'aauthor':author_avatar,
                    'instance':self.instance,
                    'created_at':created_at,
                    'crawled':now
                }
                if image : 
                    meta['image'] = image
                    meta['preview_image'] = preview_image
                    self.log("Storing image {}".format(image))
                isAnUpdate = not self.project.pushDocument( 
                    url, 
                    body, 
                    meta=meta,
                    lang=lang, 
                    metaWords=('author:'+author,'instance:'+self.instance),
                    logger=self,
                    stamp = created_at//32 if created_at else 0 # stamps are timestamps/32 (32-bit safe for a few centuries)
                )
                if isAnUpdate and ( self.method == 'recent' ) :
                    self.instance = 'pickone'
                    self.log("Got out of recent posts at id {}".format(id))
                    return True
        
        self.log("Next iterator = {}".format(self.iterator))
        time.sleep(2)
        return True
