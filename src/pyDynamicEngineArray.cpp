#include "pyDynamicEngineArray.h"

#include <structmember.h>

#include "dynengineArray.h"
#include "charPtrArray.h"



static PyMemberDef dynamicEngineArray_members[] = {
    {NULL}
};


static void
PyDynamicEngineArray_dealloc(PyDynamicEngineArray *self) {
    if ( self->engineArray ) delete self->engineArray;
    Py_TYPE(self)->tp_free((PyObject *) self);
}

static PyObject *
PyDynamicEngineArray_new(PyTypeObject *type, PyObject *args, PyObject *kwds) {
    PyDynamicEngineArray *self;
    self = (PyDynamicEngineArray *) type->tp_alloc(type, 0);
    self->engineArray = NULL;
    return (PyObject *) self;
}

static int
PyDynamicEngineArray_init(PyDynamicEngineArray *self, PyObject *args, PyObject *kwds) {

    static const char * kwConst[] = {"path", "lang", NULL};
    static CharPtrArray kwlist( kwConst );
    PyObject *path_obj = NULL, *lang_obj = NULL;
    const char *path, *lang;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "O&O", kwlist.get(),
                                     PyUnicode_FSDecoder, &path_obj, &lang_obj))
        return -1;

    if ( path_obj != Py_None ) {
        path = PyUnicode_AsUTF8( path_obj );
    } else {
        return -1;
    }

    if ( lang_obj != Py_None ) {
        lang = PyUnicode_AsUTF8( lang_obj );
    } else {
        return -1;
    }

    if ( ! self->engineArray ) self->engineArray = new lsDynEngineArray();
    self->engineArray->init( path, lang );

    return 0;
}

static PyMethodDef dynamicEngineArray_methods[] = {
/*    {"test", (PyCFunction) PyDynamicEngine_test, METH_VARARGS,
     "Return the key 'coucou' from the configs dictionary"
    },*/
    {NULL}  /* Sentinel */
};

PyTypeObject DynamicEngineArrayType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "trinet.DynamicEngineArray",
    .tp_basicsize = sizeof(PyDynamicEngineArray),
    .tp_itemsize = 0,
    .tp_dealloc = (destructor) PyDynamicEngineArray_dealloc,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
    .tp_doc = PyDoc_STR("Dynamic Trinet search engine instance"),
    .tp_methods = dynamicEngineArray_methods,
    .tp_members = dynamicEngineArray_members,
    .tp_init = (initproc) PyDynamicEngineArray_init,
    .tp_new = PyDynamicEngineArray_new,
};
