

from curses import curs_set


class Tokenizer :

    class BufferOverflowException(Exception):
        pass

    buffer:str=''
    curs:int=0
    maxBufferLength:int=128*1024

    # Helper method that keeps a buffer of text. Advancing curs indicates that text before curs has been treated
    def pushText( self, text: str ):
        self.buffer = self.buffer[self.curs:]+text
        self.curs = 0
        if len(self.buffer) > self.maxBufferLength :
            print('buffer overflow : ',len(self.buffer))
            raise Tokenizer.BufferOverflowException()

    # Jump to the end of buffer, letting n characters left for next step
    def seekToEnd( self, n ):
        self.curs = len(self.buffer)-n
        if self.curs < 0 : self.curs = 0

    def seekNextLine( self ):
        current = self.curs
        while self.curs < len(self.buffer) and self.buffer[self.curs] != '\n' : self.curs += 1
        if self.curs < len(self.buffer) : 
            self.curs += 1
            return True
        else :
            self.curs = current
            return False

    def getLine( self ):
        begin = self.curs
        self.seekNextLine()
        return self.buffer[ begin : self.curs ]


    def tokenize( self, text: str, flush: bool ): # flush means we're done, no more text is coming afterwards
        pass