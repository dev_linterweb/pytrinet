#include "charPtrArray.h"

#include <string.h>

CharPtrArray::CharPtrArray( const char ** strings ) {
    int length = 0;
    int count = 0;
    for ( int i = 0 ; strings[i] ; i++ ) {
        length += strlen(strings[i])+1;
        count ++;
    }
    text = new char[length];
    ptrs = new char*[count+1];

    char * curs = text;
    for ( int i = 0 ; strings[i] ; i++ ) {
        strcpy( curs, strings[i] );
        ptrs[i] = curs;
        curs += strlen(strings[i])+1;
    }
    ptrs[count] = NULL;
}
