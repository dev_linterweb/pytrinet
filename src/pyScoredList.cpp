#include "pyDynamicEngine.h"

#include <structmember.h>
#include <algorithm>

#include "pyScoredList.h"

#define TAKE_AVERAGE_ON 5

static long maxListSizeToPython = 1024;

static PyMemberDef scoredList_members[] = {
    {NULL}
};


static void
PyScoredList_dealloc(PyScoredList *self) {
    self->list.free();
    Py_TYPE(self)->tp_free((PyObject *) self);
}

static PyObject *
PyScoredList_new(PyTypeObject *type, PyObject *args, PyObject *kwds) {
    PyScoredList *self;
    self = (PyScoredList *) type->tp_alloc(type, 0);
    self->list = listIndex();
    return (PyObject *) self;
}

static int
PyScoredList_init(PyScoredList *self, PyObject *args, PyObject *kwds) {
    return 0;
}

static PyObject *
PyScoredList_get(PyScoredList *self, PyObject *args) {

    unsigned long id;

    if (!PyArg_ParseTuple(args, "k", &id)) {
        PyErr_SetString(PyExc_TypeError, "Expecting an id");
        return NULL;
    }

    unsigned long element = self->list.element(id);
    unsigned long count = self->list.count(id);

    return Py_BuildValue("kk", element, count );
}

static PyObject *
PyScoredList_setScore(PyScoredList *self, PyObject *args) {

    unsigned long id;
    unsigned long score;

    if (!PyArg_ParseTuple(args, "kk", &id, &score )) {
        PyErr_SetString(PyExc_TypeError, "Expecting an id and a score, both long integers");
        return NULL;
    }

    if (( id >= (unsigned long)self->list.length() )||( id < 0 )) {
        PyErr_SetString(PyExc_TypeError, "id out of range");
        return NULL;
    }

    self->list.setCount( id, score );

    Py_RETURN_NONE;
}

static PyObject *
PyScoredList_sortDown(PyScoredList *self) {
    self->list.sortCounts();
    Py_RETURN_NONE;
}

static PyObject *
PyScoredList_length(PyScoredList *self) {
    return PyLong_FromLong( self->list.length() );
}

static PyObject *
PyScoredList_getWindow(PyScoredList *self, PyObject *args) {

    unsigned long i,j;

    if (!PyArg_ParseTuple(args, "kk", &i,&j)) {
        PyErr_SetString(PyExc_TypeError, "Expecting two integers");
        return NULL;
    }

    intIndex n = self->list.length();
    if ( j > n ) j = n;
    if ( i > n ) i = n;
    if ( j-i < 0 ) j = i;

    PyObject * tuple = PyTuple_New( j-i );

    for ( intIndex k = i ; k < j ; k++ ) {

        unsigned long element = self->list.element(k);
        unsigned long count = self->list.count(k);

        PyTuple_SetItem(
            tuple, 
            k-i,
            Py_BuildValue( "kk", element, count )
        );
    }
    return tuple;
}

static PyObject *
PyScoredList_getIdTuple(PyScoredList *self) {

    intIndex n = std::min( self->list.length(), maxListSizeToPython );

    PyObject * tuple = PyTuple_New( n ); // new reference

    for ( intIndex k = 0 ; k < n ; k++ ) {

        unsigned long element = self->list.element(k);

        PyTuple_SetItem(
            tuple,
            k,
            PyLong_FromUnsignedLong( element ) // new reference stolen by PyTuple_SetItem
        );
    }
    return tuple; // new reference is passed to the caller
}

static PyObject *
PyScoredList_getMaxScore(PyScoredList *self) {
    intIndex score = 0;
    for ( long i = 0 ; i < std::min( (long)TAKE_AVERAGE_ON, self->list.length() ) ; i++ ) {
        score += self->list.count(i);
    }
    return PyLong_FromLong( score / TAKE_AVERAGE_ON );
}

static PyMethodDef scoredList_methods[] = {
    {"sortDown", (PyCFunction) PyScoredList_sortDown, METH_NOARGS,
     "Sorts elements by decreasing scores. You may want to call this after using setScore which may break the ordering."
    },
    {"setScore", (PyCFunction) PyScoredList_setScore, METH_VARARGS,
     "Sets the score of element at position i. Parameter i is integer. Parameter score is an integer."
    },
    {"get", (PyCFunction) PyScoredList_get, METH_VARARGS,
     "Gets the tuple (element,score) at position i. Parameter i is integer."
    },
    {"length", (PyCFunction) PyScoredList_length, METH_NOARGS,
     "Gets the size of this list."
    },
    {"getWindow", (PyCFunction) PyScoredList_getWindow, METH_VARARGS,
     "Gets a tuple of tuples (element,score) from position i (included) to j (excluded)."
    },
    {"getIdTuple", (PyCFunction) PyScoredList_getIdTuple, METH_NOARGS,
     "Gets a tuple of all elements, without scores."
    },
    {"getMaxScore", (PyCFunction) PyScoredList_getMaxScore, METH_NOARGS,
     "Gets the maximal score of the list, or 0 if list is empty."
    },
    
    {NULL}  /* Sentinel */
};

PyTypeObject ScoredListType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "trinet.ScoredList",
    .tp_basicsize = sizeof(PyScoredList),
    .tp_itemsize = 0,
    .tp_dealloc = (destructor) PyScoredList_dealloc,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
    .tp_doc = PyDoc_STR("Trinet list of tuples (element,score)"),
    .tp_methods = scoredList_methods,
    .tp_members = scoredList_members,
    .tp_init = (initproc) PyScoredList_init,
    .tp_new = PyScoredList_new,
};

PyObject * PyScoredList_FromListIndex( listIndex list ) {
    
    PyScoredList * self = PyObject_New( PyScoredList, &ScoredListType );
    self->list = list;
    return (PyObject*)self;
}
