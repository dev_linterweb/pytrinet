#include "dymap.h"
#include "dynengine.h"

wordMapDynamic::wordMapDynamic()
               : mapWord() {

  fileLock = new lsLockable();

  for ( intIndex i = 0 ; i < WORD_HACK_SIZE ; i++ ) {
    hackEntry[i] = NULL; szEntry[i] = szMaxEntry[i] = 0;
  }
  syncState = false;
}

wordMapDynamic::~wordMapDynamic() {

  for ( int i = 0 ; i < WORD_HACK_SIZE ; i++ ) if ( hackEntry[i] ) free(hackEntry[i]);
  delete fileLock;
}

intIndex wordMapDynamic::load( const char *fileName ) {

  attachToFile( fileName );
  { WriteLock _(this);
    ReadLock __(fileLock);

    openFile( false );
    loadHeader();

    intIndex  offset[WORD_HACK_SIZE+1];

    if ( !lsOffsetFile::load<intIndex>( *offset, (WORD_HACK_SIZE+1)*sizeof(intIndex) ) ) {
      DEBUG_LOG << fileName << " offset table unreadable";
      size = 0;
      closeFile();
      return 0;
    }

    list.alloc( size );
    for ( intIndex idx = 0 ; idx < size ; idx++ ) list[idx].offset = 0;

    for ( intIndex i = 0 ; i < WORD_HACK_SIZE ; i++ ) {
      intIndex sz = offset[i+1]-offset[i];
      if ( sz > WORD_MAP_HACK_MAX_SIZE ) {
        DEBUG_LOG << fileName << " unreadable from word #" << i;
      }
      if ( sz > sizeof(intIndex) ) {
        szEntry[i] = sz-sizeof(intIndex);
        szMaxEntry[i] = (intIndex)(DYLIST_RATIO_ALLOC*(sz+sizeof(intIndex)));
        hackEntry[i] = (char*)malloc( szMaxEntry[i]+3*sizeof(intIndex) );
  //      lsStatistics::inc( "wordMap", szMaxEntry[i]+3*sizeof(intIndex) );
        lsOffsetFile::load<char>( *hackEntry[i], sz );

        char *curs = hackEntry[i];
        intIndex index;
        while ( (index = *((intIndex*)curs)) != intIndexInvalid ) {

          curs += sizeof(intIndex);
          if ( index < size ) { // empty entry code
          list[index].offset = curs-hackEntry[i];
          list[index].hack = i;
          } else {
            ((intIndex*)curs)[-1] = intIndexInvalid-1;
            curs[5]=0;
          }
          curs += sizeof(intIndex);
          if ( version < 3 ) {
          while ( *curs ) { curs++; } curs++;
          } else {
          curs += *((guchar*)curs)+1;
          }
        }
        // Le marqueur de fin est la succession de 3 guint32 à intIndexInvalid
        curs += sizeof(intIndex);
        *((intIndex*)curs) = intIndexInvalid;
        curs += sizeof(intIndex);
        *((intIndex*)curs) = intIndexInvalid;
      } else { intIndex dummy;
              lsOffsetFile::load<intIndex>( dummy, sz );
              szEntry[i] = szMaxEntry[i] = 0; hackEntry[i] = NULL; }
    }

    if ( version < 3 ) convertFromVersion_1();

    syncState = true;
    closeFile();
  }
  return length();
}

void wordMapDynamic::save() {

  if ( syncState ) return;

  { ReadLock _(this);
    WriteLock __(fileLock);

    // Backup 

    try {
      std::filesystem::path from( fileName );
      std::filesystem::path to( from ); to += ".bak";
      std::filesystem::rename( from, to );
    } catch (std::filesystem::filesystem_error& e) {
      std::cout << e.what() << '\n';
    }

    // Save

    openFile( true );

    size = length();
    writeHeader();

    intIndex  offset = 0x0A + (WORD_HACK_SIZE+1)*sizeof(intIndex);
    for ( int i = 0 ; i < WORD_HACK_SIZE ; i++ ) {
      lsOffsetFile::save<intIndex> ( offset );
      intIndex sz = szEntry[i]+sizeof(intIndex);
      offset += sz;
    }
    lsOffsetFile::save<intIndex> ( offset );
  
    for ( int i = 0 ; i < WORD_HACK_SIZE ; i++ ) {
      gsize sz = szEntry[i];
      if ( sz ) lsOffsetFile::save<char> ( *hackEntry[i], sz );
      intIndex endCode = intIndexInvalid;
      lsOffsetFile::save<intIndex> ( endCode );
    }

    syncState = true;
    closeFile();
  }
}

void wordMapDynamic::convertFromVersion_1() {
 // Conversion version 1,2 -> version 3 : chaque chaine est convertie de zstring vers chaine avec
 // octet préfixe de longueur

 for ( intIndex i = 0 ; i < WORD_HACK_SIZE ; i++ ) {
  char *c = hackEntry[i];
  if ( c ) {
   while ( *((intIndex*)c) != intIndexInvalid ) {
    c += 8;
    guint len = strlen( c );
    memmove( c+1, c, len );
    *c = (guchar)len;
    c += len+1;
   }
  }
 }
}

lsBufferConst wordMapDynamic::getWord( intIndex idx ) {

  if (( idx >= length() )|| ! list[idx].offset ) return lsBufferConst("");
  char *c = hackEntry[list[idx].hack] + list[idx].offset + sizeof(intIndex);
  intIndex len = *((guchar*)c);

  return lsBufferConst( c + 1, len );
}

intIndex wordMapDynamic::getNCorpus( intIndex idx ) {

  if (( idx >= length() )|| ! list[idx].offset ) return 0;
  return *((intIndex*)(hackEntry[list[idx].hack] + list[idx].offset));
}

char * wordMapDynamic::getEntry( intIndex hack ) {

  return hackEntry[hack];
}

void wordMapDynamic::insert( intIndex idx, const char * word ) {

  if ( idx > length() ) return;
  syncState = false;
  if ( idx == list.size ) list.grow();
  
  intIndex hack = list[idx].hack = getHack( word );
  gsize lenWord = strlen( word );

  // -- Parse this hack entry searching for a dead entry with the good length
  if ( szEntry[hack] ) {
   guchar *curs = (guchar*)hackEntry[hack];
   while (( ((intIndex*)curs)[2] != intIndexInvalid )&&(( *(intIndex*)curs != intIndexInvalid-1 )||( curs[8] != lenWord )))
     curs += curs[8]+9;
   if ( ((intIndex*)curs)[2] != intIndexInvalid ) { // found one - we take it
    memcpy( curs+9, word, lenWord );
    ((intIndex*)curs)[0] = idx;
    ((intIndex*)curs)[1] = 0; // reset the number of occurences
    list[idx].offset = (char*)curs+4 - hackEntry[hack];
    return;
   }
  }

  // -- Didn't find one, we need to grow the hack entry
  gsize newLen = szEntry[hack] + lenWord + 1 + 2*sizeof(intIndex);
  if ( newLen > szMaxEntry[hack] ) {

    szMaxEntry[hack] = (gsize)(DYLIST_RATIO_ALLOC*newLen);
    if ( szMaxEntry[hack] < DYLIST_MIN_ALLOC ) szMaxEntry[hack] = DYLIST_MIN_ALLOC;
    if ( hackEntry[hack] ) 
      hackEntry[hack] = (char*)realloc( hackEntry[hack], szMaxEntry[hack]+3*sizeof(intIndex) );
    else hackEntry[hack] = (char*)malloc( szMaxEntry[hack]+3*sizeof(intIndex) );
  }
  // -- then add the data at the end
  *((intIndex*)(hackEntry[hack] + szEntry[hack])) = idx;
  szEntry[hack] += sizeof(intIndex);
  *((intIndex*)(hackEntry[hack] + (list[idx].offset = szEntry[hack]))) = 0;
  szEntry[hack] += sizeof(intIndex);
  *(hackEntry[hack] + szEntry[hack]) = (guchar)lenWord;
  szEntry[hack] ++;
  strcpy( hackEntry[hack] + szEntry[hack], word );
  szEntry[hack] += lenWord;
  // -- put the end marker after szEntry : 3 times intIndexInvalid (guint32)
  *((intIndex*)(hackEntry[hack] + szEntry[hack])) = intIndexInvalid;
  ((intIndex*)(hackEntry[hack] + szEntry[hack]))[1] = intIndexInvalid;
  ((intIndex*)(hackEntry[hack] + szEntry[hack]))[2] = intIndexInvalid;
}

intIndex wordMapDynamic::push( const char * word, guint& nWordInsert ) {

  intIndex id = intIndexInvalid;
  { WriteLock _(this);
    id = getIndex( word );

    if ( id == intIndexInvalid ) {
      if ( !nWordInsert ) { return intIndexInvalid; }
      for ( id = 0 ; ( id < length() )&& list[id].offset ; id ++ ) ;; // Search an unused id
      if ( id > MAX_INDEX ) { return intIndexInvalid; }
      nWordInsert--;
      insert( id, word );
    }

    intIndex nCorpus = getNCorpus( id );
    if ( nCorpus > 2*BASE_OCCURENCE ) {
      divNCorpus();
      nCorpus = getNCorpus( id );
    }
    (*((intIndex*)(hackEntry[list[id].hack]+list[id].offset))) = nCorpus+1;
    syncState = false;
  }
  
  return id;
}

void wordMapDynamic::divNCorpus() {

  for ( intIndex id = 0 ; id < length() ; id++ )
    (*((intIndex*)(hackEntry[list[id].hack]+list[id].offset))) = (getNCorpus(id)>>1)+1;
  syncState = false;
}

void wordMapDynamic::cutWords( intIndex *tWords, intIndex oldLen, intIndex newLen ) {

  syncState = false;
  for ( intIndex hack = 0 ; hack < WORD_HACK_SIZE ; hack++ ) {
   if ( hackEntry[hack] ) {
    char *curs = hackEntry[hack];
    char *dest = hackEntry[hack];
    intIndex index;
    while ( (index = *((intIndex*)curs)) != intIndexInvalid ) {
      curs += sizeof( intIndex );
      intIndex nCorpus = *((intIndex*)curs);
      curs += sizeof( intIndex );
      guint wLen = *(guchar*)curs;
      char *str = curs;
      curs += wLen+1;

      intIndex new_e = ( index < oldLen )? tWords[index] : index - oldLen + newLen;
      if ( new_e != intIndexInvalid ) {
       *((intIndex*)dest) = new_e;
       dest += sizeof( intIndex );
       list[new_e].offset = dest-hackEntry[hack];
       list[new_e].hack = hack;
       *((intIndex*)dest) = nCorpus;
       dest += sizeof( intIndex );
       memmove( dest, str, wLen+1 );
       dest += wLen+1;
      }
    }
    szEntry[hack] = dest-hackEntry[hack];
    *((intIndex*)dest) = intIndexInvalid; // Marqueurs de fin --
    ((intIndex*)dest)[1] = intIndexInvalid;
    ((intIndex*)dest)[2] = intIndexInvalid;
   }
  }
  list.size = newLen;
}

void wordMapDynamic::debug( intIndex id ) {

  if ( id >= length() ) std::cout << "Unreachable" << std::endl;
  else std::cout << getWord(id) << " : (" << getNCorpus(id) << ")" << std::endl;
}

void wordMapDynamic::debugReverse( char *word ) {

  intIndex id = getIndex( word );
  if ( id == intIndexInvalid ) std::cout << "Unknown" << std::endl;
  else std::cout << id << std::endl;
}

bool wordMapDynamic::removeWord( intIndex id ) {

  if ( ! list[id].offset ) return false;
  syncState = false;
  guchar *curs = (guchar*)hackEntry[list[id].hack]+list[id].offset;
  curs[5] = 0; // code 0 at first position of the word to make it invisible
  list[id].offset = 0;
  ((intIndex*)curs)[-1] = intIndexInvalid-1;
  return true;
}

void wordMapDynamic::shrink( lsShrinker wShrinker ) {

  for ( intIndex i = 0 ; i < WORD_HACK_SIZE ; i++ ) {
    char *curs = hackEntry[i];
    char *dest = curs;
    if ( szEntry[i] ) {
      intIndex id;
      while ((id=((intIndex*)curs)[0]) != intIndexInvalid ) {
        intIndex targetId = wShrinker.rewrite( id );
        intIndex thisLen = 9+(guchar)(curs[8]);
        if ( targetId != intIndexInvalid ) {
          ((intIndex*)curs)[0] = targetId;
          if ( dest != curs ) memmove( dest, curs, thisLen );
          list[targetId].hack = i;
          list[targetId].offset = dest-hackEntry[i]+4;
          dest += thisLen;
        }
        curs += thisLen;
      }
      szEntry[i] = dest-hackEntry[i];
      *((intIndex*)dest) = intIndexInvalid; // Marqueurs de fin --
      ((intIndex*)dest)[1] = intIndexInvalid;
      ((intIndex*)dest)[2] = intIndexInvalid;
    }
  }
  list.size = wShrinker.targetLength();
  syncState = false;
}

void wordMapDynamic::checkDuplicates() {

  { WriteLock _(this);
    for ( intIndex i = 0 ; i < WORD_HACK_SIZE ; i++ ) {
      char *curs = hackEntry[i];
      if ( szEntry[i] ) {
        intIndex id;
        while ((id=((intIndex*)curs)[0]) != intIndexInvalid ) {
          intIndex thisLen = 9+(guchar)(curs[8]);
          if (( id != intIndexInvalid-1 )&&(( list[id].hack != i )||( list[id].offset != curs+4-hackEntry[i] ))) {
            char *ocurs = hackEntry[list[id].hack]+list[id].offset ;
            DEBUG_LOG << "Word conflict " << lsBufferConst( curs+9, (guchar)(curs[8]) ) << " : ( " << i << ", " << curs+4-hackEntry[i] << " ) against ( " << list[id].hack << ", " << list[id].offset << " ) : " << lsBufferConst( ocurs+5, (guchar)(ocurs[4]) );
            syncState = false;
            *((intIndex*)curs) = intIndexInvalid-1;
            curs[9]=0;
          }
          curs += thisLen;
        }
      }
    }
  }
}

