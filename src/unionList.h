#ifndef _UNION_LIST_H_
#define _UNION_LIST_H_
 
#include "compactList.h"
#include "index.h"

template <class Tpointer>
class unionList {

public:
  unionList( listIndex l, compactIndex<Tpointer> *_index ); 
   /**< Initialiser cette unionList avec les compactList de l'index _index dont les num&eacute;ros sont
     donn&eacute;s par l. */
  unionList( compactIndex<Tpointer> *_index ); 
   /**< Initialiser cette unionList avec TOUTES les compactList de l'index _index. Utilisé par la méthode lsEngine::classify. 
    * Ne pas utiliser sur un index normal : il risque de contenir beaucoup trop de compactList.
   */
  ~unionList(); ///< Destructeur
  unionList<Tpointer>& setLazy( bool lazy ) { bLazy = lazy; return *this; }
  unionList<Tpointer>& setIntersect( bool intersect ) { bIntersect = intersect; return *this; }
  void add( compactList<Tpointer> l ); ///< Je sais plus ...
  listIndex reunion(intIndex *scoreMap, intIndex listMax);
   /**< R&eacute;alise l'union pond&eacute;r&eacute;e des compactlist en convertissant les codes de scores au
     moyen de scoreMap. Le r&eacute;sultat est retourn&eacute; sous la forme d'une nouvelle listIndex 
     de taille maximale listMax. */
  listIndex reunionIntersectWithScoreAdd(intIndex *scoreMap, listIndex inter);
   /**< R&eacute;alise l'union pond&eacute;r&eacute;e des compactList en convertissant les codes de scores au
     moyen de scoreMap. Seuls les &eacute;l&eacute;ments pr&eacute;sents dans inter sont retenus. Les autres sont
     supprim&eacute;s. (on intersecte l'union avec inter).
   */
  listIndex intersect( intIndex* scoreMap = scoreMapNull );
   /**< R&eacute;alise l'intersection des compactList en convertissant les codes de scores au moyen
     de scoreMap. */
  listIndex weakIntersect( 
    intIndex* scoreMap, 
    listIndex l, 
    intIndex maxScore, 
    intIndex minScore, 
    guint maxPages, 
    guint minPages
  );
  /**< R&eacute;alise l'"intersection faible" des compactList.
    La liste l doit &ecirc;tre la liste fournie au constructeur. Elle donne un score &agrave; chacune des listIndex.
    maxScore doit &ecirc;tre le score maximal affect&eacute; &agrave; ces compactList.
    Si l'intersection stricte des compactList est de taille sup&eacute;rieure &agrave; minPages, alors celle-ci 
    est retourn&eacute;e.
    Sinon, pour chaque &eacute;l&eacute;ment, cet &eacute;l&eacute;ment apparait dans un certain nombre de listCompact. 
    Il est retenu si la somme des scores des listIndex qui le contiennent d&eacute;passe minScore.
    On retourne les minPages &eacute;l&eacute;ments retenus de meilleur score.
  */
  listIndex weakIntersectHomogeneous( 
    intIndex* scoreMap, intIndex maxScore, intIndex minScore, guint maxPages, guint minPages );
  /**< Same as weakIntersect, but compactList's are not weighted by parameter [l]  */
  
  listIndex weakIntersectWithIntersect( 
    intIndex* scoreMap, 
    listIndex l, 
    intIndex maxScore, 
    intIndex minScore, 
    guint maxPages, 
    guint minPages, 
    guint realIntersect 
  );
	/**< Same as weakIntersect, but the first realIntersect lists do a real intersection */

  unionList<Tpointer> & useReordering( const std::vector<intIndex> * _reordering ) {
    reordering = _reordering;
    return *this;
  }

  listIndex classify( compactList<Tpointer> document, intIndex keepMax = 3, double keepOver = 0.95 );
  /**< Get the compactList id that is the most similar to document */

  listIndex classify( listIndex document, intIndex keepMax = 3, double keepOver = 0.95 );
  /**< Get the compactList id that is the most similar to document */

protected:
  void       dummytest();
  void       initIterators();
  void       releaseIterators();
  compactIterator<Tpointer> *list;
  intIndex  nList;
  bool      bLazy, bIntersect;
  compactIndex<Tpointer> *index;
  ReadLock  * indexLock;
  listIndex  baseList;
  bool      ownBaseList;
  const     std::vector<intIndex> * reordering;

  intIndex  reorder( intIndex id, intIndex score ) const {
    return reordering
      ? ( id < reordering->size() ) 
        ? (*reordering)[id] 
        : 0
      : score;
  }
};


template <class Tpointer>
unionList<Tpointer>::unionList( listIndex l, compactIndex<Tpointer> *_index ) {

  bLazy = false;
  bIntersect = false;
  index = _index;
  nList = l.length();
  if ( nList > 0 ) list = new compactIterator<Tpointer>[nList];
  baseList = l;
  ownBaseList = false; // We are borrowing baseList, we're not responsible for its destruction
  reordering = NULL;
}

template <class Tpointer>
unionList<Tpointer>::unionList( compactIndex<Tpointer> *_index ) {

  bLazy = false;
  bIntersect = false;
  index = _index;
  baseList = _index->listNonEmptyIndexes();
  ownBaseList = true; // We are responsible of baseList destruction
  nList = baseList.length();
  if ( nList > 0 ) list = new compactIterator<Tpointer>[nList];
  reordering = NULL;
}


template <class Tpointer>
unionList<Tpointer>::~unionList() {

  if ( nList > 0 ) delete [] list;
  if ( ownBaseList ) baseList.free();
}

template <class Tpointer>
void unionList<Tpointer>::initIterators() {
  // Separate from the constructor : the index has to be locked the whole time between
  // the iterators creation and their parsing

#ifdef WIN32
  compactList<Tpointer> * cl = (compactList<Tpointer> *) _malloca( nList * sizeof( compactList<Tpointer> ));
#else
  compactList<Tpointer> cl[nList];
#endif
  indexLock = new ReadLock(index);
  index->access( baseList, cl, bLazy );
  for ( guint i = 0 ; i < nList ; i++ ) {
    list[i] = cl[i].iterator();
    // cout << baseList.element(i) << " > "; cl[i].debug(); cout << endl;
  }
}

template <class Tpointer>
void unionList<Tpointer>::releaseIterators() {
 
  delete indexLock;
}

template <class Tpointer>
listIndex unionList<Tpointer>::classify( compactList<Tpointer> document, intIndex keepMax, double keepOver ) {

  initIterators();

  listIndex scores( nList );
  for ( intIndex i = 0 ; i < nList ; i++ ) {
    scores.append( i, 0 );
  }
  intIndex element, score;

  for ( compactIterator<Tpointer> itDocument = document.iterator() ; itDocument.remains() ; ++itDocument ) {
    itDocument.getElement( element, score );
    for ( intIndex i = 0 ; i < nList ; i++ ) {
      intIndex iElement = intIndexInvalid;
      while ( list[i].remains() &&(( iElement = list[i].element() ) < element ) ) ++list[i];
      if ( iElement == element ) {
        intIndex iScore = list[i].score();
        if ( score > 2 ) score = 2;
        if ( iScore > 2 ) iScore = 2;
        scores.setCount( i, scores.count(i) + std::min( iScore, score ) );
      }
    }
  }

  releaseIterators();

/*
  2-2 => 3    = j+1
  2-1 => 2
  2-0 => 1
  1-1 => 2
  1-0 => 1
  0-0 => 1
*/

  scores.sortCounts();
  intIndex c = 1;
  if ( keepMax > nList ) keepMax = nList;
  for ( ;( c < keepMax )&&( scores.count(c) >= keepOver*scores.count(0) ) ; c++ ) ;;
  scores.cut(c);
  return scores;
}


template <class Tpointer>
listIndex unionList<Tpointer>::classify( listIndex document, intIndex keepMax, double keepOver ) {

  initIterators();

  listIndex scores( nList );
  for ( intIndex i = 0 ; i < nList ; i++ ) {
    scores.append( i, 0 );
  }
  intIndex element, score;

  for ( intIndex j = 0 ; j < document.length() ; j++ ) {
    element = document.element(j);
    score = document.count(j);
    for ( intIndex i = 0 ; i < nList ; i++ ) {
      intIndex iElement = intIndexInvalid;
      while ( list[i].remains() &&(( iElement = list[i].element() ) < element ) ) ++list[i];
      if ( iElement == element ) {
        intIndex iScore = list[i].score();
        if ( score > 2 ) score = 2;
        if ( iScore > 2 ) iScore = 2;
        scores.setCount( i, scores.count(i) + std::min( iScore, score ) );
      }
    }
  }

  releaseIterators();

  scores.sortCounts();
  intIndex c = 1;
  if ( keepMax > nList ) keepMax = nList;
  for ( ;( c < keepMax )&&( scores.count(c) >= keepOver*scores.count(0) ) ; c++ ) ;;
  scores.cut(c);
  return scores;
}


template <class Tpointer>
listIndex unionList<Tpointer>::reunionIntersectWithScoreAdd(intIndex *scoreMap, listIndex inter) {

  initIterators();
  listIndex li( inter.length() );

  for ( intIndex iInter = 0 ; iInter < inter.length() ; iInter++ ) {
   intIndex currentElement = inter.element( iInter );
   intIndex scoreOfElement = 0;
   for ( intIndex i = 0 ; i < nList ; i++ ) {

     intIndex element, score;
     while ( (list[i].length()) &&
             ( list[i].getElement( element, score ), element < currentElement ) ) ++list[i];

     if ( list[i].length() && (element == currentElement) ) {
  
       scoreOfElement += scoreMap[score] * baseList.count(i);
       ++list[i];
     }
   }
   li.append( currentElement, scoreOfElement + inter.count( iInter ) );
  }

  releaseIterators();
  return li;
}

template <class Tpointer>
listIndex unionList<Tpointer>::reunion( intIndex* scoreMap, intIndex listMax ) {

  if ( !nList ) return listIndex();
  initIterators();
  intIndex unionSize = 0;
  for ( intIndex i = 0 ; i < nList ; i++ ) {
    unionSize += list[i].length();
  }
  if ( unionSize > listMax ) unionSize = listMax;
  listIndex li(unionSize);

  if ( nList == 1 ) {
    li.fromCompactScoreMap( list[0], scoreMap );
    releaseIterators();
    return li;
  }
#ifdef WIN32
  int * backtrace = (int *) _malloca( nList * sizeof( int ));
  intIndex * curElt = (intIndex *) _malloca( nList * sizeof( intIndex ));
  intIndex * curScore = (intIndex *) _malloca( nList * sizeof( intIndex ));
#else
  intIndex backtrace[nList];
  intIndex curElt[nList];
  intIndex curScore[nList];
#endif
  for ( intIndex i = 0 ; i < nList ; i++ ) 
     if ( list[i].length() ) list[i].getElement( curElt[i], curScore[i] );
  intIndex lastBackTraced;
  do {
   intIndex smallestIndex = intIndexInvalid;
   intIndex scoreOfIndex = 0;
   lastBackTraced = intIndexInvalid;
   for ( intIndex i = 0 ; i < nList ; i++ ) {
     if ( list[i].length() ) {
        if ( curElt[i] < smallestIndex ) {
         smallestIndex = curElt[i];
         scoreOfIndex = scoreMap[curScore[i]] * baseList.count(i);
         backtrace[i] = intIndexInvalid;
         lastBackTraced = i;
       } else if ( curElt[i] == smallestIndex ) {
         scoreOfIndex += scoreMap[curScore[i]] * baseList.count(i);
         backtrace[i] = lastBackTraced;
         lastBackTraced = i;
       }
     }
   }
   intIndex i = lastBackTraced;
   while ( i != intIndexInvalid ) {
     ++list[i]; list[i].getElement( curElt[i], curScore[i] );
     i = backtrace[i];
   }
   if ( lastBackTraced != intIndexInvalid ) li.insertSortedCounts(smallestIndex, scoreOfIndex);
  } while ( lastBackTraced != intIndexInvalid );

  releaseIterators();
  return li;
}

template <class Tpointer>
void unionList<Tpointer>::dummytest() {
       intIndex element, score;
       list[0].getElement( element, score );
}

template <class Tpointer>
listIndex unionList<Tpointer>::intersect( intIndex* scoreMap ) {

  if ( !nList ) return listIndex();
  initIterators();
  guint smallestList = 0;
  for ( int i = 1 ; i < nList ; i++ ) 
    if ( list[i].length() < list[smallestList].length() ) smallestList = i;
  if ( smallestList > 0 ) {

    compactIterator<Tpointer> is = list[smallestList];
    list[smallestList] = list[0];
    list[0] = is;
  } /* 0 is the smallest list */

  listIndex li(list->length());
  for ( ; list->length() ; ++(*list) ) {

    intIndex j, currentIndex = list->element();
    intIndex score = scoreMap[ list->score() ];
    for ( j = 1 ; 
      (j < nList)&&( 
       list[j].dicoCutOver( currentIndex ),
       score = max( score, scoreMap[ list[j].score() ] ),
       list[j].element() == currentIndex 
       ) ; j++ ) ;;
    if ( j == nList ) li.append(currentIndex, score);
  }

  releaseIterators();
  return li;
}

template <class Tpointer>
listIndex unionList<Tpointer>::weakIntersect( 
  intIndex* scoreMap, 
  listIndex l, 
  intIndex maxScore, 
  intIndex minScore, 
  guint maxPages, 
  guint minPages ) {

  if ( !nList ) return listIndex();
  initIterators();

  // Calculates an upper bound for the score we can reach considering certain lists may be
  // empty, especially in the case we are in lazy mode
  intIndex ubScore = 0;
  intIndex maxScoreMap = std::max( scoreMap[2], scoreMap[3] );
  for ( guint i = 0 ; i < nList ; i++ ) {
    if ( list[i].length() ) ubScore += l.count(i) * maxScoreMap;
  }
  if ( ubScore < minScore ) {
    releaseIterators();
    return listIndex(); // and exits if this upper bound is under the minimal score
  }

  listIndex li( minPages );
#ifdef WIN32
  int * backtrace = (int *) _malloca( nList * sizeof( int ));
#else
  intIndex backtrace[nList];
#endif
  intIndex lastBackTraced;
  do {
   intIndex smallestIndex = intIndexInvalid;
   intIndex scoreOfIndex = 0;
   intIndex ctLists = 0;
   lastBackTraced = intIndexInvalid;
   for ( intIndex i = 0 ; i < nList ; i++ ) {
     if ( list[i].length() ) {
       intIndex element, score;
       list[i].getElement( element, score );
       if ( element < smallestIndex ) {
         smallestIndex = element;
         scoreOfIndex = l.count(i) * scoreMap[score];
         backtrace[i] = intIndexInvalid;
         lastBackTraced = i;
         ctLists = 1;
       } else if ( element == smallestIndex ) {
         scoreOfIndex += l.count(i) * scoreMap[score];
         backtrace[i] = lastBackTraced;
         lastBackTraced = i;
         ctLists++;
       }
     }
   }
   intIndex i = lastBackTraced;
   while ( i != intIndexInvalid ) {
     ++list[i];
     i = backtrace[i];
   }
   if (
    ( scoreOfIndex >= minScore )&&(
      ( !bIntersect )||( ctLists == nList )
    )) li.insertSortedCounts( 
      smallestIndex, 
      reorder( smallestIndex, scoreOfIndex )
    );
  } while ( lastBackTraced != intIndexInvalid );

  releaseIterators();

  if ( maxPages < minPages ) {
    intIndex i = 0;
    for ( ; ( i < li.length() )&&( li.count(i) >= maxScore ) ; i++ ) ;;
    if ( i >= maxPages ) li.cut(i);
  }
  return li;
}

template <class Tpointer>
listIndex unionList<Tpointer>::weakIntersectHomogeneous( 
       intIndex* scoreMap, intIndex maxScore, intIndex minScore, guint maxPages, guint minPages ) {
  
  if ( !nList ) return listIndex();
  initIterators();

  listIndex li( minPages );
#ifdef WIN32
  int * backtrace = (int *) _malloca( nList * sizeof( int ));
  intIndex * curElt = (intIndex *) _malloca( nList * sizeof( intIndex ));
  intIndex * curScore = (intIndex *) _malloca( nList * sizeof( intIndex ));
#else
  int backtrace[nList];
  intIndex curElt[nList];
  intIndex curScore[nList];
#endif
  int lastBackTraced;
  for ( int i = 0 ; i < nList ; i++ ) 
    if ( list[i].length() ) list[i].getElement( curElt[i], curScore[i] );
  
  do {
    intIndex smallestIndex = intIndexInvalid;
    intIndex scoreOfIndex = 0;
    lastBackTraced = intIndexInvalid;
    for ( int i = 0 ; i < nList ; i++ ) {
 
      if ( list[i].length() ) {
 
        if ( curElt[i] < smallestIndex ) {
 
          smallestIndex = curElt[i];
          scoreOfIndex = scoreMap[curScore[i]];
          backtrace[i] = intIndexInvalid;
          lastBackTraced = i;
        } else if ( curElt[i] == smallestIndex ) {
 
          scoreOfIndex += scoreMap[curScore[i]];
          backtrace[i] = lastBackTraced;
          lastBackTraced = i;
        }
      }
    }
    int i = lastBackTraced;
    while ( i != intIndexInvalid ) {
      ++list[i];
      if ( list[i].length() ) list[i].getElement( curElt[i], curScore[i] );
      i = backtrace[i];
    }
    if ( scoreOfIndex >= minScore ) li.insertSortedCounts(smallestIndex, scoreOfIndex);
  } while ( lastBackTraced != intIndexInvalid );

  releaseIterators();
  intIndex i = 0;
  for ( ; ( i < li.length() )&&( li.count(i) >= maxScore ) ; i++ ) ;;
  if ( i >= maxPages ) li.cut(i);
  return li;
}

template <class Tpointer>
listIndex unionList<Tpointer>::weakIntersectWithIntersect( 
  intIndex* scoreMap, 
  listIndex l, 
  intIndex maxScore, 
  intIndex minScore, 
  guint maxPages, 
  guint minPages, 
  guint realIntersect ) {

	if ( !realIntersect ) return weakIntersect( scoreMap, l, maxScore, minScore, maxPages, minPages );

  if ( !nList ) return listIndex();
  initIterators();

  // Calculates an upper bound for the score we can reach considering certain lists may be
  // empty, especially in the case we are in lazy mode
  intIndex ubScore = 0;
  intIndex maxScoreMap = std::max( scoreMap[2], scoreMap[3] );
  for ( guint i = realIntersect ; i < nList ; i++ ) {
    if ( list[i].length() ) ubScore += l.count(i) * maxScoreMap;
  }
  if ( ubScore < minScore ) {
    releaseIterators();
    return listIndex(); // and exits if this upper bound is under the minimal score
  }

  listIndex li( minPages );
  
  guint backtrace[realIntersect];
  intIndex curElt[realIntersect];
  intIndex curScore[realIntersect];

  guint lastBackTraced;
  for ( guint i = 0 ; i < realIntersect ; i++ ) 
    if ( list[i].length() ) list[i].getElement( curElt[i], curScore[i] );
  
  do {
   intIndex smallestIndex = intIndexInvalid;
   intIndex scoreOfIndex = 0;
   
   
  /* Intersection of real intersection part

   bool bRealFail = false;
   
   if ( !list[0].length() ) break;
   list[0].getElement( smallestIndex, scoreOfIndex );
   ++list[0];
   
   for ( guint i = 1 ; i < realIntersect ; i++ ) {
   	 intIndex element = intIndexInvalid; intIndex score = intIndexInvalid;
     while ( list[i].length() &&( list[i].getElement( element, score ), element < smallestIndex ) )
	     	++list[i];
	   if ( element > smallestIndex ) bRealFail = true;
   }
   */

  /* Union of real intersection part */

    lastBackTraced = intIndexInvalid;
    for ( guint i = 0 ; i < realIntersect ; i++ ) {
      if ( list[i].length() ) {
        if ( curElt[i] < smallestIndex ) {
          smallestIndex = curElt[i];
          backtrace[i] = intIndexInvalid;
          lastBackTraced = i;
        } else if ( curElt[i] == smallestIndex ) { 
          backtrace[i] = lastBackTraced;
          lastBackTraced = i;
        }
      }
    }
    if ( smallestIndex == intIndexInvalid ) break;

    guint i = lastBackTraced;
    while ( i != intIndexInvalid ) {
      ++list[i];
      if ( list[i].length() ) list[i].getElement( curElt[i], curScore[i] );
      i = backtrace[i];
    }

//   if ( !bRealFail ) {
   	for ( guint i = realIntersect ; i < nList ; i++ ) {
   		intIndex element = intIndexInvalid; intIndex score = intIndexInvalid;
    	while ( list[i].length() &&( list[i].getElement( element, score ), element < smallestIndex ) ) {
	  		++list[i];
      }
			if ( element == smallestIndex ) {
        scoreOfIndex += l.count(i) * scoreMap[score];
			}
   	}
   
    if ( scoreOfIndex >= minScore ) li.insertSortedCounts(
      smallestIndex, 
      reorder( smallestIndex, scoreOfIndex )
    );
//   }
  } while ( 1 );

  releaseIterators();
  if ( maxPages < minPages ) {
    intIndex i = 0;
    for ( ; ( i < li.length() )&&( li.count(i) >= maxScore ) ; i++ ) ;;
    if ( i >= maxPages ) li.cut(i);
  }
  return li;
}

#endif
