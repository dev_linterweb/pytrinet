import $ from 'jquery';

class ApiConnector {

    constructor(url) {
        this.backends = url.split(',')
        this.backend = this.backends[0]
        this.projectViews = []
        this.backendViews = []
    }

    setBackend( backend ) {
        if ( backend != this.backend ) {
            this.backend = backend
            this.backendViews.forEach( view => view.onBackendChange(backend) )
            this.get( '*', 'list' )
        }
    }

    registerBackendChange(view) {
        this.backendViews.push(view)
        return this.backend
    }

    unregisterBackendChange(view) {
        this.backendViews = this.backendViews.filter( v => view!=v )
    }

    registerAsProjectView(view) {
        this.projectViews.push(view)
    }

    unregisterAsProjectView(view) {
        this.projectViews = this.projectViews.filter( v => view!=v )
    }

    post( project, action, what, onSuccess, onError ) {
        let me = this
        $.ajax({
            url: this.backend+'/'+project+'/'+action,
            type:'POST',
            data:JSON.stringify(what),
            dataType: 'json',
            success: function(res){
                if ( res.error ) {
                    if ( onError ) onError(res.error)
                    else me.showError(res.error)
                } else {
                    if ( onSuccess ) onSuccess(res)
                    if ( res.projects ) {
                        me.projectViews.forEach(view => {
                            view.updateProjects(res.projects)
                        });
                    }
                    if ( res.project ) {
                        me.projectViews.forEach(view => {
                            view.updateProject(res.project)
                        })
                    }
                }
            },
            error: function(e) {
                if ( ! e.status ) {
                    me.showError('Could not contact backend')
                } else {
                    me.showError('Internal error 1')
                }
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Basic coucou");
            }
        });
    }

    get( project, action, onSuccess, onError ) {
        let me = this
        $.ajax({
            url: this.backend+'/'+project+'/'+action,
            type:'GET',
            dataType: 'json',
            success: function(res){
                if ( res.error ) {
                    if ( onError ) onError(res.error)
                    else me.showError(res.error)
                } else {
                    if ( onSuccess ) onSuccess(res)
                    if ( res.projects ) {
                        me.projectViews.forEach(view => {
                            view.updateProjects(res.projects)
                        });
                    }
                    if ( res.project ) {
                        me.projectViews.forEach(view => {
                            view.updateProject(res.project)
                        })
                    }
                }
            },
            beforeSend: function (xhr) {
                xhr.setRequestHeader ("Authorization", "Basic coucou");
            },            
            error: function(e) {
                if ( ! e.status ) {
                    me.showError('Could not contact backend')
                } else {
                    me.showError('Internal error 1')
                }
            }
        });
    }

    showError( message ) {
        alert(message)
    }

}

export default ApiConnector;
