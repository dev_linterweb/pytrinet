#ifndef _MAP_DYNAMIC_H_
#define _MAP_DYNAMIC_H_

#include "lintersearch.h"
#include "index.h"
#include "dylist.h"

class lsDynEngine;

class wordMapDynamic : public mapWord {

public:
  wordMapDynamic();
  virtual ~wordMapDynamic();

  intIndex load( const char *fileName );
  void save();
  ///< Sauvegarde - tous les index et maps doivent avoir été read-lockés au préalable
  virtual lsBufferConst getWord( intIndex idx );
  ///< Read unsafe

  virtual char * getEntry( intIndex hack );
  ///< Read unsafe
  virtual intIndex getNCorpus( intIndex idx );
  ///< Read unsafe

  void  shrink( lsShrinker wShrinker );
  intIndex push( const char * word, guint& nWordInsert );
  virtual  intIndex  length() { return list.size; }
  ///< Read unsafe
  void     cutWords( intIndex *tWords, intIndex oldLen, intIndex newLen );
  bool removeWord( intIndex id );
  ///< Read unsafe (while not const, it does not perform a write lock)

  void debug( intIndex id );
  void debugReverse( char *word );
  void checkDuplicates();
  
protected:

  lsLockable *          fileLock;

  void     convertFromVersion_1();
  void     insert( intIndex idx, const char * word );
  void     divNCorpus();

  char *hackEntry[WORD_HACK_SIZE];
  gsize szMaxEntry[WORD_HACK_SIZE];
  gsize szEntry[WORD_HACK_SIZE];

  typedef struct { guint16 hack; intIndex offset; } wordOffset;
  dyList<wordOffset> list;
  
  bool  syncState; ///< true: is sync'ed with its file on disk : won't save when calling save()
};

#endif
