import argparse
import gc
import hashlib
from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer
import json
import os
import re
from threading import Lock
import time
from pytrinet.JSONApiEncoder import JSONApiEncoder
from pytrinet.ProjectArray import ProjectArray
from cachetools import TTLCache


class ApiHandler(BaseHTTPRequestHandler):

    debugging = True

    def end_headers(self):
        self.send_header("Access-Control-Allow-Origin", "*")
        self.send_header('Access-Control-Allow-Credentials', 'true')
        BaseHTTPRequestHandler.end_headers(self)

    def output( self, data ) :
        self.wfile.write( 
            #json.dumps(data).encode('utf-8')
            JSONApiEncoder().encode(data).encode('utf-8')
        )        

    def dropCrawlOutdated( self, project, data ) :
        self.output({'got':'it'})
        self.finish()
        self.connection.close()
        timestamp = data.get('timestamp',0)
        now = int(time.time())
        if not timestamp :
            timestamp = now - 3600*24*30 # default: 1 month old is outdated
        for element in project.map.iterator() :
            crawled = element['meta'].get('crawled',0)
            if crawled < timestamp :
                print( 'Removing {} because it\'s outdated ({:.2f} days old)'.format(element['url'],(now-crawled)/(3600*24)) )
                project._removeDocument( element['id'], element['lang'] )


    def countDocumentsByMetawords( self, project, data ) :
        prefix = data['prefix'] if 'prefix' in data else None
        lang = data['lang'] if 'lang' in data else None
        if not prefix :
            raise Exception('prefix parameter is missing')
        if not lang :
            raise Exception('prefix lang is missing')
        key = "{}/{}/{}".format(project.name,prefix,lang)
        with ApiHandler.ttlCacheLock :
            if key in ApiHandler.ttlCache :
                counts = ApiHandler.ttlCache[key]
            else :
                counts = project.countDocumentsByMetawords( lang, prefix )
                ApiHandler.ttlCache[key] = counts
        self.output({
            'metawordCounts':counts,
            'prefix':prefix
        })

    def removeOrphans( self, project, data ) :
        lang = data['lang'] if 'lang' in data else False
        project.removeOrphans( lang )
        print('Done removing orphans')
        self.output({})

    def queryAll( self, project, data ) :
        q = data['query'] if 'query' in data else ''
        limit = data['limit'] if 'limit' in data else 25
        lang = data['lang'] if 'lang' in data else None
        self.output({
            'project_results':ApiHandler.projectArray.queryAll( q, lang, limit ),
            'lang':lang
        })

    def query( self, project, data ) :
        q = data['query'] if 'query' in data else ''
        url = data['url'] if 'url' in data else None
        lang = data['lang'] if 'lang' in data else None
        offset = data['offset'] if 'offset' in data else 0
        limit = data['limit'] if 'limit' in data else 25
        sortBy = data['sortBy'] if 'sortBy' in data else False
        
        metaWords = data['metaWords'] if 'metaWords' in data else []
        metaWords = tuple(map( lambda word : '{}:{}'.format(word[0],word[1]), metaWords ))

        ids = data.get('ids',False)
        sortByStamp = False
        if sortBy == 'stamp:-1' :
            sortByStamp = True
            sortBy = False
        ( lang, results ) = ( 
            project.query( q, lang, metaWords=metaWords, sortByStamp=sortByStamp ) if not url  else project.queryConnex( url, sortByStamp=sortByStamp )
        )
        results.sortBy(sortBy).offsetLimit( offset, limit )
        if ids :
            self.output({
                'results':list( 
                    map (
                        lambda res : res.id,
                        results
                    ) 
                ),
                'all_count':project.allCount( lang ),
                'lang':lang
            })
        else :
            self.output({
                'results':list(
                    filter( lambda res : res[0], 
                        map (
                            lambda res : (res.id,res.url,res.score,res.meta),
                            results
                        ) 
                    ) 
                ),
                'all_count':project.allCount( lang ),
                'lang':lang
            })

    def get( self, project ) :
        self.output({ 'project':project })

    def set( self, project, data ) :
        if data['section'].startswith('explorer'):
            i = data['section'][8:]
            i = int(i) if i else 0
            setattr( project.explorers[i], data['key'], data['value'] )
            self.get( project )

    def getmap( self, project, data ) :
        if 'ids' in data : self.output( project.getsMap( data['lang'], data['ids'] ) )
        else : self.output( project.getMap( data['lang'], data['id'] ) )

    def start( self, project ) :
        if project :
            for explorer in project.explorers :
                explorer.start()
        else :
            ApiHandler.projectArray.startAll()
        self.output({ 'project':project })


    def stop( self, project ) :
        if project :
            for explorer in project.explorers :
                explorer.stop()
        else :
            ApiHandler.projectArray.stopAll()
        self.output({ 'project':project })

    def reset( self, project ) :
        if not project :
            raise Exception('Reset command must target a project')
        for explorer in project.explorers :
            explorer.reset()
        self.output({ 'project':project })

    def save( self, project ) :
        if project :
            project.save()
        else :
            ApiHandler.projectArray.saveAll()
        self.output( { 'project':project } if project else {} )

    def debug( self, project ) :
        if not project :
            raise Exception('Debug command must target a project')
        # project.debug()
        print(gc.get_stats())

    def push( self, project, data ) :
        if not project :
            raise Exception('Push command must target a project')
        if not 'url' in data :
            raise Exception('No field url on project {} with meta = {}'.format(project,data.get('meta')))
        body = False
        if 'file' in data :
            body = open( data['file'], 'r', encoding="utf-8" )
        elif 'body' in data :
            body = data['body']
        project.explorers[0].push(
            data['url'], 
            body,
            meta = data.get('meta'),
            lang = data.get('lang'),
            metaWords = data.get('metaWords'),
            stamp = data.get('stamp',0)
        )
        self.output({})

    def list( self ) :
        self.output({
            'projects':list(ApiHandler.projectArray.projects.values())
        })

    def quit( self ) :
        ApiHandler.projectArray.quit()
        print("Ready to quit ...")
        self.output({'quitting':'true'})
        ApiHandler.quitting = True
        os._exit(os.EX_OK)

    def do_OPTIONS(self):
        self.send_response(204, "No Content")
        self.send_header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS')
        self.send_header("Access-Control-Allow-Headers", "Content-type, Authorization")
        self.end_headers()

    def do_GET(self):        
        self.do_GET_POST(False)

    def do_POST(self):
        self.do_GET_POST(True)
        
    def do_GET_POST(self,isPost):
        try:
            if ApiHandler.password :
                authorization = self.headers.get('Authorization')
                if not authorization :
                    self.send_response(401,"Missing authorization header")
                    self.end_headers()
                    return
                authorization = re.match( '^Basic\\s+([^\\s]+)$',authorization )
                if not authorization :
                    self.send_response(401,"Invalid authorization header")
                    self.end_headers()
                    return
                password = authorization.group(1) if authorization else ''
                if hashlib.md5(password.encode('utf-8')).hexdigest() != ApiHandler.password :
                    self.send_response(401,"Wrong password")
                    self.end_headers()
                    return

            parts = self.path.split('/')
            if len(parts) == 3 :
                _,project_name,action = parts
                if project_name == '*' :
                    project_name = None
            else :
                raise Exception('Wrong number of parameters')

            content_len = int(self.headers.get('Content-Length')) if isPost else 0
            payload = self.rfile.read(content_len).decode("utf-8")
            data = json.loads( payload ) if content_len else {}

            if project_name :
                project = ApiHandler.projectArray.getProject(project_name)
                if not project :
                    raise Exception('Project {} does not exist'.format(project_name))
            else :
                project = None

            self.send_response(200)
            self.send_header("Content-type", 'application/json')
            self.end_headers()

            if action == 'query' :
                self.query(project,data)

            if action == 'queryall' :
                self.queryAll(project,data)

            if action == 'countDocumentsByMetawords' :
                self.countDocumentsByMetawords(project,data)

            if action == 'push' :
                self.push(project,data)

            if action == 'start' :
                self.start(project)

            if action == 'stop' :
                self.stop(project)

            if action == 'reset' :
                self.reset(project)

            if action == 'save' :
                self.save(project)

            if action == 'list' :
                self.list()

            if action == 'getmap' :
                self.getmap(project,data)

            if action == 'get' :
                self.get(project)

            if action == 'set' :
                self.set(project,data)

            if action == 'debug' :
                self.debug(project)

            if action == 'quit' :
                self.quit()

            if action == 'dropCrawlOutdated' :
                self.dropCrawlOutdated(project,data)

            if action == 'cleanup' : 
                self.cleanup(project)

            if action == 'removeOrphans' : 
                self.removeOrphans(project,data)

        except Exception as e:
            self.send_response(404)
            self.send_header("Content-type", 'application/json')
            self.end_headers()
            self.wfile.write(json.dumps({'error': e.__str__() if self.debugging else ''}).encode("utf-8"))
            if self.debugging : raise e

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Launches a trinet engine array behind an http api')
    parser.add_argument('--path')
    args = parser.parse_args()

    if not args.path :
        raise Exception('Missing parameter path')

    ApiHandler.quitting = False
    ApiHandler.password = False
    try:
        with open('.api-password') as f:
            ApiHandler.password = f.read().strip()
    except Exception as e:
        print("No password defined ; the API is opened to anyone !")

    ApiHandler.projectArray = ProjectArray( args.path )
    ApiHandler.projectArray.startAll()

    ApiHandler.ttlCache = TTLCache(maxsize=32, ttl=60)
    ApiHandler.ttlCacheLock = Lock()

    with ThreadingHTTPServer(("", ApiHandler.projectArray.port), ApiHandler) as httpd:
        while not ApiHandler.quitting :
            httpd.handle_request()

    print("Quitting main thread")
    os._exit(os.EX_OK)
        # httpd.serve_forever()
    #    threading.Thread(target=httpd.serve_forever).start()
