#include "dynindex.h"
#include "dynengine.h"
#include "dystorage.h"

wordIndexDynamic::wordIndexDynamic( wordMapDynamic *_map, lsDynEngine *_engine ) 
: compactIndex<dyPointer>( new lsDynamicStorage("WILI") ) {

  engine = _engine;
  map = _map;
  wordMaxArticle = WORD_MAX_ARTICLE;
  articleExtensionSize = ARTICLE_EXTENSION_SIZE;
  articles = NULL;
  bKill = false;
  topExtension = 0;
  nAllResynch = intIndexInvalid;
  bAsynchExtension = false;
}

void wordIndexDynamic::startSynchThread() {

  bAsynchExtension = true;
  std::thread( statRealizeExtension, this ).detach();
}

void wordIndexDynamic::notifyQuit() {
  bKill = true;
}

wordIndexDynamic::~wordIndexDynamic() {

  bKill = true;
  while ( bKill ) sleep(1); // waits for sync thread to exit
}

void wordIndexDynamic::statRealizeExtension( wordIndexDynamic *wi ) {

  wi->realizeExtension();
}

void wordIndexDynamic::attachToFile( const char* _fileName ) {

  { WriteLock _(this);
    ReadLock __(fileLock);

    compactIndex<dyPointer>::attachToFile( _fileName );
    std::istream & in( storage->getInStream() );
    if ( in ) {
      { WriteLock _(&extensionMutex);
        topExtension = 0;
        for ( ; topExtension < N_EXTENSION+1 ; topExtension++ ) {
          extension[topExtension].load(in);
          if ( ! extension[topExtension].length() ) break;
          else engine->log("general") << "loading async list : " << extension[topExtension].length();
        }
      }
    }
    storage->closeFile();
  }
}

void wordIndexDynamic::sync() {
 
  { ReadLock _(this);
    WriteLock __(fileLock);

    storage->sync();
    std::ostream & out = storage->getOutStream();
    if ( out ) {
      { WriteLock _(&extensionMutex);
        buildExtension.save(out);
        intIndex i = 0;
        for ( ; i < topExtension ; i++ ) extension[i].save(out);
      }
    }
    storage->closeFile();
  }
}

void wordIndexDynamic::syncConvert() {
  
  { ReadLock _(this);
    storage->syncConvert();
    std::ostream & out = storage->getOutStream();
    if ( out ) {
      { WriteLock _(&extensionMutex);
        buildExtension.save(out);
        intIndex i = 0;
        for ( ; i < topExtension ; i++ ) extension[i].save(out);
      }
    }
    storage->closeFile();
  }
}

bool wordIndexDynamic::doFlushExtension() {
  // pushes buildExtension into the extension file (file of articles to be synchronized asap)

  if ( engine->isClassifier() ) return true;
  if ( buildExtension.length() ) {
    if ( topExtension < N_EXTENSION ) extension[topExtension++] = buildExtension;
    else { return false; }
  }
  buildExtension = listIndex( articleExtensionSize );
  return true;
}

bool wordIndexDynamic::flushExtension() {
  
  if ( engine->isClassifier() ) return true;
  WriteLock _(&extensionMutex);
  return doFlushExtension();
}

void wordIndexDynamic::waitExtension() {
 
  if ( engine->isClassifier() ) return;
  if ( ! bKill ) {
    std::cout << "waitExtension " << topExtension << std::endl;
    while ( topExtension && !bKill ) sleep(1);
    std::cout << "waitedExtension " << std::endl;
  }
}

bool wordIndexDynamic::pushExtension( intIndex id ) {

  if ( engine->isClassifier() ) return true;
  WriteLock _(&extensionMutex);
  if (( ! buildExtension.appendable() )&&( ! doFlushExtension() )) return false;
  buildExtension.append( id, 1 );
  return true;
}

void wordIndexDynamic::realizeExtension() {

  if ( engine->isClassifier() ) return;
  listIndex wordList(wordMaxArticle);
  do {
    WriteLock * lock = NULL;
    while ( lock = new WriteLock(&extensionMutex), !topExtension &&( nAllResynch == intIndexInvalid )&& !bKill ) {
      delete(lock);
      sleep(2);
    }
    intIndex nAllResynchEnd = nAllResynch;
    if (( !topExtension )&&( nAllResynch != intIndexInvalid )) {
      topExtension++;
      extension[0] = listIndex( 2*articleExtensionSize );
      for ( ; ( nAllResynchEnd < articles->length() )&&( extension[0].appendable() ) 
            ; nAllResynchEnd++ ) extension[0].append( nAllResynchEnd, 1 );
      if ( nAllResynchEnd >= articles->length() ) nAllResynchEnd = intIndexInvalid;
    }
    extension[0].sortElements();
    extension[0].collapse();
    delete(lock);
    if ( bKill ) break;

    intIndex         nMax = extension[0].length();

    if ( nMax > 0 ) {
     { WriteLock _(&syncMutex);
      intIndex         thisLength = length();
      engine->log("wiki") << "** begin sync " << nMax;
      dyCompactList      *articleBase = new dyCompactList[nMax];
      dyCompactIterator  *article = new dyCompactIterator[nMax];
      intIndex         *currentElement = new intIndex[nMax];

      { ReadLock _(articles); 
        articles->access( extension[0], articleBase );
        for ( intIndex i = 0 ; i < nMax ; i++ ) articleBase[i] = articleBase[i].dup();
      }


      for ( intIndex i = 0 ; i < nMax ; i++ ) {
        article[i] = dyCompactIterator( articleBase[i] );
        currentElement[i] = article[i].remains() ? article[i].element() : intIndexInvalid;
        extension[0].setCount( i, intIndexInvalid );
      }

      for ( intIndex w = 0 ; w < thisLength ; w++ ) {

        if ( bKill ) break;
        bool atLeastOneArticle = false;
        for ( intIndex a = 0 ; a < nMax ; a++ ) {
          if ( currentElement[a] == w ) {
              extension[0].setCount( a, article[a].score() );
              ++article[a];
              currentElement[a] = article[a].remains() ? article[a].element() : intIndexInvalid;
              atLeastOneArticle = true;
          }
        }

        { ReadLock _(this);
          dyCompactList oldEntry = access( w );
          if ( atLeastOneArticle ) wordList.fromCompactFusion( oldEntry, extension[0] );
            // Remet au passage tous les scores de extension à intIndexInvalid
          else atLeastOneArticle = wordList.fromCompactRemove( oldEntry, extension[0] );
        }

        if ( atLeastOneArticle ) {
          dyCompactList l = dyCompactList( wordList );
          l.fromList( wordList );
          { WriteLock _(this);
            replace( w, l );
          }
        }
      }
      for ( intIndex i = 0 ; i < nMax ; i++ ) articleBase[i].free();
      delete [] currentElement;
      delete [] articleBase;
      delete [] article;
    }
    engine->log("wiki") << "** end sync";
   }

   nAllResynch = nAllResynchEnd;

   { WriteLock _(&extensionMutex);
    extension[0].free();
    for ( intIndex i = 1 ; i < topExtension ; i++ ) extension[i-1] = extension[i];
    topExtension--;
   }

  } while (( !bKill )&&( bAsynchExtension ||( nAllResynch != intIndexInvalid )));
  wordList.free();
  bKill = false;
}

void wordIndexDynamic::syncExtension() { // performs all synchronization in once without using the separate thread  "syncThread"

  nAllResynch = 0;
  realizeExtension();
}

intIndex wordIndexDynamic::push( const char *word, guint& nWordInsert ) {

  if ( strlen( word ) < 2 ) return intIndexInvalid;
  intIndex idx = map->push( word, nWordInsert );
  if ( idx == intIndexInvalid ) return intIndexInvalid;
  if ( idx >= length() ) {
    WriteLock _(this);
    replace( idx, dyCompactList() );
  }
  return idx;
}

int wordIndexDynamic::getAsynch() {

  if ( engine->isClassifier() ) return 0;
  int ct = buildExtension.length();
  for ( intIndex i = 0 ; i < topExtension ; i++ ) ct += extension[i].length();
  return ct;
}

void wordIndexDynamic::debug( intIndex id ) {

  if ( id >= length() ) std::cout << "Hors de portée" << std::endl;
  else { access(id).debug(); }
}

void wordIndexDynamic::debugExtension() {
  { WriteLock _(&extensionMutex);
    buildExtension.debug("buildExtension");
    for ( intIndex i = 0 ; i < topExtension ; i++ ) {
      extension[i].debug("extension ");
    }
  }
}

bool wordIndexDynamic::checkStructure( intIndex maxTarget ) {

  bool rt = false;
/*  
  { ReadLock _(this); 
    for ( int i = 0 ; i < length() ; i++ ) {
      if ( entry(i).checkStructure( maxTarget ) ) {
        engine->config->log("general") << "CheckStructure > overload on word entry " << i;
        list[i].free();
        rt = true;
      }
    }
  }
  */
  return rt;
}

lsShrinker wordIndexDynamic::getWordShrinker( lsShrinker aShrinker ) {
 
  lsShrinker wShrinker( length() );
  wShrinker.removeAll(); // remove all, then keep some
  for ( intIndex i = 0 ; i < length() ; i++ ) {
    dyCompactIterator it( access(i) );
    for ( ; it.length() ; ++it ) {
      intIndex id = it.element();
      if ( aShrinker.rewrite(id) != intIndexInvalid ) { wShrinker.keep(i); break; }
    }
  }
  wShrinker.build();
  return wShrinker;
}


articleIndexDynamic::articleIndexDynamic( wordIndexDynamic *_words, lsDynEngine *_engine  ) : compactIndex<dyPointer>( new lsDynamicStorage("AILI") ) {

  engine = _engine;
  articleMaxWord = ARTICLE_MAX_WORD;
  words = _words;
  words->setArticles( this );
  idx = 0;
}

articleIndexDynamic::~articleIndexDynamic() {}

void articleIndexDynamic::attachToFile( const char * _fileName ) {

  { WriteLock _(this);    
    ReadLock __(fileLock);

    compactIndex<dyPointer>::attachToFile( _fileName );
    // Reading stamps : 32 bit numbers assigned to each document, probably timestamps
    std::istream & in( storage->getInStream() );
    if ( in ) {
      char signature[4];
      in.read( signature, 4 );
      if ( in &&( ! std::memcmp( signature, "STMP", 4 ) )) {
        intIndex size = 0;
        in.read( (char*)&size, 4 );
        if ( in &&( size <= length()) ) {
          engine->stamps.resize( size );
          in.read( (char*)engine->stamps.data(), size * sizeof(intIndex) );
          std::cout << "Read " << size << " timestamps \n";
        }
      }
    }
  }
  storage->closeFile();
}

void articleIndexDynamic::sync() {

  { ReadLock _(this);
    WriteLock __(fileLock);
    storage->sync();
    if ( engine->stamps.size() ) {
      std::ostream & out = storage->getOutStream();
      if ( out ) {
        const char * signature = "STMP";
        out.write( signature, 4 );
        intIndex size = engine->stamps.size();
        out.write( (char*)&size, 4 );
        out.write( (char*)engine->stamps.data(), size * sizeof(intIndex) );
      }
    }
    storage->closeFile();
  }
}

void articleIndexDynamic::syncConvert() {

  { ReadLock _(this);
    storage->syncConvert();
    if ( engine->stamps.size() ) {
      std::ostream & out = storage->getOutStream();
      if ( out ) {
        const char * signature = "STMP";
        out.write( signature, 4 );
        intIndex size = engine->stamps.size();
        out.write( (char*)&size, 4 );
        out.write( (char*)engine->stamps.data(), size * sizeof(intIndex) );
      }
    }
    storage->closeFile();
  }
}


intIndex  articleIndexDynamic::push( listIndex lWords, intIndex pageLimit, intIndex stamp ) {

  if ( !lWords.length() ) {
    return intIndexInvalid;
  }
  WriteLock * lock = new WriteLock(this);
  intIndex idx = storage->findFreeIndex( pageLimit );
  if ( idx != intIndexInvalid ) {
    dyCompactList l = dyCompactList(lWords);
    l.fromList(lWords);
    replace( idx, l );
    delete lock;
    words->pushExtension( idx );
    engine->setStamp( idx, stamp );
  } else delete lock;
  return idx;
}

void  articleIndexDynamic::pop( intIndex article ) { 

  if ( article >= length() ) return;
  { WriteLock _(this);
    replace( article, dyCompactList() );
  }
  words->pushExtension(article);
}

bool articleIndexDynamic::update( intIndex article, listIndex lWords, intIndex stamp ) {

  { WriteLock _(this);
    dyCompactList l;
    if ( article < length() ) {
      dyCompactList oldList = access( article );
      l.allocFromListTitleFusion( oldList, lWords );
    } else {
      l = dyCompactList(lWords);
      l.fromList(lWords);
    }
    replace( article, l );
    engine->setStamp( article, stamp );
  }
  words->pushExtension(article);
  return false;
}

bool articleIndexDynamic::fusionTitle( intIndex article, listIndex lWords, intIndex stamp ) {

  { WriteLock _(this);
    dyCompactList l;
    dyCompactList oldList = ( article < length() ) ? access( article ) : dyCompactList();
    l.allocFromFusionTitle( oldList, lWords );
    replace( article, l );
    engine->setStamp( article, stamp );
  }
  words->pushExtension(article);
  return false;
}



intIndex articleIndexDynamic::checkDuplicate( intIndex id ) { // needs read-lock

  if ( id >= length() ) return intIndexInvalid;
  for ( intIndex i = 0 ; i < length() ; i++ ) {
    if ( id != i ) {
      listIndex l2(2); l2.append(i,1); l2.append(id,1);
      dyCompactList cl[2];
      access( l2, cl );
      l2.free();
      if ( ! cl[1].checkDuplicate( cl[0] ) ) {
        return id;
      }
    }
  }
  return intIndexInvalid;
}

bool articleIndexDynamic::checkStructure( intIndex maxTarget ) {

  bool rt = false;
  return rt;
}

void articleIndexDynamic::wordCount( intIndex nWords, intIndex *tWords ) { // must read-lock

  for ( intIndex i = 0 ; i < length() ; i++ ) { // MEILLEUR ACCES PAR GROUPE PEUT ETRE
    access(i).wordCount( nWords, tWords );
  }
}

void articleIndexDynamic::debug( intIndex id, mapWord * wordMap ) {

  if ( id >= length() ) std::cout << "Hors de portée" << std::endl;
  else { 
    ReadLock _(this);
    dyCompactList l = access(id);
    dyCompactIterator it = l.iterator();

    std::cout << it.length() << " : [ ";
    for ( ; it.remains() ; ++it ) {
      intIndex idx = it.element();
      lsBufferConst word = wordMap->getWord( idx );
      std::cout << "(" << word << "["<< idx <<"]" << "," << it.score() << ") ";
    }
    std::cout << " ]" << std::endl;

  }
}

void articleIndexDynamic::debugTop( intIndex id, mapWord * wordMap ) {

  if ( id >= length() ) std::cout << "Hors de portée" << std::endl;
  else { 
    ReadLock _(this);
    dyCompactList l = access(id);
    dyCompactIterator it = l.iterator();

    std::cout << it.length() << " : ";
    for ( ; it.remains() ; ++it ) {
      intIndex idx = it.element();
      if ( it.score()>=2 ) {
        lsBufferConst word = wordMap->getWord( idx );
        std::cout << word << " ";
      }
    }
    std::cout << std::endl;

  }
}


void articleIndexDynamic::debugStatus() {

  std::cout << length() << " articles, " << words->length() << " mots." << std::endl;
}

int articleIndexDynamic::getNArticles() {

  return length();
}

int articleIndexDynamic::getNWords() {

  return words->length();
}

