
import hashlib
import json
import re
from pytrinet.DynamicEngine import ArticleMap
from pymongo import MongoClient
from bson.objectid import ObjectId
from pymongo.errors import DuplicateKeyError

# Document map in a mongo database
# The mongo collection MUST have an index on field 'trinet'

class MongoMap(ArticleMap):

    parameters = (
        ('mongoUrl','mongodb://127.0.0.1:27017'),
        ('mongoDatabase','resident'),
        ('mongoCollection','')  # Collection name
    )
 
    def __init__( self, config, name ):
        super().__init__( config )
        mongodb_client = MongoClient(self.mongoUrl)
        database = mongodb_client[self.mongoDatabase]
        self.collection = database[ self.mongoCollection ]

    def iterator( self ):
        for element in self.collection.find({'trinet':{'$exists':True}}) :
            if not element : break
            ( lang, id ) = self.splitLangIndex( element['trinet'] )
            yield {
                'url':element['url'],
                'lang':lang,
                'id':int(id), 
                'meta':element.get('meta',{})
            }

    def splitLangIndex( self, langIndex ):
        for i in range(len(langIndex)) :
            if langIndex[i].isdigit() : break
        return langIndex[:i],langIndex[i:]

    def getHash( self, url:str ) :
        return ( hashlib.new( 'ripemd160', url.encode() ).hexdigest() )[0:24]

    def getUrl( self, lang: str, id: int ) -> str:
        result = self.collection.find_one({'trinet':'{}{}'.format(lang,id)})
        return result['url'] if result else None

    def getMeta( self, lang: str, id: int ) -> dict:
        result = self.collection.find_one({'trinet':'{}{}'.format(lang,id)})
        return result.get('meta',None) if result else None

    def getField( self, lang: str, id: int, field: str, default: str ) :
        result = self.collection.find_one({'trinet':'{}{}'.format(lang,id)},{field:1})
        return result.get(field,default) if result else default
    
    def get( self, lang: str, id: int ) :
        result = self.collection.find_one({'trinet':'{}{}'.format(lang,id)})
        meta = result.get('meta',{}) if result else {}
        return {'url':result['url'],'meta':meta} if result else {}
    
    def gets( self, lang: str, ids: list ) :
        results = self.collection.find({'trinet':{'$in':['{}{}'.format(lang,id) for id in ids ]}})
        rt = {}
        for result in results :
            rt[ int(result['trinet'][len(lang):]) ] = { 'url':result['url'], 'meta':result.get('meta',{}) }
        return rt

    def getId( self, url: str ) -> tuple:  # returns (lang,id) if url exists, or (None,None)
        mongoid = ObjectId(self.getHash(url))
        result = self.collection.find_one({'_id':mongoid})
        if result and 'trinet' in result :
            (lang,id) = self.splitLangIndex(result['trinet'])
            return (lang,int(id))
        else : return (None,None)

    def set( self, lang: str, id: int, url: str, meta: dict ):
        mongoid = ObjectId(self.getHash(url))
        trinet_id = lang+str(id)
        # try:
        self.collection.delete_many(
            {'trinet':trinet_id}
        )
        self.collection.update_one(
            {'_id':mongoid},
            {'$set':{
                'trinet':trinet_id,
                'url':url,
                'meta':meta if meta else False
            }},
            True
        )
        """  except DuplicateKeyError:
            self.collection.delete_one(
                {'trinet':trinet_id}
            )
            self.collection.update_one(
                {'_id':mongoid},
                {'$set':{
                    'trinet':trinet_id,
                    'url':url,
                    'meta':meta if meta else False
                }},
                True
            ) """

    def delete( self, url: str ):
        mongoid = ObjectId(self.getHash(url))
        self.collection.delete_one(
            {'_id':mongoid}
        )

    def trinetIdToId( self, trinetId ) :
        i = 0
        while i < len(trinetId) and not trinetId[i].isnumeric() : i += 1
        if i >= len(trinetId) : raise Exception('trinetId format is invalid')
        return int(trinetId[i:])

    def deduplicate( self, gen ) :
        # return gen # uncomment when safe
        seen = set()
        for id in gen :
            if not id in seen:
                seen.add(id)
                yield id

    def sortBy( self, lang, _list, field, direction ) :
        trinetIds = map( lambda id : '{}{}'.format(lang,id), _list )
        return tuple( self.deduplicate( map(
            lambda res : self.trinetIdToId(res['trinet']), 
            self.collection.find(
                { 'trinet': {'$in':tuple(trinetIds)} },
                { 'trinet':1 }
            ).sort(field,direction)
        ) ) )
        

