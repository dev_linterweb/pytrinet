
from io import StringIO
import re
import time
import requests
from urllib.parse import quote_plus
from pytrinet.explorer.Explorer import Explorer
from pytrinet.explorer.MediawikiBot import MediawikiBot

class WikimediaExplorer(Explorer):

    parameters = (
        ('wikimedia',''), # wikimedia project : wikipedia, wikibooks, ...
        ('apcontinue',''), # Next value of apcontinue in mediawiki api list=allpages
        ('apcycling','false'),
        ('irclistening','false'),
        ('autoSave','100'),
        ('lang','') # Language of the mediawiki
    )

    def __init__( self, project, config, id ):
        super().__init__( project, config, id )
        self.pageIds = []
        self.ircBotLaunched = False

    def urlApiListAllPages(self) :
        return 'https://{}.{}.org/w/api.php?action=query&list=allpages&format=json&apcontinue={}'.format(self.lang,self.wikimedia,quote_plus(self.apcontinue))

    def urlApiParse(self,pageid):
        return 'https://{}.{}.org/w/api.php?action=parse&pageid={}&format=json&prop=text'.format(self.lang,self.wikimedia,pageid)

    def ircPush(self,title) :
        if ':' in title:
            self.log('({}.{}) Dropping IRC push : {}'.format( self.lang, self.wikimedia, title ))
            return
        self.log('({}.{}) Got IRC push : {}'.format( self.lang, self.wikimedia, title ))
        url = 'https://{}.{}.org/wiki/{}'.format( self.lang, self.wikimedia, title )
        bodyLength = 0
        try :
            r = requests.get( url, timeout=4, headers={'User-Agent': 'https://archives.wikiwix.com'} )
            r.raise_for_status()
            self.log('Got a {} on {}'.format(r.status_code,url))
            
            body = r.text
            bodyLength = len(body)
            body = StringIO(body) # would love to use r.iter_content, but iter_content does not guess encoding as well            
        except Exception :
            self.log('{} > Could not download'.format(url))

        meta = { 'title':title }
        metaWords = ( 'domain:{}.{}.org'.format(self.lang,self.wikimedia), )

        if bodyLength :
            self.log('{} > IRC pushing {}B with metawords {} and meta {}'.format(url,bodyLength,metaWords,meta))
            self.project.pushDocument( url, body, meta, self.lang, metaWords, logger=self )


    def doExplore(self) :

        if not self.ircBotLaunched and self.irclistening == 'true' :
            MediawikiBot.getInstance().startListening( self.lang, self.wikimedia, self )
            self.ircBotLaunched = True

        if self.apcycling == 'true' :

            if not self.pageIds :
                r = requests.get( self.urlApiListAllPages())
                r.raise_for_status()
                result = r.json()
                if not 'continue' in result :
                    self.apcycling = 'false'
                else :
                    self.apcontinue = result['continue']['apcontinue']
                    if not self.apcontinue :
                        self.apcycling = 'false'
                self.pageIds = list(map( lambda item : item['pageid'], result['query']['allpages'] ))

            if not self.pageIds:
                return False

            pageid = self.pageIds.pop(0)
            r = requests.get( self.urlApiParse(pageid) )
            r.raise_for_status()
            result = r.json()
            title = result['parse']['title']
            url = 'https://{}.{}.org/wiki/{}'.format( self.lang, self.wikimedia, title )
            body = result['parse']['text']['*']
            if not '<div class="redirectMsg">' in body :
                text = '<html><head><title>{}</title></head><body>{}</body></html>'.format(title,body)
                body = StringIO(text)
                meta = { 'title':title }
                metaWords = ( 'domain:{}.{}.org'.format(self.lang,self.wikimedia), )

                self.log('{} > pushing with metawords {} and meta {}'.format(url,metaWords,meta))
                self.project.pushDocument( url, body, meta, self.lang, metaWords, logger=self )
                time.sleep(1)
            else :
                redir_match = re.search(r'<a href=[^>]+>([^<]+)</a>',body)
                if redir_match :
                    sourceTitle = title
                    text = '<html><head><title>{}</title></head></html>'.format(sourceTitle)
                    body = StringIO(text)
                    metaWords = ( 'domain:{}.{}.org'.format(self.lang,self.wikimedia), )
                    title = redir_match.group(1)
                    url = 'https://{}.{}.org/wiki/{}'.format( self.lang, self.wikimedia, title )
                    self.log(' > pushing redirection from {} to {}'.format(sourceTitle,url))
                    self.project.pushDocument( url, body, {}, self.lang, metaWords, logger=self, fusionTitle=True )
                    time.sleep(1)

            return True

        else :
            return False
