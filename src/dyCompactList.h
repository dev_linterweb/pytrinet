#ifndef _DY_COMPACT_LIST_H_
#define _DY_COMPACT_LIST_H_

#include "lintersearch.h"
#include "compactList.h"
#include "compactListConvert.h"
#include "mutex.h"
#include <ostream>

#if ( defined( MEM_CHECK_BREAK ) || defined( MEM_CHECK_NOTIFY ) ) 
#define MEM_CHECK
#endif

#ifdef MEM_CHECK
#define CHECK_MEM( ptr, len ) dyPointer::check_access( (guchar*)ptr, len );
#else
#define CHECK_MEM( ptr, len ) ;;
#endif

/** Bloc de memoire utilise par dyPointer.
Zone de stockage pour l'allocation efficace des listes (compactList). Un dyChunk stocke
plusieurs atomes de meme taille.
Chaque compactList est une suite d'atomes de tailles normalisees, repartis dans les
differents dyChunk.
\sa \ref chunks
*/
class dyChunk {

public:
  void *operator new( size_t num_bytes, guint _atomSize );
  void operator delete( void * data );
  /// Allocateur. Alloue une zone de memoire de la taille atomSize.
  guchar *alloc();
  /// Liberateur. Libere une zone de memoire qui a ete allouee par ce dyChunk.
  void  free( guchar* ptr );  
  
  void whereIs( guchar *ptr, guint16 &n, guint16 &i, guint16 &j, guint16 this_n, bool withFollower, guint16 this_i = 0 );
  bool getMarked( guint i );
  void cumulStat( guint &nBlocs, guint &nSpareBlocs );
  bool empty() const;

  gsize getnChunks(); ///< Returns the number of chunks in this chain. Multiply getChunkNumber()*nAtom*atomSize to get the byte size of the whole chain.

  guint getAtomSize() const { return atomSize; }
  guint getnAtom() const { return nAtom; }

protected:
  guchar  *entry( guint i );
  guchar** getFollower(guchar*);
  void     mark( guint i );
  void     unmark( guint i );
  void     adjustFreeFirst();
  void     remove( dyChunk *chunk );


  union {
   guchar   * chunk;
   gulong   * chunkMark;
  };
  guchar * chunkStore;
  guint    atomSize;
  guint    nAtom;
  guint    freeFirst;
  dyChunk  *follow;
  friend class dyPointer;
};

/** Allocateur / Iterateur de memoire pour compactList.
L'objet dyPointer est statique (global). Il doit etre initialise en debut de processus
par la methode globalinit.
Il permet d'allouer la chaine d'atomes constituant une compactList, et 
de gerer un iterateur sur une chaine d'atomes.
\sa \ref chunks
*/
class dyPointer {

  static bool inOverload;
public:
  friend class handlerOverload;
  class handlerOverload {
    bool inCall;
    static void sHandleOverload( handlerOverload * );
    public:
      handlerOverload() { dyPointer::inOverload = false; }
      void handleOverload();
      virtual void doHandleOverload() = 0;
  };
  static bool isinOverload() { return inOverload; }

  static void debug( std::ostream& out ); ///< Displays details about the allocated chunk structure in ostream out

private:
  guchar  *curs;
  guint16  len, thislen, n;
  static handlerOverload *ho;

public: 
  static handlerOverload* setHandlerOverload( handlerOverload *handler );
  
  /** Initialisation. A appeler en debut de processus. 
     @param _hiMin puissance de 2 a partir de laquel les atomes sont la zone d'allocation superieure
     @param _hiMax puissance de 2 donnant la taille maximale d'un atome. Toutes les demandes d'allocation superieures genereront une chaine commencant par un ou plusieurs atomes de taille 2^hiMax.
  */
  static void    globalinit( guint _hiMin, guint _hiMax );
  /// Liberation des blocs d'allocation. Jamais utilise chez nous.
  static void    globalfree();

  /// Allocation d'une chaine d'atomes dont la taille totale (somme des tailles des atomes) est sz.
  void alloc( guint16 sz );
  /// Liberation d'une chaine d'atomes dont la taille est sz. Attention il faut memoriser la taille d'une chaine pour pouvoir la liberer.
  void free();

  /** Initialisation d'un triplet iterateur. Noter que cette fonction n'a pas besoin de ptr. 
   @param sz Taille de la chaine d'atomes (nombre d'elements)
   @param thisLen Reference a l'element thisLen du quadruplet a initialiser
   @param n Reference a l'element n du quadruplet a initialiser
  */
  static void    initIterator( guint16 sz, guint16& thisLen, guint16& n );
  /** Mettre a jour le quadruplet (ptr,sz,thisLen,n) afin qu'il pointe sur le quadruplet suivant
  */
  static guchar* nextChunk( guchar* ptr, guint16& sz, guint16& thisLen, guint16& n );

  static void check_access( guchar* ptr, gsize len );

  dyPointer() {
    curs = NULL; len = 0; thislen = 0; 
  }
  
  void init( guchar *base, guint16 wsz ) {
    curs = base;
    len = wsz;
    initIterator( len, thislen, n );
  }
 
  static guint mbTotalSize() { return (totalSize>>20)*INDEX_CODE_SIZE; }
   // MB allocated
   
  operator guchar*() { return curs; }
  operator char*() { return (char*)curs; }
  intIndex & operator *() { CHECK_MEM( curs, sizeof(intIndex) ); return *((intIndex*)curs); }
  void iterate();
  guint16 length() const { return len; }
  guint16 chunkLength() const { return thislen; }
  void   nextChunk();
  
  static guint*    memoryLimit; // in mb

private:
  static void whereIs( guchar *ptr, guint16 &n, guint16 &i, guint16 &j );
  static dyChunk *chainByAtomSize( guint atomSize );
  
  static dyChunk  **hiChunk;
  static dyChunk  **lowChunk;
  static guint    lowMax;
  static guint    hiMin;
  static guint    hiMax;
  static lsLockable *lowMutex;
  static lsLockable *hiMutex;
  static lsLockable  globalMutex;
  static guint64  totalSize;
  friend class dyChunk;
};

inline void  dyPointer::initIterator( guint16 sz, guint16& thisLen, guint16& n ) {

  if ( sz <= lowMax ) { thisLen = sz; n = 0; return; }
  for ( n = hiMax ; sz < (1<<n) ; n-- ) ;;
  thisLen = 1<<n;
}

inline void dyPointer::iterate() {

  curs += INDEX_CODE_SIZE;
  len--; thislen--;
  if ( !len ) return;
  if ( !thislen ) {
    if ( len > (sizeof(guchar*)/INDEX_CODE_SIZE) ) curs = *((guchar**)curs);
    initIterator( len, thislen, n );
  }
}

inline void dyPointer::nextChunk() {

  curs += INDEX_CODE_SIZE*thislen;
  len -= thislen; thislen = 0;
  initIterator( len, thislen, n );
  if ( !len ) return;
  if ( len > (sizeof(guchar*)/INDEX_CODE_SIZE) ) curs = *((guchar**)curs);
  CHECK_MEM( curs, thislen*INDEX_CODE_SIZE );
}

typedef compactList<dyPointer> dyCompactList;
typedef compactIterator<dyPointer> dyCompactIterator;

typedef compactListConvert<dyPointer> dyCompactListConvert;


#endif
