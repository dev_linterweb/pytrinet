import socket
import sys

# Create a UDS socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = '/tmp/foobar'
print ( 'connecting to %s' % server_address )
try:
    sock.connect(server_address)
except socket.error as msg:
    print >>sys.stderr, msg
    sys.exit(1)

try:
    
    # Send data
    message = "This is the message.  It will be repeated.\n  fezfe "
    print( 'sending "%s"' % message )
    sock.sendall( bytes(message, 'utf-8'))

    amount_received = 0
    amount_expected = len(message)
    
    data = sock.recv(30)
    print ( 'received "%s"' % data )

finally:
    print ( 'closing socket' )
    sock.close()

