cd $1

for name in `find -name *.bak`
do
    original=${name::-4}
    echo "$name > $original"
    mv $name $original
done