#ifndef _INDEX_DYNAMIC_H_
#define _INDEX_DYNAMIC_H_

#include "index.h"
#include "dymap.h"
#include "dyCompactList.h"

class lsDynEngine;
class wordIndexDynamic;

class articleIndexDynamic : public compactIndex<dyPointer> {

public:
  articleIndexDynamic( wordIndexDynamic* _words, lsDynEngine *_engine );
  virtual ~articleIndexDynamic();

  virtual   void attachToFile( const char * _fileName );
  virtual   void sync();
  void      syncConvert();

  intIndex  push( listIndex lWords, intIndex pageLimit, intIndex stamp );
  void      pop( intIndex article );
  bool      update( intIndex article, listIndex lWords, intIndex stamp );
  bool      fusionTitle( intIndex article, listIndex lWords, intIndex stamp );
  void      wordCount( intIndex nWords, intIndex* tWords );

  void      setArticleMaxWord( int _articleMaxWord ) {
                articleMaxWord = _articleMaxWord; }

  void      debug( intIndex id, mapWord * wordMap );
  void      debugTop( intIndex id, mapWord * wordMap );
  
  void      debugStatus();

  int      getNArticles();
  int      getNWords();
  bool  checkStructure( intIndex maxTarget );
  intIndex  checkDuplicate( intIndex id );


protected:

  wordIndexDynamic *words;
  intIndex     *maxList;
  intIndex      idx;

  lsDynEngine *engine;
  intIndex  articleMaxWord;

  friend class wordIndexDynamic;
};

class wordIndexDynamic : public compactIndex<dyPointer> {

public:
  wordIndexDynamic( wordMapDynamic *_map, lsDynEngine *_engine );
  virtual ~wordIndexDynamic();
  virtual void attachToFile( const char * _fileName );
  virtual void sync();
  void      syncConvert();

  void       notifyQuit();
  
  intIndex   push( const char *word, guint& nWordInsert );
  void      setArticles( articleIndexDynamic *_articles ) { articles = _articles; }
  void      setWordMaxArticle( int _wordMaxArticle ) { 
                wordMaxArticle = _wordMaxArticle; }
  intIndex  getWordMaxArticle() const { return wordMaxArticle; }
  bool  pushExtension( intIndex id );
  void      setArticleExtensionSize( int _articleExtensionSize ) {
             articleExtensionSize = _articleExtensionSize; }
  
  void      debug( intIndex id );
  void      debugExtension();
  int      getAsynch();
  void      allReSynch() { nAllResynch = 0; }
  bool  checkStructure( intIndex maxTarget );
  void      startSynchThread();
  void      syncExtension();
  bool      flushExtension();
  void      waitExtension();
  
  lsShrinker getWordShrinker( lsShrinker aShinker );
  
  wordMapDynamic* getMap() { return map; }
  
  typedef struct { extraIndex src; extraIndex target; } tscode;

  lsLockable extensionMutex;
  lsLockable syncMutex;

protected:
  static    void statRealizeExtension( wordIndexDynamic *wi );
  void      realizeExtension();
  bool  doFlushExtension();
      
  intIndex  wordMaxArticle;
  intIndex  articleExtensionSize;
  bool  bKill;
  wordMapDynamic *map;
  articleIndexDynamic *articles;
  lsDynEngine *engine;
  #define   N_EXTENSION 4
  intIndex  topExtension;
  listIndex extension[N_EXTENSION+1];
  listIndex buildExtension;
  intIndex  nAllResynch;
  bool  bAsynchExtension;
};

#endif
