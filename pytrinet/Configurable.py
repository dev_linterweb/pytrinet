class Configurable :

    parameters: tuple

    def __init__( self, config ):
        for (name,deflt) in self.parameters :
            setattr( self, name, config[name] if name in config else deflt )

    def saveConfig( self, config ) :
        for (name,deflt) in self.parameters :
            config[name] = getattr( self, name ).replace('%', '%%')

    def apiJSON(self) :
        rt = {}
        for (key,_) in self.parameters :
            rt[key] = getattr( self, key )
        return rt