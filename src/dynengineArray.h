#ifndef _TRINET_DYNAMIC_ENGINE_ARRAY_H_
#define _TRINET_DYNAMIC_ENGINE_ARRAY_H_

#include <Python.h>

#include <bits/stdc++.h>


#include "config.h"
#include "index.h"

class lsDynEngineArray : public lsConfig {

public:
    std::string test( const char * key );

    ~lsDynEngineArray();
    void init( const char * _path, const char * _lang );

    mapWord * get() { return wordMap; }

private:
    mapWord * wordMap;
};

#endif