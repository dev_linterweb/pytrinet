#ifndef _PY_SCORED_LIST_H_
#define _PY_SCORED_LIST_H_

#include <Python.h>
#include "list.h"

typedef struct {
    PyObject_HEAD
    listIndex list;
} PyScoredList;

extern PyTypeObject ScoredListType;

PyObject * PyScoredList_FromListIndex( listIndex list );

#endif