#ifndef _PY_DYNAMIC_ENGINE_ARRAY_H_
#define _PY_DYNAMIC_ENGINE_ARRAY_H_

#include <Python.h>
#include "dynengineArray.h"

typedef struct {
    PyObject_HEAD
    lsDynEngineArray * engineArray;
} PyDynamicEngineArray;

extern PyTypeObject DynamicEngineArrayType;

#endif