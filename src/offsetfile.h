#ifndef _OFFSET_FILE_H_
#define _OFFSET_FILE_H_

#include "mutex.h"
#include "config.h"
#include "dylist.h"
#include "list.h"

#include <cstdint>
#include <iostream>
#include <filesystem>

class lsOffsetFile : public lsLockable {
  
protected:
  intIndex              size;
  std::string           fileName;
  char                  magic[5];
  uint16_t              version;
  std::ifstream         in;
  std::ofstream         out;

public:
  void   openFile( bool bWrite ) {

    if ( bWrite ) {
      std::filesystem::create_directories(
          std::filesystem::path( fileName ).remove_filename()
      );
      out.open( fileName, std::ofstream::out|std::ofstream::binary );
    } else {
      in.open( fileName, std::ifstream::in|std::ifstream::binary );
    }
  }
  
  void   closeFile() {
    
    if ( in.is_open() ) in.close();
    if ( out.is_open() ) out.close();
  }
  
protected:

  void backup() {
    
    try {
      std::filesystem::path from( fileName );
      std::filesystem::path to( from ); to += ".bak";
      std::filesystem::rename( from, to );
    } catch (std::filesystem::filesystem_error& e) {
      std::cout << e.what() << '\n';
    }
  }

  void   loadHeader() {
   
    version = 0; size = 0;
    if ( in ) {
      char this_magic[5];
      in.read( (char*)this_magic, 4 );
      if ( !in  ) {
        std::cout << "Opening " << magic << " project file failed : cannot open file"<< std::endl;
    		return; 
      }
      this_magic[4]=0;
      if ( strcmp( magic, this_magic ) ) {
        std::cout << "Opening " << magic << " project file failed : wrong file type"<< this_magic << std::endl;
		    return; 
      }
      in.read( (char*)&version, 2 );
      if ( version > VERSION_CURRENT ) {
        std::cout << "Opening " << magic << " project file failed : wrong file version"<< std::endl;
		    return;
      }
      in.read( (char*)&size, 4 );
      if ( !in ) {
        std::cout << "Opening " << magic << " project file failed : no size found"<< std::endl;
		    return;
      }
    }
  }
  
  void   writeHeader() {

    if ( out ) {
      uint16_t version = VERSION_CURRENT;
      out.write( magic, 4 );
      out.write( (char*)&version, 2 );
      out.write( (char*)&size, 4 );
    }
  }
  
  template <class T> bool  load( T& target, intIndex len = sizeof(T) ) {
 
    if ( in ) {
      in.read( (char*)&target, len );
      return in &&( in.gcount() == len );
    }
    return false;
  }
  
  template <class T> void  save( T& target, intIndex len = sizeof(T) ) {
 
    if ( out ) {
      out.write( (char*)&target, len );
    }
  }

  intIndex loadToEnd( char *& data, intIndex len ) {
    
    in.read( data, len );
    return in.gcount();
  }
  
  void seek( intIndex offset ) {
   
    if ( in ) {
      in.seekg( offset );
    }
  }

public:

  lsOffsetFile( const char *_magic ) {

    std::memcpy( magic, _magic, 5 );
    size = 0; version = 0;
  }
  
  virtual ~lsOffsetFile() { 
    closeFile();  
  }
  
  virtual intIndex  length() const { return size; }
  
  virtual void  attachToFile( const char * _fileName ) {
    fileName = _fileName;
  }

  virtual void sync() {} // save to disk

  std::istream& getInStream() { return in; }

  std::ostream& getOutStream() { return out; }
};


#endif
