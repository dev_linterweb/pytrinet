#include "config.h"
#include <cstdio>
#include <filesystem>

lsConfig *lsConfig::defaultConfig = NULL;

cfLogLine::cfLogLine( const char *fileName, bool bTimeStamp ) {
    
    std::filesystem::create_directories(
        std::filesystem::path( fileName ).remove_filename()
    );

    out.open( fileName, std::ios::app|std::ios::out );
    if ( bTimeStamp ) {
        time_t rawtime;
        time( &rawtime );
        out << ctime( &rawtime );
    }
}

cfLogLine::~cfLogLine() {
    out << "\n";
    out.close();
}


void lsConfig::init( const char * _path, const char * _lang ) {
    lang = _lang;
    path = _path;
}

void lsConfig::clearLog( const char* logName ) {

    STR_FORMAT(log_path,"%s/log/%s",path.c_str(),logName);
    std::remove( log_path );
}

cfLogLine lsConfig::log( const char* logName, bool bTimeStamp ) {

    STR_FORMAT(log_path,"%s/log/%s",path.c_str(),logName);
    cfLogLine rt( log_path, bTimeStamp );
    return rt;
}

std::string lsConfig::buildPath( const char* fileName ) {
 // Crée le chemin configPath/fileName, le place dans tmp, tampon de longueur tmpSize.

    STR_FORMAT(file_path,"%s/%s/%s",path.c_str(),lang.c_str(),fileName);
    return std::string(file_path);
}
