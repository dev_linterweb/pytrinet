#include "dystorage.h"

lsDynamicStorage::lsDynamicStorage( const char *_magic ) : Tparent( _magic ) {

  bGlobalInvalidate = false;
}

void lsDynamicStorage::load() {

  bGlobalInvalidate = false;
  loadHeader();
  if ( !version ) {
    size = 0; return;
  }
  list.alloc( size );
  std::cout << "Alloc'ed " << size << std::endl;

  guint i;

  in.seekg( length()*sizeof(intIndex), std::ios_base::cur );
  for ( i = 0 ; ( i < length() )&&( list[i].loadWithPrefix( in, version ) ) ; i++ ) {}
  if ( i < length() ) {
    DEBUG_LOG << fileName << " unreadable from article #" << i;
    size = i;
  }
}

void lsDynamicStorage::attachToFile( const char* fileName ) {

  lsOffsetFile::attachToFile( fileName );
  openFile(false);
  std::cout << "Attached to " << fileName << std::endl;
  load();
  // CAUTION : if ( bKeepOpen ), this storage keeps [in] opened for reading the post data
  // it NEEDS to be detached further by detachIo()
}

// Get indexes for which we store a non empty list
listIndex lsDynamicStorage::listNonEmptyIndexes() {

  intIndex count = 0;
  for ( intIndex i = 0 ; i < length() ; i++ ) {
    if ( list[i].length() ) count++;
  }
  listIndex rt( count );
  for ( intIndex i = 0 ; i < length() ; i++ ) {
    if ( list[i].length() ) rt.append(i,1);
  }

  return rt;
}

void lsDynamicStorage::sync() {

  backup();

  // Storage

  size = length();
  openFile( true );
  writeHeader();

  intIndex  offset = 0x0A + length()*sizeof(intIndex);
  for ( intIndex i = 0 ; i < length() ; i++ ) {
    lsOffsetFile::save<intIndex>( offset );
    offset += list[i].saveLength();
  }
  for ( intIndex i = 0 ; i < length() ; i++ ) list[i].saveWithPrefix( out );
  bGlobalInvalidate = false;
  // CAUTION : this storage keeps [in] opened for writing the post data
  // it NEEDS to be detached further by detachIo()
}

// Stores itself in a file, using compactListConvert type of compactList : one shot operation to store in a different format
void lsDynamicStorage::syncConvert() {

  backup();

  // Storage

  size = length();
  openFile( true );
  writeHeader();

  intIndex  offset = 0x0A + length()*sizeof(intIndex);
  listIndex current(4096);

  for ( intIndex i = 0 ; i < length() ; i++ ) {
    lsOffsetFile::save<intIndex>( offset );
    list[i].toList( current );
    intIndex storageSize = (2+dyCompactListConvert::wSize( current ))*INDEX_CODE_SIZE;
    offset += storageSize;
  }

  for ( intIndex i = 0 ; i < length() ; i++ ) {
    list[i].toList( current );
    dyCompactListConvert converted( current );
    converted.fromList( current );
    converted.saveWithPrefix( out );
    converted.free();
  }

  current.free();
  
  bGlobalInvalidate = false;
  // CAUTION : this storage keeps [in] opened for writing the post data
  // it NEEDS to be detached further by detachIo()
}

void lsDynamicStorage::shrink( lsShrinker sShrinker, lsShrinker tShrinker ) {

  intIndex i = 0;
  for ( i = 0 ; i < length() ; i++ ) {
    intIndex iTarget = sShrinker.rewrite(i);
    if (( iTarget != intIndexInvalid )&&( i != iTarget )) list[iTarget] = list[i];
    if ( iTarget == intIndexInvalid ) list[i].free();
  }
  size = list.size = std::min( size, sShrinker.targetLength() );
  for ( intIndex i = 0 ; i < length() ; i++ ) list[i].shrink( tShrinker );
}

