# Receives push requests and ignores the body. This explorer downloads the pushed urls,
# contrary to standard passive explorers (Explorer instances), where the push method
# expects the document body.

from io import StringIO
import re
import traceback
import requests
from urllib.parse import urlparse
from pytrinet.explorer.Explorer import Explorer
from pytrinet.wordStreamer.Tokenizer import Tokenizer

MAX_DOWNLOAD_BYTES = 4*1024*1024

class PassiveHttpExplorer(Explorer):

    parameters = (
        ('autoSave','100'),
        ('useDomainMetawords','true'),
    )

    def __init__( self, project, config, id ):
        super().__init__( project, config, id )
        self.useTimestamps = True

    def detectTitle( self, body ) :
        m = re.search(r'<\s*title\s*>([^<]+)<\s*/\s*title\s*>',body)
        return m.group(1).strip() if m else False


    def push( self, url, body, meta:dict = None, lang:str = None, metaWords:tuple=None, stamp=0 ):
        try :
            self.log("{} > Passive push".format(url))
            body = False # This implementation of push discards the given body
            bodyLength = 0
            try :
                r = requests.get( url, timeout=4, headers={'User-Agent': 'https://toots.cat'}, stream=True )
                contentType = r.headers.get('content-type')

                if not 'html' in contentType :
                    self.log('Dropping {} : content type = {}'.format(url,contentType))
                    raise Exception()

#               contentLength = r.headers.get('content-length')
#               if not contentLength or contentLength > MAX_DOWNLOAD_BYTES :
#                    self.log('Dropping {} : content length = {}'.format(url,contentLength))
#                    raise Exception()

                r.raise_for_status()
                self.log('Got a {} on {}'.format(r.status_code,url))
                
                title = False

                body = r.text
                title = self.detectTitle(body)
                self.log("Title {}".format(title))
                bodyLength = len(body)
                body = StringIO(body) # would love to use r.iter_content, but iter_content does not guess encoding as well
                
            except Exception :
                self.log('{} > Could not download ({})'.format(url,r.status_code if r else ''))

            if bodyLength :

                if self.useDomainMetawords:
                    o = urlparse(url)
                    if o and o.hostname :
                        hostname = o.hostname.split('.')
                        domainMetaWords = list(metaWords) if metaWords else []
                        for i in range(0,len(hostname)-1) :
                            domainMetaWords.append('domain:'+('.'.join(hostname[i:])))
                        metaWords = tuple(domainMetaWords)

                if title :
                    if not meta : meta = {}
                    meta['title'] = title
                self.log('{} > pushing {}B with metawords {} and meta {}'.format(url,bodyLength,metaWords,meta))
                self.project.pushDocument( url, body, meta, lang, metaWords, logger=self, stamp=stamp )

            self.count += 1
            if self.autoSave and int(self.autoSave) and self.count >= int(self.autoSave) :
                self.count = 0
                self.project.save()
            
        except Tokenizer.BufferOverflowException :
            self.log(" > Buffer overflow exception. Dropping this one and continuing.")
        except Exception :
            self.log(traceback.format_exc())
