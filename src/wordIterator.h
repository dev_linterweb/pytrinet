#ifndef _WORD_ITERATOR_H_
#define _WORD_ITERATOR_H_

#include "lintersearch.h"
#include "buffer.h"

/**
 * @brief Iterates on a char buffer which contains a list of white space separated words.
 * 
 */
class lsWordIterator {

private:
    lsBufferConst buffer;
    size_t        offset;

public:
    lsWordIterator( lsBufferConst _buffer ) : buffer( _buffer ) {
        offset = 0;
    }

    lsWordIterator( const char * _buffer ) : buffer( _buffer ) {
        offset = 0;
    }

    lsBufferConst nextWord() {
        size_t begin = offset;
        while (( offset < buffer.getSize() )&&( buffer[offset] != ' ' )) offset++;
        size_t end = offset;
        while (( offset < buffer.getSize() )&&( buffer[offset] == ' ' )) offset++;
        return lsBufferConst( buffer, begin, end );
    }

    bool done() const {
        return offset >= buffer.getSize();
    }

    void rewind() { offset = 0; }

};

#endif