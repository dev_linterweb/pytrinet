import re
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError
from bson.objectid import ObjectId
from pytrinet.DynamicEngine import ArticleMap
import hashlib

# In mongodb collection url : field trinet should be indexed unique but sparse
# db.resident.createIndex( { trinet : 1 }, { name: "trinet_sparse_index", unique: true, sparse: true } )

class WikwixArchiveExplorer(ArticleMap) :

    parameters = (
        ('archiveIteratorUrl',''),
        ('adminToken',''),
        ('iterator',''),
        ('autoSave','100'),
        ('mongoUrl','mongodb://127.0.0.1:27017'),
        ('mongoDatabase','resident')
    )

    reUrl = re.compile("(https?|ftp)://")
 
    def __init__( self, project, config ):
        super().__init__( config )
        self.project = project
        self.nRetry = 0
        mongodb_client = MongoClient(self.mongoUrl)
        database = mongodb_client[self.mongoDatabase]
        self.collection = database['urls']
        self.field = 'trinetid'

    def splitLangIndex( self, langIndex ):
        for i in range(len(langIndex)) :
            if langIndex[i].isdigit() : break
        return langIndex[:i],langIndex[i:]

    # == Map interface =====

    def getHash( self, url:str ) :
        return ( hashlib.new( 'ripemd160', url.encode() ).hexdigest() )[0:24]

    def getUrl( self, lang: str, id: int ) -> str:
        result = self.collection.find_one({self.field:'{}{}'.format(lang,id)},{'redirurl':1})
        return result['redirurl'] if result else None

    def getMeta( self, lang: str, id: int ) -> dict:
        result = self.collection.find_one({self.field:'{}{}'.format(lang,id)},{'redirurl':1,'meta':1,'title':1})
        if not result : return None
        meta = result['meta'] if 'meta' in result else {}
        meta['title'] = result['title'] if 'title' in result else result['redirurl']
        meta['url'] = result['redirurl']
        return meta

    def get( self, lang: str, id: int ) -> str:
        result = self.collection.find_one({self.field:'{}{}'.format(lang,id)},{'redirurl':1,'meta':1,'title':1})
        meta = result['meta'] if ( result and 'meta' in result ) else {}
        if result : 
            meta['title'] = result['title'] if 'title' in result else result['redirurl']
            meta['url'] = result['redirurl']
        else :
            meta['url'] = 'unknown'
            meta['title'] = 'unknown'
        return {'url':result['redirurl'],'meta':meta} if result else {}

    def gets( self, lang: str, ids: list ) :
        results = self.collection.find({self.field:{'$in':['{}{}'.format(lang,id) for id in ids ]}},{'redirurl':1,'meta':1,'title':1,self.field:1})
        rt = {}
        for result in results :
            rt[int(result[self.field][len(lang):])] = { 
                'url':result['redirurl'], 
                'meta':{ 
                    'url': result['redirurl'], 
                    'title': result['title'] if 'title' in result else result['redirurl'] 
                } 
            }
        return rt

    def getId( self, url: str ) -> tuple:  # returns (lang,id) if url exists, or (None,None)
        mongoid = ObjectId(self.getHash(url))
        result = self.collection.find_one({'_id':mongoid})
        if result and self.field in result :
            (lang,id) = self.splitLangIndex(result[self.field])
            return (lang,int(id))
        else : return (None,None)


    def set( self, lang: str, id: int, url: str, meta: dict ):
        mongoid = ObjectId(self.getHash(url))
        trinet_id = lang+str(id)
        data = {self.field:trinet_id}
        if meta :
            data['meta'] = meta
        try:
            self.collection.update_one(
                {'_id':mongoid},
                {'$set':data}
            )
        except DuplicateKeyError:
            self.collection.update_one(
                {self.field:trinet_id},
                {'$unset':{self.field:'','meta':''}}
            )
            self.collection.update_one(
                {'_id':mongoid},
                {'$set':data}
            )

    def delete( self, url: str ):
        mongoid = ObjectId(self.getHash(url))
        self.collection.update_one(
            {'_id':mongoid},
            {'$unset':{self.field:''}}
        )

    # == Explorer interface =====

"""     def doExplore(self) :
        
        if len(self.iterator)>0 :

            print('Getting document at index {}'.format(self.iterator))
            r = requests.post( self.archiveIteratorUrl, data={'from':self.iterator,'admin':self.adminToken} ) #, stream=True )
            if r.status_code != 200 : 
                raise Exception('Failed to contact {} with from={}'.format(self.archiveIteratorUrl,self.iterator))

            response = r.text.split('\n',3)

            if len(response)<2 or not len(response[0]) :
                self.iterator = ''
                self.project.syncExtension()
                self.project.save()
                raise Exception('When contacting {} with from={}, got uncomplete header : {}'.format(self.archiveIteratorUrl,self.iterator,response))

            self.iterator = response[1]

            url = response[0]
            if not self.reUrl.match(url) :
                raise Exception('Got incorrect header {}',r.text[:1000])

            print('Got url {}'.format(url))
            r.close()
            del r
            
            if len(response)>=4:
                body = StringIO(response[3]) # not streaming
                print("Pushing ",url)
                self.project.pushDocument( url, body )
                del body
            return True
        else:
            return False
            
    def reset(self):
        self.iterator = '0' """

    