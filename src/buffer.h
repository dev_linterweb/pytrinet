#ifndef _BUFFER_H_
#define _BUFFER_H_

#include "lintersearch.h"

#include <string>
#include <iostream>

class lsBufferConst {

private:
    const char *buffer;
    size_t size;

public:
    lsBufferConst( const char *_buffer, size_t _size ) {
        buffer = _buffer;
        size = _size;
    }

    lsBufferConst( const char *_buffer ) {
        buffer = _buffer;
        size = strlen(buffer);
    }

    lsBufferConst( const lsBufferConst & b ) {
        buffer = b.buffer;
        size = b.size;
    }

    lsBufferConst( const lsBufferConst & b, size_t begin, size_t end ) {
        buffer = b.buffer+begin;
        size = end-begin;
    }

    lsBufferConst( const std::string & str ) {
        buffer = str.c_str();
        size = str.length();
    }

    lsBufferConst & operator = ( const lsBufferConst & b ) {
        buffer = b.buffer;
        size = b.size;
        return *this;
    }

    char operator[] ( size_t pos ) const { return buffer[pos]; }

    std::string getString() const {
        return std::string( buffer, size );
    }

    void assignTo( std::string & s ) const {
        s.assign( buffer, size );
    }

    void assignTo( char * dest, size_t destSize ) const {
        size_t n = std::min( destSize-1, size );
        memcpy( dest, buffer, n );
        dest[n] = 0;
    }

    size_t getSize() const { return size; }

    const char * getBuffer() const { return buffer; }

    friend std::ostream& operator << ( std::ostream& os, const lsBufferConst& buffer );
    friend std::string& operator += ( std::string& s, const lsBufferConst& buffer );

    bool contains( char c ) {
        for ( size_t i = 0 ; i < size ; i++ ) {
            if ( buffer[i] == c ) return true;
        }
        return false;
    }
};

inline std::ostream& operator << ( std::ostream& os, const lsBufferConst& buffer ) {
  os.write(buffer.buffer, buffer.size);
  return os;
}

inline std::string& operator += ( std::string& s, const lsBufferConst& buffer ) {
    s.append( buffer.buffer, buffer.size );
    return s;
}



#endif