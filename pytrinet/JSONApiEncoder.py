
from json import JSONEncoder
from bson.objectid import ObjectId

class JSONApiEncoder(JSONEncoder):

    def default(self, object):

        if callable(getattr(object,'apiJSON',None)):
            return object.apiJSON()

        if isinstance( object, ObjectId ) :
            return str(object)

        else:

            # call base class implementation which takes care of

            # raising exceptions for unsupported types

            return JSONEncoder.default(self, object)

