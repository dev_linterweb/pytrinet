import argparse
import os
import trinet
from pytrinet.ProjectArray import ProjectArray


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Command line tool for manipulating trinet indexes')
    parser.add_argument('--path')
    args = parser.parse_args()

    if not args.path :
        print("Please provide a path to the project with --path=")
        os._exit(os.EX_OK)

    projectArray = ProjectArray( args.path )

    projectName = ''
    lang = 'fr'

    while True :
        line = input('trinet: {}/{} > '.format(projectName,lang))
        line = line.split()

        if not line : line = ['']

        if line[0] == 'project' :
            _projectName = line[1]
            _project = projectArray.getProject(_projectName)
            if not _project :
                print("Could not find project {}".format(_projectName))
            else :
                project = _project
                projectName = _projectName
                print("Selected project {}".format(projectName))

        elif line[0] == 'lang' :
            lang = line[1]
            print("Selecting language {}".format(lang))
        
        elif line[0] in [ 'word', 'article', 'status', 'articleTop' ] :
            engine = project.getEngine(lang)
            if not engine :
                print("Could not get engine for project {} and language {}".format(projectName,lang))
            else :
                engine.debug( line[0], line[1] if len(line)>1 else '' )

        elif line[0] == 'articleMap' :
            articleId = int(line[1])
            print( project.map.get( lang, articleId ) )

        elif line[0] == 'exit' :
            os._exit(os.EX_OK)

        elif line[0] == 'convert' :
            projectArray.convertAll()

        elif line[0] == 'classify' :
            classifierProject = projectArray.getProject('classifier')
            articleId = int(line[1])
            engine = project.getEngine(lang)
            classIds = classifierProject.getEngine(lang).classify( engine, articleId )
            meta = project.map.get( lang, articleId )

            for id in classIds :
                print ( classifierProject.map.get( lang, id )['url'], ' ' )
                
            print ( meta, "\n\n" )

        elif line[0] == 'classifyall' :
            out = open( line[1], 'w' )
            classifierProject = projectArray.getProject('classifier')
            engine = project.getEngine(lang)
            nArticles = engine.infos()[0]
            for id in range(0,nArticles) :
                classIds = classifierProject.getEngine(lang).classify( engine, id )
                meta = project.map.get( lang, id )
                for id in classIds :
                    out.write ( classifierProject.map.get( lang, id )['url'] )
                    out.write ( ' ' )
                out.write( "{}\n\n".format( meta ) )

        elif line[0] == 'test' :
            print(project.getsMap('fr',[11,23,41,91,120]))

        elif line[0] == 'removeOrphans' :
            project.removeOrphans( lang )