#include "shrinker.h"
#include <string.h>
#include <iostream>


lsShrinker::lsShrinker( intIndex _len ) {
  len = _len;
  trewrite = new intIndex[len+1]; // trewrite[len] is the reference counter
  trewrite[len] = 1;
  targetLen = 0;
}

lsShrinker::~lsShrinker() {
  if ( !--trewrite[len] ) delete [] trewrite;
}

lsShrinker & lsShrinker::operator = ( const lsShrinker &s ) {
 
  len = s.len;
  targetLen = s.targetLen;
  trewrite = s.trewrite;
  trewrite[len]++;
  return *this;
}
  
void lsShrinker::removeAll() {
  memset( trewrite, ~0, len*sizeof(intIndex) );
}

void lsShrinker::keep( intIndex id ) {
  if ( id < len ) trewrite[id] = 0; 
}
    
void lsShrinker::build() {
  targetLen = 0;
  for ( intIndex i = 0 ; i < len ; i++ ) if ( !trewrite[i] ) trewrite[i] = targetLen++;
}

void lsShrinker::debug() {
 
  intIndex ct = 0;
  for ( intIndex i = 0 ; i < len ; i++ ) {
    std::cout << "(" << i;
    if ( trewrite[i] == intIndexInvalid ) std::cout << ") ";
    else std::cout << "," << trewrite[i] << ") ";
    if ( ct++ > 20 ) { std::cout << std::endl; ct = 0; }
  }
  if ( ct ) std::cout << std::endl;
}