import React from 'react';

import ControlledInput from './controlledInput'

import checkLine from './img/check-line.png'
import pencilLine from './img/pencil-line.png'
import closeLine from './img/close-line.png'
import './editable.css';

class Editable extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            folded:true,
            value:props.value
        }
    }

    onChange() {
        this.setState({folded:true})
        this.props.onSubmit(this.state.value)
    }

    onCancel() {
        this.setState({folded:true,value:this.props.value})
    }

    unfold() {
        this.setState({folded:false})
    }

    render() {
        return this.state.folded ? 
            <div className="editable" onClick={this.unfold.bind(this)}>
                <span>{this.props.value}</span>
                <img src={pencilLine}/>
            </div> :
            <div className="editable">
                <ControlledInput type='text' property='value' container={this} onSubmit={this.onChange.bind(this)}/>
                <img src={checkLine} onClick={this.onChange.bind(this)}/>
                <img src={closeLine} onClick={this.onCancel.bind(this)}/>
            </div>
    }
}

export default Editable;
