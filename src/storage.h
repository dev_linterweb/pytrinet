#ifndef _STORAGE_H_
#define _STORAGE_H_

#include "offsetfile.h"
#include "compactList.h"

template <class Tpointer> class lsStorage : public lsOffsetFile {

public:
  lsStorage( const char *_magic ) : lsOffsetFile( _magic ) {}
  virtual void shrink( lsShrinker sShrinker, lsShrinker tShrinker ) {}
  virtual void decay( guint spare ) {}
  /** Stores itself in a file, using compactListConvert type of compactList : one shot operation to store in a different format */
  virtual void syncConvert();

  virtual void access( listIndex l, compactList<Tpointer> *lst, bool bLazy ) {}
  virtual compactList<Tpointer> access( intIndex idx ) { return compactList<Tpointer>(); }

  virtual void replace( intIndex idx, compactList<Tpointer> elt ) {}

  virtual intIndex findFreeIndex( intIndex pageLimit ) { return intIndexInvalid; } // Only in dynamic storages

  virtual listIndex listNonEmptyIndexes()  { return listIndex(); } // Get indexes for which we store a non empty list
};



#endif
