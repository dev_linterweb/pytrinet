#ifndef _CONFIG_H_
#define _CONFIG_H_

#include <Python.h>

#include "lintersearch.h"

#include <bits/stdc++.h>
#include <iostream>

class cfLogLine {
public:

 cfLogLine() {}
 cfLogLine( const char *fileName, bool bTimeStamp );
 cfLogLine( cfLogLine& ll ) {
    out.swap( ll.out );
 }
 cfLogLine& operator = ( cfLogLine& ll ) {
    out.swap( ll.out );
    return *this;
 }
 ~cfLogLine();

template<class T> cfLogLine& operator << ( T t ) {
    out << t;
    return *this;
}
 
protected:
 std::ofstream out;
};

class lsConfig {

public:
    lsConfig() {
        if ( !defaultConfig ) defaultConfig = this;
    }
    ~lsConfig() {
        if ( defaultConfig == this ) defaultConfig = NULL;
    }

    void init( const char * _path, const char * _lang );

    std::string lang;
    std::string path;

    void        clearLog( const char* logName );
    cfLogLine   log( const char* logName, bool bTimeStamp=true );
    std::string buildPath( const char* fileName );

    static lsConfig * defaultConfig;
};

#endif