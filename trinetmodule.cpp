#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>

#include "src/pyDynamicEngine.h"
#include "src/pyDynamicEngineArray.h"
#include "src/pyScoredList.h"

static struct PyModuleDef trinetmodule = {
    PyModuleDef_HEAD_INIT,
    .m_name = "trinet",
    .m_doc = "Trinet search engine for Python",
    .m_size = -1,
};

PyMODINIT_FUNC
PyInit_trinet(void) {

    dyPointer::globalinit( 4, 10 ); // Memory chunks tuning

    PyObject *m;

    if (PyType_Ready(&DynamicEngineType) < 0)
        return NULL;

    if (PyType_Ready(&DynamicEngineArrayType) < 0)
        return NULL;

    if (PyType_Ready(&ScoredListType) < 0)
        return NULL;

    m = PyModule_Create(&trinetmodule);
    if (m == NULL)
        return NULL;

    Py_INCREF(&DynamicEngineType);
    if (PyModule_AddObject(m, "_DynamicEngine", (PyObject *) &DynamicEngineType) < 0) {
        Py_DECREF(&DynamicEngineType);
        Py_DECREF(m);
        return NULL;
    }
    Py_INCREF(&DynamicEngineArrayType);
    if (PyModule_AddObject(m, "DynamicEngineArray", (PyObject *) &DynamicEngineArrayType) < 0) {
        Py_DECREF(&DynamicEngineArrayType);
        Py_DECREF(m);
        return NULL;
    }
    Py_INCREF(&ScoredListType);
    if (PyModule_AddObject(m, "ScoredList", (PyObject *) &ScoredListType) < 0) {
        Py_DECREF(&ScoredListType);
        Py_DECREF(m);
        return NULL;
    }

    return m;
}


