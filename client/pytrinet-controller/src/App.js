import React from 'react';
import './App.css';

import Project from './Project.js'
import BackendSelect from './BackendSelect';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
        projects:[],
        exited:false
    }
  }

  componentDidMount() {
    this.props.api.registerAsProjectView(this)
    this.props.api.get( '*', 'list' )
  }

  componentWillUnmount() {
    this.props.api.unregisterAsProjectView(this)
  }

  updateProjects(res) {
    this.setState({projects:res})
  }

  updateProject(res) {
    this.setState({
      projects:this.state.projects.map(project=>{
        return ( project.name == res.name )? res : project
      })
    })
    if ( res.stopping ) {
      setTimeout(
        this.props.api.get.bind( this.props.api, res.name, 'get' ),
        1000
      )  
    }
  }

  onAction( method, project, action, what=false, onSuccess=false ) {
    console.log(method)
    switch ( method ) {
      case 'get':
        this.props.api.get( project, action, onSuccess )
        break
      case 'post':
        this.props.api.post( project, action, what, onSuccess )
        break
    }
  }

  saveAndQuit() {
    this.onAction( 'get', '*', 'save', false, this.quit.bind(this) )
  }

  quit() {
    let me = this
    this.onAction( 'get', '*', 'quit', false, (res)=>{
      if ( res.quitting ) {
        me.setState({exited:true})
      }
    } )
  }

  render() {
    let list = this.state.projects.map( project => 
      <Project key={project.name} project={project} onAction={this.onAction.bind(this)}></Project>
    ) 
    let quitNotice = this.state.exited ? 
      <div className="exited">Process exited</div> : ''
    return (
      <React.Fragment>
      {quitNotice}
      <div className="toolbox">
        <BackendSelect api={this.props.api}/>
        <button onClick={this.saveAndQuit.bind(this)}>Save &amp; Quit</button>
        <button onClick={this.quit.bind(this)}>Quit</button>
      </div>
      <table className="App">
        <tbody>
          {list}
        </tbody>
      </table>
      </React.Fragment>
    );
  }
}

export default App;
