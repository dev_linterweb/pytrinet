#ifndef _DYLIST_H_
#define _DYLIST_H_

#include <algorithm>
#include <iostream>

template <class T> 
class dyList {

public:
  dyList() { size = 0; maxSize = 0; list = NULL; }
  dyList( intIndex sz ) { alloc(sz); }
  ~dyList() { if ( list ) free(list); }
  void steal( dyList &l ); // steal dynamic data from [l] and reset [l] to emptyness

  void grow();
  void growUpto( intIndex sz, T fillWith );
  void alloc( intIndex sz );
  void append( T& elt );
  template <class Tchild> void append( dyList<Tchild>& l );
  void ioRead( std::istream& in );
  void ioWrite( std::ostream& out, intIndex n, T fillWith );
  void reset( bool bMemFree = false );
  void collapse(); // forbid adjascent equal elements by removing the rightmost one
  void collide(); // forbid adjascent equal elements by asking the leftmost one to "collide" with the rightmost one, then
                  // removing the rightmost one
  void removeInvalids(); // removes elements whose method valid() answers false
  void removeInvalids( bool ( * valid ) ( const T& ) );
    // remove elements such that given to the function 'valid', it returns false
  void remove( T* elt ); // remove element *elt (as returned by find, pointer into the list)
  
  T&   last() { return list[size-1]; }
  const T&   last() const { return list[size-1]; }
  
  void qsort( int ( * comparator ) ( const void *, const void * ) ) {
    if ( size ) ::qsort( list, size, sizeof(T), comparator );
  }
  void qsort() {
    if ( size ) ::qsort( list, size, sizeof(T), comp_t );    
  }
  void stlsort() { // T must be 'LessThan Comparable' in the sense of stl
    if ( size ) std::sort( list, list+size ); 
  }
  T*  find( T& t, int ( * comparator ) ( const void *, const void * ) );
  T*  findUnsorted( T& t );

  T& operator [] ( intIndex idx ) { return list[idx]; }
  const T&  operator [] ( intIndex idx ) const { return list[idx]; }
  void forEach( void (*func)(T&,void*), void* data = NULL );

  T *list;

  intIndex size;
  intIndex maxSize;
  intIndex maxSizeRealloc( intIndex sz );
  
  private:
    static int comp_t( const void *t1, const void *t2 );
};

template <class T> int dyList<T>::comp_t( const void *t1, const void *t2 ) {
 
  const T* tt1 = (const T*)t1;
  const T* tt2 = (const T*)t2;
  if ( *tt1 > *tt2 ) return +1;
  if ( *tt1 < *tt2 ) return -1;
  return 0;
}

template <class T> void dyList<T>::grow() {

  size++;
  if ( size > maxSize ) {

    maxSize = maxSizeRealloc( maxSize );
    if ( list ) list = (T*)realloc( list, maxSize*sizeof(T) );
    else list = (T*)malloc( maxSize*sizeof(T) );
  }
  list[size-1] = T();
}

template <class T> void dyList<T>::append( T& elt ) {

  grow();
  list[size-1] = elt;
}

template <class T> template <class Tchild> void dyList<T>::append( dyList<Tchild>& l ) {
 
  intIndex sz = size + l.size;
  if ( sz > maxSize ) {
    maxSize = maxSizeRealloc( sz );
    if ( list ) list = (T*)g_realloc( list, maxSize*sizeof(T) );
    else list = (T*)malloc( maxSize*sizeof(T) );
  }
  for ( intIndex i = 0 ; i < l.size ; i++ ) list[size+i] = l.list[i];
  size = sz;
}

template <class T> void dyList<T>::alloc( intIndex sz ) {

  maxSize = maxSizeRealloc( sz );
  size = sz;
  list = (T*)malloc( maxSize*sizeof(T));
}

template <class T> void dyList<T>::growUpto( intIndex sz, T fillWith ) {

  sz++; // Nombre d'éléments = (dernier élément: sz) +1
  if ( sz > maxSize ) {
    maxSize = maxSizeRealloc( sz );
    if ( list ) list = (T*)realloc( list, maxSize*sizeof(T) );
    else list = (T*)malloc( maxSize*sizeof(T) );
  }
  for ( ; size < sz ; size++ ) list[size] = fillWith;
}

template <class T> intIndex dyList<T>::maxSizeRealloc( intIndex sz ) {

  intIndex rt = (intIndex)(DYLIST_RATIO_ALLOC*sz);
  if ( rt < DYLIST_MIN_ALLOC ) return DYLIST_MIN_ALLOC;
  return rt;
}

template <class T> void dyList<T>::forEach( void (*func)(T&,void*), void* data ) {

  for ( guint i = 0 ; i < size ; i++ ) func( list[i], data );
}

template <class T> void dyList<T>::ioRead( std::istream & in ) {

  if ( in ) in.read( list, size*sizeof(T) );
}

template <class T> void dyList<T>::ioWrite( std::ostream & out, intIndex n, T fillWith ) {

  if ( out ) {
    out.write( list, std::min( n, size )*sizeof(T) );
    for ( int i = size ; i < n ; i++ ) {
      out.write( &fillWith, sizeof(T) );
    }
  }
}

template <class T> void dyList<T>::reset( bool bMemFree ) {

  size = 0;
  if ( bMemFree ) {
    maxSize = 0;
    if ( list ) {
      free(list); 
    }
    list = NULL;
  }
}

template <class T> void dyList<T>::collapse() {
 
  if ( !size ) return;
  guint s = 1, d = 1;
  for ( ; s < size ; s++ ) 
    if ( !(list[d-1] == list[s]) ) {
      if ( d != s ) list[d] = list[s];
      d++;
    }
  size = d;
}

template <class T> void dyList<T>::collide() {
 
  if ( !size ) return;
  guint s = 1, d = 1;
  for ( ; s < size ; s++ ) 
    if ( !(list[d-1] == list[s]) ) {
      if ( d != s ) list[d] = list[s];
      d++;
    } else list[d-1].collide( list[s] ); 
  size = d;
}

template <class T>  void dyList<T>::steal( dyList &l ) {

  if ( list ) free( list );
  list = l.list; l.list = NULL;
  size = l.size; l.size = 0;
  maxSize = l.maxSize; l.maxSize = 0;
}

template <class T>  T*  dyList<T>::find( T& t, int ( * comparator ) ( const void *, const void * ) ) {

  return size ? (T*) bsearch( (const void*)&t, list, size, sizeof(T), comparator ) : NULL;
}


template <class T>  T*  dyList<T>::findUnsorted( T& t ) {
  T* tt = list;
  for ( guint ct = 0 ; ct < size ; tt++, ct++ ) if ( *tt == t ) return tt;
  return NULL;
}


template <class T>  void dyList<T>::removeInvalids() {
  guint  d = 0;
  for ( guint s = 0 ; s < size ; s++ ) {
    if ( list[s].valid() ) {
      if ( d!=s ) list[d] = list[s];
      d++;
    }
  }
  size = d;
}

template <class T> void dyList<T>::remove( T* elt ) {
  guint id = elt-list;
  if ( id >= size ) return;
  memmove( elt, elt+1, (size-id-1)*sizeof(T) );
  size--;
}

template <class T> void dyList<T>::removeInvalids( bool ( * valid ) ( const T& ) ) {
  
  guint  d = 0;
  for ( guint s = 0 ; s < size ; s++ ) {
    if ( valid( list[s] ) ) {
      if ( d!=s ) list[d] = list[s];
      d++;
    }
  }
  size = d;
}


#endif 
