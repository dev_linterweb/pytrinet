
from pytrinet.wordStreamer.WordStreamer import WordStream
from langdetect import detect_langs
from langdetect.lang_detect_exception import LangDetectException

# Takes a few words at the beginning of the stream to detect language, then continue 
# forwarding the rest of the stream

class LangDetectWordStream :

    lang: str

    def __init__( self, sourceStream: WordStream, minWords: int = 256 ):
        self.sourceStream = sourceStream
        self.head = []
        words = []
        for _ in range(minWords) :
            token = sourceStream()
            if not token : break
            self.head.append( token )
            words.append(token[0])
        if len(words)>12 :
            words = ' '.join(words)
            try :
                langs = detect_langs(words)
                if not len(langs) : self.lang = 'en'
                elif len(langs)==1 : self.lang = langs[0].lang
                elif langs[0].lang == 'en' and 3*langs[1].prob>langs[0].prob :
                    self.lang = langs[1].lang
                else :
                    self.lang = langs[0].lang

#                self.lang = detect(words)

            except LangDetectException:
                self.lang = 'en' # ... wild guess
        else :
            self.lang = 'en' # ... wild guess
        self.curs = 0

    def __call__(self):
        if self.curs < len(self.head) :
            self.curs += 1
            return self.head[self.curs-1]
        return self.sourceStream()

