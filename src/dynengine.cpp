#include "dynengine.h"
#include <algorithm>
#include <iostream>
#include <filesystem>

void lsDynEngine::init(const char *_path, const char *_lang, lsDynEngineArray *_array)
{

  array = _array;
  fractIndex = 6;
  maxIndexSize = 1024;
  lsConfig::init(_path, _lang);
  bStatic = false;
  time(&lastAutoSave);
  wordMap = array->get();
  articles = new articleIndexDynamic((wordIndexDynamic *)(words = new wordIndexDynamic(
    (wordMapDynamic *)(wordMap), this)),
    this);
  { WriteLock _( &(dynWords()->syncMutex) );
    articles->attachToFile(buildPath("article.index").c_str());
    words->attachToFile(buildPath("word.index").c_str());
  }
  if ( ! isClassifier() ) {
    dynWords()->startSynchThread();
  }
}

lsDynEngine::~lsDynEngine() {

  if (words)
    delete words;
  if (articles)
    delete articles;
}

std::string lsDynEngine::test(const char *key)
{
  return array->test(key);
}


void lsDynEngine::setFractIndex(intIndex _fractIndex)
{
  if (_fractIndex < 4)
    _fractIndex = 4;
  if (_fractIndex > 8)
    _fractIndex = 8;
  fractIndex = _fractIndex;
}

intIndex lsDynEngine::wordScoreRare(intIndex count, intIndex corpus)
{

  if (count == intIndexInvalid)
    return 0;
  intIndex rt = (intIndex)(((double)count / corpus) * BASE_OCCURENCE);
  if (rt <= (intIndex)(RARE_WORD_SCORE * BASE_OCCURENCE))
    return 0;
  else
    return rt;
}

intIndex lsDynEngine::wordScoreMid(intIndex count, intIndex corpus)
{

  if (count == intIndexInvalid)
    return BASE_OCCURENCE + 1;

//  return (intIndex)(((double)count / corpus) * BASE_OCCURENCE);

  return (intIndex)( log2( 1 + count ) * log2( (double)BASE_OCCURENCE / corpus ) );
}

void lsDynEngine::scoresRange(listIndex &wordList)
{

  for (intIndex i = 0; i < wordList.length(); i++)
    wordList.setCount(i, wordScoreMid(wordList.count(i),
                                      ((wordMapDynamic *)wordMap)->getNCorpus(wordList.element(i))));

  wordList.sortCounts();
  wordList.cut( std::min( (fractIndex * wordList.length()) / 8, maxIndexSize ) );
  intIndex j;
  for (j = 0; j < wordList.length(); j++) {
    if (wordList.count(j) == BASE_OCCURENCE + 1)
      wordList.setCount(j, 3);
    else
      wordList.setCount(j, 2 - (3 * j) / wordList.length());
  }
  wordList.sortElements();
  wordList.collapse();
}

listIndex lsDynEngine::pageFlatList(lsWordStream &body, guint nWordInsert) {

  wordIndexDynamic *words = (wordIndexDynamic *)(this->words);

  listIndex wordList = listIndex(1024);

  guint nWordInsertTitle = 512;
  guint nWordInsertNone = 0;
  intIndex tagScore;
  while ((tagScore = body.nextWord())) {
    intIndex id = words->push(
      body.getWord(), 
      isClassifier() 
        ? nWordInsertNone 
        : ( ( tagScore == intIndexInvalid ) ? nWordInsertTitle : nWordInsert ) 
    );
    if (id != intIndexInvalid)
      wordList.push(id, tagScore);
  }
  return wordList;
}

listIndex lsDynEngine::pageQuery(lsWordStream &body) {

  listIndex wordList = pageFlatList(body, 0);
  scoresRange(wordList);
  return wordList;
}

void lsDynEngine::selfClassify( listIndex &wordList ) {
  

  if ( classifier ) {
    listIndex categories = classifier->classify( wordList );
    for ( intIndex i = 0 ; i < categories.length() ; i++ ) {
      STR_FORMAT( catWord, "cat:%u", categories.element(i) );
      std::cout << "Classifying with " << catWord << std::endl;
      guint nWordInsert = 1;
      intIndex catWordId = dynWords()->push( catWord, nWordInsert );
      if ( catWordId != intIndexInvalid ) {
        wordList.push( catWordId, 1 );
      }
    }
    wordList.sortElements();
    wordList.collapse();
  }
}

intIndex lsDynEngine::storeNewUrl( listIndex wordList, intIndex stamp ) {

  scoresRange(wordList);
  selfClassify( wordList );

  intIndex rt = ((articleIndexDynamic *)articles)->push( wordList, std::min(pageLimit, MAX_INDEX), stamp  );
  wordList.free();
  return rt;
}

void lsDynEngine::removeUrl(intIndex idUrl) {

  ((articleIndexDynamic *)articles)->pop(idUrl);
}

/** Gets the number of new words one can create while adding a new page, depending on the number of currently existing words.
*/
guint lsDynEngine::getnWordInsert() {

  guint nWordInsert = 100;
  if ( words->length() > 100 ) nWordInsert = 50;
  if ( words->length() > 200 ) nWordInsert = 20;
  if ( words->length() > 500 ) nWordInsert = 5;
  return nWordInsert;
}

intIndex lsDynEngine::addUrl( lsWordStream &body, intIndex stamp ) {

  listIndex wordList = pageFlatList( body, getnWordInsert() );
  return storeNewUrl( wordList, stamp );
}

void lsDynEngine::updateUrl( intIndex idUrl, lsWordStream &body, intIndex stamp  ) {

  listIndex wordList = pageFlatList( body, getnWordInsert() );
  scoresRange( wordList );
  selfClassify( wordList );

  ((articleIndexDynamic*)articles)->update( idUrl, wordList, stamp );
  wordList.free();
}

void lsDynEngine::updateUrl( intIndex idUrl, listIndex wordList, intIndex stamp  ) {

  scoresRange( wordList );
  selfClassify( wordList );

  ((articleIndexDynamic*)articles)->update( idUrl, wordList, stamp );
  wordList.free();
}

void lsDynEngine::fusionTitle( intIndex idUrl, listIndex wordList, intIndex stamp ) {

  scoresRange( wordList );

  ((articleIndexDynamic*)articles)->fusionTitle( idUrl, wordList, stamp );
  wordList.free();
}


void lsDynEngine::save() {
  { ReadLock _(&diskLock);
    articles->sync();
    words->sync();
    dynWordMap()->save();
  }
}

void lsDynEngine::convert() {
  { ReadLock _(&diskLock);
    dynArticles()->syncConvert();
    dynWords()->syncConvert();
    dynWordMap()->save();
  }
}


void lsDynEngine::notifyQuit() {
  dynWords()->notifyQuit();
  new WriteLock(&diskLock); // lock and drop the key
}


long lsDynEngine::getNArticles() {

  return ((articleIndexDynamic*)articles)->getNArticles();
}

long lsDynEngine::getNWords() {

  return ((articleIndexDynamic*)articles)->getNWords();
}

long lsDynEngine::getAsynch() {

  return ((wordIndexDynamic*)words)->getAsynch();
}

void lsDynEngine::debugLArticle( intIndex id ) {

  ((articleIndexDynamic*)articles)->debug(id,wordMap);
}

void lsDynEngine::debugLArticleTop( intIndex id ) {
  
    ((articleIndexDynamic*)articles)->debugTop(id,wordMap);
}

void lsDynEngine::debugLWord( intIndex id ) {

  ((wordIndexDynamic*)words)->debug(id);
}

void lsDynEngine::debugMapWord( intIndex id ) {

  ((wordMapDynamic*)wordMap)->debug(id);
}

void lsDynEngine::debugIdWord( char *word ) {

  ((wordMapDynamic*)wordMap)->debugReverse( word );
}

void lsDynEngine::debugExtension() {

  ((wordIndexDynamic*)words)->debugExtension();
}