#ifndef _DY_STORAGE_H_
#define _DY_STORAGE_H_

#include "storage.h"
#include "dylist.h"
#include "dyCompactList.h"
#include "buffer.h"

class lsDynamicStorage : public lsStorage<dyPointer> {

  typedef lsStorage<dyPointer> Tparent;

  dyList<dyCompactList> list;
  bool bGlobalInvalidate;

protected:
  virtual bool onCharge() const { return false; }

  virtual void                  doRelease( intIndex idx ) {}
  virtual void                  doRelease( listIndex l ) {}

public:
  lsDynamicStorage( const char *_magic );

  virtual ~lsDynamicStorage() {
    for ( intIndex i = 0 ; i < length() ; i++ ) list[i].free();
  }
  
  virtual void clear() {
    for ( intIndex i = 0 ; i < length() ; i++ ) list[i].free();
    list.reset();
    size = 0;
    bGlobalInvalidate = true;
  }
  
  virtual listIndex listNonEmptyIndexes(); // Get indexes for which we store a non empty list

  virtual void shrink( lsShrinker sShrinker, lsShrinker tShrinker );

  virtual void sync();

  /** Stores itself in a file, using compactListConvert type of compactList : one shot operation to store in a different format */
  virtual void syncConvert();

  virtual dyCompactList access( intIndex idx ) {
    return ( idx < length() )?list[idx]:dyCompactList();
  }

  virtual void  invalidate( intIndex idx ) {  bGlobalInvalidate = true; }

  virtual intIndex findFreeIndex( intIndex pageLimit ) {
    intIndex idx = 0;
    for ( ; ( idx < list.size )&&( list[idx].length() ); idx++ ) {}
    return ( idx < pageLimit )? idx : intIndexInvalid;
  }

  virtual void  replace( intIndex idx, dyCompactList lst ) {
    list.growUpto( idx, dyCompactList() );
    size = list.size;
    list[idx].free();
    list[idx] = lst;
    invalidate( idx );
  }

  virtual void                  access( listIndex l, dyCompactList *lst, bool bLazy ) {
    for ( guint i = 0 ; i < l.length() ; i++ )
      if ( l.element(i) < length() )
        lst[i] = list[l.element(i)]; else lst[i] = dyCompactList();
  }

          void                  load();
  virtual void                  attachToFile( const char * fileName );

  virtual void                  cleanup() {}
};

#endif
