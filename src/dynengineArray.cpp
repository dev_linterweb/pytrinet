#include "dynengineArray.h"
#include "dymap.h"

std::string lsDynEngineArray::test( const char * key ) {
    return std::string("Engine array ") + path + "/" + lang + ":" + key;
}

lsDynEngineArray::~lsDynEngineArray() {

    if ( wordMap ) delete wordMap;
}

void lsDynEngineArray::init( const char * _path, const char * _lang ) {

    if ( wordMap ) delete wordMap;
    lsConfig::init( _path, _lang );
    wordMapDynamic* wm = new wordMapDynamic();
    wm->load( buildPath("word.map").c_str() );
    wordMap = wm;
}


