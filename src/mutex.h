#ifndef _MUTEX_H_
#define _MUTEX_H_

#include <shared_mutex>
#include <mutex>

class ReadLock;
class WriteLock;


/*
  Best use : 
  { ReadLock( ptr_to_lockable );
  ... read protected operations
  }
  { WriteLock( ptr_to_lockable );
  ... write protected operations
  }

*/

class lsLockable {

public:

  std::shared_mutex mutex;

  /* only for legacy style lock */
  /*
  ReadLock * readLock = NULL;
  WriteLock * writeLock = NULL;

  void lockRead();
  void unlockRead();
  void lockWrite();
  void unlockWrite();
  */
  /* --- */
};


class ReadLock {

  std::shared_lock< std::shared_mutex > readLock;

  public:
    ReadLock( lsLockable *lockable ) : readLock(lockable->mutex) {
    }
};

class WriteLock {

  std::unique_lock< std::shared_mutex > writeLock;

  public:
    WriteLock( lsLockable *lockable ) : writeLock(lockable->mutex) {
    }
};

/* only for legacy style lock */
/*
inline void lsLockable::lockRead() {
  readLock = new ReadLock(this);
}

inline void lsLockable::unlockRead() {
  delete readLock;
}

inline void lsLockable::lockWrite() {
  writeLock = new WriteLock(this);
}

inline void lsLockable::unlockWrite() {
  delete writeLock;
}
*/
/* --- */

#endif 
