#include "wordStream.h"

#include <cstring>
#include <iostream>

intIndex lsWordStream::nextWord() {

    if ( eostream ) return 0;

    PyObject * rt = PyObject_CallObject( o, NULL );
    if (( !rt )||( rt == Py_None )) {
        eostream = true;
        return 0;
    }
    if (( ! PyTuple_Check( rt ) )||( PyTuple_Size( rt ) != 2 )){
        Py_DECREF(rt);
        PyErr_SetString(PyExc_TypeError, "word stream callback should return a tuple of size 2");
        eostream = true;
        return 0;
    }

    if ( !PyLong_Check( PyTuple_GetItem( rt, 1 ) ) ) {
        Py_DECREF(rt);
        PyErr_SetString(PyExc_TypeError, "word stream callback should return a tuple of size 2, the second argument should be an integer");
        eostream = true;
        return 0;
    }

    if ( !PyUnicode_Check( PyTuple_GetItem( rt, 0 ) ) ) {
        Py_DECREF(rt);
        PyErr_SetString(PyExc_TypeError, "word stream callback should return a tuple of size 2, the first argument should be a string");
        eostream = true;
        return 0;
    }

    Py_ssize_t wordSize = 0;
    const char * _word = PyUnicode_AsUTF8AndSize( PyTuple_GetItem( rt, 0 ), &wordSize );
    if ( wordSize > 255 ) {
        Py_DECREF(rt);
        PyErr_SetString(PyExc_TypeError, "word stream should not serve words bigger than 255 bytes long");
        eostream = true;
        return 0;
    }
    long score = PyLong_AsLong( PyTuple_GetItem( rt, 1 ) );
    if ( wordSize > 0 ) {
        std::strcpy( word, _word );
    } else {
        word[0] = 0;
    }
    Py_DECREF(rt); // PyTuple_GetItem( rt, 0 ) is a borrowed reference, and _word
    // depends on it. Do not decref rt before working with _word.
    return ( score < 0 ) ? intIndexInvalid : (intIndex)score;
}