#ifndef _BINARY_ITERATOR_H_
#define _BINARY_ITERATOR_H_

#include "lintersearch.h"

template <class Tpointer> class lsBinaryIterator {

public:
  lsBinaryIterator() { iLen = 0; bCurs = 0; }
  lsBinaryIterator( guchar *base, guint wsz, guint sz );

  bool remains() { return iLen>0; }
   ///< Indicateur de fin de liste. Si remains() == false, l'&eacute;criture et la lecture sont interdites.
  intIndex length() const { return iLen; }
   ///< Retourne le nombre d'&eacute;l&eacute;ment restant apr&egrave;s la position actuelle de l'it&eacute;rateur

protected:
  intIndex read( guint bCurs, guint nBits ); ///< Lit <nBits> bits à la position bCurs
  bool bitRead( guint bCurs ); ///< Lit 1 bit à la position bCurs
  void     write( guint bCurs, guint nBits, intIndex value );
    ///< Ecrire <value> de taille <nBits> à l'offset binaire <bCurs>. Modifie la suite !!!
  void     writeBit( guint bCurs, bool bit );
    ///< Ecrire le bit <bit> à la position <bCurs> ne modifie pas la suite
  void     next( guint nBits ); ///< Installe les variables de parcours <nBits> plus loin
  
  // Variables de parcours -------
  //  intIndex len, thislen, n; // Triplet itérateur de chaines d'atomes -> pointe word2
  intIndex word1, word2, iLen, bCurs;
    // double mot corrant 1 et 2, nombre d'éléments restants, offset dans le double mot
  Tpointer curs1, curs2;
    // Position de l'itérateur de chaine d'atome -> pointe word1, word2
};

template <class Tpointer>
inline lsBinaryIterator<Tpointer>::lsBinaryIterator( guchar *base, guint wsz, guint sz ) {
 
  iLen = sz; bCurs = 0;
  curs1.init( base, wsz );
  curs2 = curs1;
  if ( curs1.length() ) {
   word1 = *curs1;
   curs2.iterate();
  }
  if ( curs2.length() ) word2 = *curs2;
}

template <class Tpointer>
inline intIndex lsBinaryIterator<Tpointer>::read( guint bCurs, guint nBits ) {

  guint b1, b2, bCurs2;
  if ( bCurs + nBits <= 8*INDEX_CODE_SIZE ) { b1 = nBits ; b2 = 0; }
  else if ( bCurs <= 8*INDEX_CODE_SIZE ) {
   b1 = 8*INDEX_CODE_SIZE-bCurs; b2 = nBits - b1; bCurs2 = 0; }
  else { b1 = 0; b2 = nBits; bCurs2 = bCurs - 8*INDEX_CODE_SIZE; }
  intIndex rt = b1 ? ( word1 >> bCurs ) & (((intIndex)1<<b1)-1) : 0;
  if ( b2 ) rt |= (( word2 >> bCurs2 ) & (((intIndex)1<<b2)-1)) << b1;
  return rt; 
}

template <class Tpointer>
inline bool lsBinaryIterator<Tpointer>::bitRead( guint bCurs ) {

  return ( 8*INDEX_CODE_SIZE > bCurs ) ? ( word1 >> bCurs ) & 1
                                       : ( word2 >> (bCurs-8*INDEX_CODE_SIZE)) & 1;
}

template <class Tpointer>
inline void lsBinaryIterator<Tpointer>::writeBit( guint bCurs, bool bit ) {

 if ( bit ) {
  if ( bCurs < 8*INDEX_CODE_SIZE ) { 
    word1 |= 1<<bCurs;
    *curs1 = word1; 
  }
  else { 
    word2 |= 1<<(bCurs-8*INDEX_CODE_SIZE);
    *curs2 = word2; 
  }
 } else {
  if ( bCurs < 8*INDEX_CODE_SIZE ) { 
    word1 &= ~(1<<bCurs);
    *curs1 = word1; 
  }
  else { 
    word2 &= ~(1<<(bCurs-8*INDEX_CODE_SIZE)); 
    *curs2 = word2; 
  }
 }
}

template <class Tpointer>
inline void lsBinaryIterator<Tpointer>::write( guint bCurs, guint nBits, intIndex value ) {

  if ( bCurs + nBits <= 8*INDEX_CODE_SIZE ) {
   word1 = (word1 &((1<<bCurs)-1)) | ( value << bCurs );
   *curs1 = word1;
  } else if ( bCurs < 8*INDEX_CODE_SIZE ) {
   word1 = (word1 &((1<<bCurs)-1)) | ( value << bCurs );
   word2 = value >> (8*INDEX_CODE_SIZE-bCurs);
   *curs1 = word1;
   if ( curs2.length() ) *curs2 = word2;
  } else {
   bCurs -= 8*INDEX_CODE_SIZE;
   word2 = (word2 &((1<<bCurs)-1)) | ( value << bCurs );
   if ( curs2.length() ) *curs2 = word2;
  }
}

template <class Tpointer>
inline  void   lsBinaryIterator<Tpointer>::next( guint nBits ) {
  bCurs += nBits;
  iLen--;
  if ( bCurs >= 8*INDEX_CODE_SIZE ) {
   bCurs -= 8*INDEX_CODE_SIZE;
   word1 = word2;
   curs1 = curs2;
   if ( curs2.length() ) {
    curs2.iterate();
    if ( curs2.length() ) word2 = *curs2;
  }
 }
}

#endif
