#include "config.h"
#include "list.h"
#include "index.h"
#include "compactList.h"
#include <cstddef>

intIndex scoreMapNull[4] = {1,1,1,1};
intIndex scoreMapVocSpe[4] = {1,2,3,3};
intIndex scoreMapArticleVocSpe[4] = {8,12,12,12};
intIndex scoreMapQuery[4] = {1,2,3,7};

listIndex::listIndex() {

  list = NULL;
  size = maxLength = 0;
}

listIndex::listIndex( long _maxLength ) {

  if ( _maxLength > LIST_INDEX_MAX_SIZE ) {
#ifndef WIN32
   DEBUG_LOG << "Security overflow on listIndex : attempt for " << _maxLength << " B.";
#endif
   _maxLength = LIST_INDEX_MAX_SIZE;
  }
  size = 0;
  list = NULL;

  alloc( _maxLength );
}

listIndex::listIndex( const intIndex *s ) {

  if ( !s ) { maxLength = size = 0; list = NULL; return; }

  size = *(s++);
  list = NULL;
  alloc( size );

  for ( intIndex i = 0 ; i < size ; i++ ) {
    list[2*i] = *(s++); list[2*i+1] = 1;
  }
}

listIndex::~listIndex() {

}

void listIndex::alloc( long _maxLength ) {

  maxLength = _maxLength;

  if ( list ) delete [] list;

  if ( maxLength > 0 ) {
    long tmp = 2 * maxLength;
    list = new intIndex[tmp + 1];
    list[tmp] = 0;
  } else {
    list = NULL;
  }
}

void listIndex::grow( long _maxLength ) {

  if ( _maxLength > maxLength ) {
    intIndex * nlist = new intIndex[2*_maxLength+1];
    if ( size ) memcpy( nlist, list, size*2*sizeof(intIndex) );
    nlist[2*_maxLength]=0;
    if ( list ) delete [] list;
    list = nlist;
    maxLength = _maxLength;
  }
}

intIndex listIndex::getSortedLength() {

  return maxLength ? list[maxLength * 2] : 0;
}

void listIndex::setSortedLength(intIndex i) {

  if ( maxLength ) {
    list[maxLength * 2] = i;
  }
}

intIndex* listIndex::serialize() const {

  intIndex *rt = new intIndex[size+1];
  intIndex *data = rt;
  *(data++) = size;
  for ( intIndex i = 0 ; i < size ; i++ ) *(data++) = element(i);
  return rt;
}

void listIndex::free() {

  if ( list ) { delete [] list; list=NULL; maxLength = size = 0; }
}

void listIndex::append( listIndex l ) {

	intIndex toCopy = std::min( maxLength-size, (unsigned)l.length() );
	memcpy( list+(size*2), l.list, (toCopy*sizeof(intIndex)*2) );
	size += toCopy;
}

void listIndex::dup( listIndex l ) {

  size = l.size;

  alloc( size );

  memcpy( (char*)list, (const char*)l.list, 2*maxLength*sizeof(intIndex) );
}

void listIndex::cutZero() {

  intIndex i;
  for ( i = 0 ; (i < length())&&(count(i)) ; i++ ) ;;
  size = i;
}

void listIndex::appendWithAverageCut( intIndex element, intIndex &maxScore ) {

  if ( !maxLength ) return;
  if ( size < maxLength ) {
   if ( !size ) {
     list[0] = element;
     list[1] = 0;
     maxScore = 1;
     size = 1;
   } else {
    list[2*size] = element;
    list[2*size+1] = maxScore++;
    size++;
   }
  } else {
    double scoreStep = (3.0*maxScore) / (2.0*maxLength); // 2/3 cut
    double iStep = 0;
    intIndex dst = 0;
    for ( long i = 0 ; i < size ; i++ ) {
      if ( list[2*i+1] > iStep ) {
        list[2*dst] = list[2*i];
        list[2*dst+1] = list[2*i+1];
        dst++;
        iStep += scoreStep;
      }
    }
    size = dst;
    if ( size < maxLength ) {
      list[2*size] = element;
      list[2*size+1] = maxScore++;
      size++;
    }
  }
}

void listIndex::averageCut( intIndex sz ) {
// Keeps sz elements among the list, taken averagely at regular intervals

  intIndex step = length() / sz;
  if ( step < 2 ) cut( sz );
  else {
    for ( intIndex i = 0, j = 0 ; i < sz ; i++, j+=step ) {
      setElement( i, element( j ));
      setCount( i, count( j ));
    }
    size = sz;
  }
}

void listIndex::cutUnderScore( intIndex score ) {

  intIndex s,d;
  for ( s=0 ; ( s < length() )&&( count(s)>=score ) ; s++ ) ;;
  for ( d=s++ ; s < length() ; s++ ) 
    if ( count(s) >= score ) {
      setElement( d, element(s) );
      setCount( d, count(s) );
      d++;
    }
  size = d;
}

void listIndex::removeZero() {

  intIndex s,d;
  for ( s=0 ; s < length() && count(s) ; s++ ) ;;
  for ( d=s++ ; s < length() ; s++ ) 
    if ( count(s) ) {
      setElement( d, element(s) );
      setCount( d, count(s) );
      d++;
    }
  size = d;
}

static int listCountCompare( const intIndex* v1, const intIndex *v2 ) {
	
  return (*(v1+1)>*(v2+1))?-1:+1;
}

void listIndex::sortCounts() {
	
  qsort( list, size, 2*sizeof(intIndex),
    (int(*) (const void*, const void*))listCountCompare );
  setSortedLength( 0 );
}


static int listCountCompareInv( const intIndex* v1, const intIndex *v2 ) {
	
  return (*(v1+1)<*(v2+1))?-1:+1;
}

void listIndex::sortCountsInv() {
	
  qsort( list, size, 2*sizeof(intIndex),
    (int(*) (const void*, const void*))listCountCompareInv );
  setSortedLength( 0 );
}

static int listElementCompare( const intIndex* v1, const intIndex *v2 ) {
	
  return (*(v1)<*(v2))?-1:+1;
}

void listIndex::sortElements() {
	
  qsort( list, size, 2*sizeof(intIndex), 
    (int(*) (const void*, const void*))listElementCompare );

  setSortedLength( size );
}

void listIndex::push( intIndex elt, intIndex score ) {

  if ( elt == intIndexInvalid ) return;

  intIndex i;
  intIndex sortedLength;

  // On recherche dans la partie triee de la liste
  sortedLength = getSortedLength();
  if (sortedLength) {
    intIndex dmin = 0;
    intIndex dmax = sortedLength;

    i = (dmin + dmax) / 2;
    intIndex current = element(i);

    while((current != elt) && (dmin < dmax)) {
      if (elt > current) dmin = (i < dmax) ? (i + 1) : dmax;
      else dmax = (i > 0) ? (i - 1) : 0;

      i = (dmin + dmax) / 2;
      current = element(i);
    }
    if ( current == elt ) {
      if (( score == intIndexInvalid )||( list[2*i+1] == intIndexInvalid )) list[2*i+1] = intIndexInvalid; else list[2*i+1] += score;
      return;
    }
  }

  // Puis dans la partie pas triee
  for ( i = sortedLength ; (i < size)&&(element(i)!=elt) ; i++ ) ;;
  if ( i < size ) { if (( score == intIndexInvalid )||( list[2*i+1] == intIndexInvalid ))
                    list[2*i+1] = intIndexInvalid; else list[2*i+1] += score; }
  else {
    if ( size >= maxLength ) {
      grow(maxLength*2);
    }
    if ( size < maxLength ) {
      list[2*i] = elt;
      list[2*i+1] = score;
      size++;
    }
  }

  // On trie de temps en temps
  if ( size > sortedLength + 256 ) {
    sortElements();
  }
}

void listIndex::insertSortedCounts( intIndex elt, intIndex score ) {
// Inserts the new element (elt, score) in this count-sorted list
// Preserves the order and maxLength -- may remove one old element

  intIndex s,d;
  if (( size == maxLength )&& size &&( count(size-1) >= score )) return;
  for ( s = 0 ; (s < length()) && ( score < count(s) ) ; s++ ) ;;
  if ( s < maxLength ) {
   if ( size < maxLength ) size++;
   for ( d = length()-1 ; d > s ; d-- ) {
     setElement( d, element(d-1) );
     setCount( d, count(d-1) );
   }
   setElement( s, elt );
   setCount( s, score );
  }
}

void listIndex::insertSortedCountsInv( intIndex elt, intIndex score ) {

  intIndex s,d;
  for ( s = 0 ; (s < length()) && ( score >= count(s) ) ; s++ ) ;;
  if ( s < maxLength ) {
   if ( size < maxLength ) size++;
   for ( d = length()-1 ; d > s ; d-- ) {
     setElement( d, element(d-1) );
     setCount( d, count(d-1) );
   }
   setElement( s, elt );
   setCount( s, score );
  }
}

void listIndex::debug( const char *text ) const {

  std::cout << text << "[" << length() << "] = ";
  for ( intIndex i = 0 ; i < length() ; i++ ) {

    std::cout << "( " << element(i) << ", " << count(i) << " ), ";
  }
  std::cout << std::endl << std::endl;
}

void listIndex::intersect( listIndex l ) {

  intIndex s=0, sl=0, d=0;
  for ( ; ( s < length() )&&( sl < l.length() ) ; ) {
    if ( element(s) < l.element(sl) ) s++; else sl++;
    if ( element(s) == l.element(sl) ) {
     list[2*d] = list[2*s];
     list[2*d+1] = list[2*s+1];
     d++;
    }
  }
  size = d;
}

void listIndex::reunion( listIndex l1, listIndex l2 ) {

  alloc( l1.length() + l2.length() );

  intIndex i=0, j=0, k=0;
  while (( i < l1.length() )||( j < l2.length() )) {
    if (( i == l1.length() )||((j<l2.length())&&(l1.element(i) > l2.element(j)))) {
      list[2*k] = l2.list[2*j];
      list[2*k+1] = l2.list[2*j+1];
      k++; j++;	
    }
    else if (( j == l2.length() )||((i<l1.length())&&(l1.element(i) < l2.element(j)))) {
      list[2*k] = l1.list[2*i];
      list[2*k+1] = l1.list[2*i+1];
      k++; i++;	
    }
    else {
      list[2*k] = l1.list[2*i];
      list[2*k+1] = l1.list[2*i+1] + l2.list[2*j+1];
      k++; i++; j++;
    }
  }
  size = k;
}

void listIndex::reunionMax( listIndex l1, listIndex l2 ) {

  alloc( l1.length() + l2.length() );

  intIndex i=0, j=0, k=0;
  while (( i < l1.length() )||( j < l2.length() )) {

    if (( i == l1.length() )||((j<l2.length())&&(l1.element(i) > l2.element(j)))) {
  	
      list[2*k] = l2.list[2*j];
      list[2*k+1] = l2.list[2*j+1];
      k++; j++;	
    }
    else if (( j == l2.length() )||((i<l1.length())&&(l1.element(i) < l2.element(j)))) {
	
      list[2*k] = l1.list[2*i];
      list[2*k+1] = l1.list[2*i+1];
      k++; i++;	
    }
    else {
      list[2*k] = l1.list[2*i];
      list[2*k+1] = std::max( l1.list[2*i+1], l2.list[2*j+1] );
      k++; i++; j++;	
    }
  }
  size = k;
}

void listIndex::diff( listIndex l ) {

 intIndex dst = 0, i = 0;
 for ( intIndex src = 0 ; src < length() ; src++ ) {
  while (( i < l.length() )&&( l.element(i) < element(src) )) i++;
  if (( i >= l.length() )||( l.element(i) > element(src) )) {
   setElement(dst, element(src) );
   setCount(dst, count(src) );
   dst++;
  }
 }
 size = dst;
}

void listIndex::collapse() {

  if ( !length() ) return;
  intIndex i = 1;
  for ( intIndex k = 1 ; k < length() ; k++ ) {

    if ( element(i-1) == element(k) )  list[2*i-1] = std::max(list[2*i-1], list[2*k+1] );
    else {
      list[2*i] = list[2*k];
      list[2*i+1] = list[2*k+1];
      i++;
    }
  }
  size = i;
  setSortedLength(size);
}

bool  listIndex::operator == ( const listIndex &l ) const {

  if ( length() != l.length() ) return false;
  for ( intIndex i = 0 ; i < length() ; i++ ) 
    if ( element(i) != l.element(i) ) return false;
  return true;
}

intIndex listIndex::findDownScore( intIndex score ) {

  intIndex i;
  for ( i = 0 ; (i < length())&&(list[2*i+1]>=score) ; i++ ) ;;
  return i;
}

void listIndex::_switch( intIndex i1, intIndex i2 ) {

  intIndex elt1 = element(i1);
  intIndex count1 = count(i1);
  setElement( i1, element(i2) );
  setCount( i1, count(i2) );
  setElement( i2, elt1 );
  setCount( i2, count1 );
}

void listIndex::checkOverflow( intIndex sz ) {

  for ( long i = 0 ; i < length() ; i++ ) DEBUG_CHECK( element(i) < sz );
}


void listIndex::load( std::istream & in ) {

  if ( in ) {
    in.read((char*)&size,sizeof(intIndex));
  }
  if ( !in ) {
    size = 0; return;
  }
  if ( size > maxLength ) {
    alloc( size );
  }
  if ( in ) {
    in.read( (char*)list,  2*size*sizeof(intIndex) );
  }
  if ( !in ) {
    size = 0; return;
  }
}

void listIndex::save( std::ostream & out ) {

  out.write( (char*)&size, sizeof(intIndex) );
  out.write( (char*)list, 2*size*sizeof(intIndex) );
}

void sListIndex::fusion( listIndex l, intIndex id ) {

  for ( intIndex i = 0 ; ( i < l.length() )&&( appendable() ) ; i++ )
    listIndex::append( l.element(i), ( l.count(i)&0xffff )|( id << 16 ) );
  sortCounts();
  size = std::min( size, maxLength/2 );
}

static int slistCountCompare( const intIndex* v1, const intIndex *v2 ) {

  return ((*(v1+1)&0xffff)>(*(v2+1)&0xffff))?-1:+1;
}

void sListIndex::sortCounts() {

  qsort( list, size, 2*sizeof(intIndex),
         (int(*) (const void*, const void*))slistCountCompare );
}

void sListIndex::append( intIndex element, uint16_t score, uint16_t id ) {

  listIndex::append( element, score | (((intIndex)id)<<16) );
}
