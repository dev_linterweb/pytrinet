import io
from pytrinet.Configurable import Configurable
from nltk.stem.snowball import *


# WordStreamer transforms a text into a WordStream: i.e. a callable that deliver the list of
# stemmed words, with scores.
# A WordStream is a callable Callable[[],(str,int)] that delivers tuples (word,score), where score is
# a multiplier : it acts the same as delivering (word, 1), (word, 1), ... score times. 
# Score should be tuned by the WordStream to take into account the relative importance of different parts of the document to index.
# The WordStream should return NULL of NONE when the stream of words is done.

class WordStream :

    def __call__( self ) -> tuple :
        pass

    def toString( self ) -> str :
        words = []
        while True :
            word = self()
            if not word : break
            score = word[1]
            if score >= 0 : 
                for _ in range(word[1]) :
                    words.append( word[0] )
            else :
                words.append( word[0] ) # -1 : title score ; not applicable for queries ; just put once
        return " ".join(words)
            

class WordStreamMetaWords(WordStream):

    def __init__( self, wordStream: WordStream, metaWords: tuple ):
        self.wordStream = wordStream
        self.metaWords = metaWords
        self.curs = 0

    def __call__( self ) -> tuple:
        if self.curs < len(self.metaWords) :
            rt = (self.metaWords[self.curs],-1)
            self.curs += 1
            return rt
        return self.wordStream()


class WordStreamer(Configurable):

    def __init__( self, config ):
        super().__init__( config )

    def getWordStream( self, text: io.TextIOBase, isPhrase: bool = False, forceLang: str = None ) -> tuple:
        pass

    def getWordStreamWithMetawords( self, text: io.TextIOBase, isPhrase: bool = False, forceLang: str = None, metaWords: tuple = None ) -> tuple :
        ( base, lang ) = self.getWordStream( text, isPhrase, forceLang )
        return ( WordStreamMetaWords( base, metaWords ) if metaWords else base, lang )
        

