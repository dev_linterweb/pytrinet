from threading import Thread
from pytrinet.Project import Project
import configparser
import trinet
import os

from pytrinet.wordStreamer.BasicWordStream import BasicWordStreamer
from pytrinet.wordStreamer.DetectHtmlWikiwixStreamer import DetectHtmlWikiwixWordStreamer

class ProjectArray :

    streamerClasses = {'BasicWordStreamer':BasicWordStreamer,'DetectHtmlWikiwixWordStreamer':DetectHtmlWikiwixWordStreamer}

    def __init__( self, path: str, externStreamerClasses: dict = False, externExplorerClasses: dict = False, externMapClasses: dict = False ) :
        if externStreamerClasses :
            self.streamerClasses.update( externStreamerClasses )
        
        config = configparser.ConfigParser()
        config.read(path+'/engine.ini')

        self.engineArray = {}
        self.wordStreamer = {}
        self.projects = {}
        self.classifierInline = None
        self.classifierRevInline = None

        if 'WordStreamer' in config:
            if config['WordStreamer']['Class'] in self.streamerClasses :
                self.wordStreamer = self.streamerClasses[config['WordStreamer']['Class']]( config['WordStreamer'] )

        self.langs = config['Engine']['langs'].split(',') if 'Engine' in config else []

        self.name = config['Engine']['name'] if 'name' in config['Engine'] else ''

        self.port = int(config['Engine']['port']) if 'port' in config['Engine'] else 5050

        self.kbLogsFileSize = config['Engine']['kbLogsFileSize'] if 'kbLogsFileSize' in config['Engine'] else 24*1024

        for lang in self.langs :
            self.engineArray[lang] = trinet.DynamicEngineArray( path, lang )

        self.classifier = None
        for dirname in os.listdir(path) :
            if ( not dirname.startswith('.') ) and ( not dirname=='log' ) and ( not dirname in self.langs ) and os.path.isdir(path+'/'+dirname) :
                print("Loading project : "+dirname)
                self.projects[dirname] = Project(self,dirname,path+'/'+dirname,externExplorerClasses,externMapClasses)
                if dirname == 'classifier' : self.classifier = self.projects[dirname]

        if self.classifier :
            self.makeClassifierInline()
            print('ClassifierInline',self.classifierInline)
            for project in self.projects :
                if project != 'classifier' :
                    self.projects[project].useClassifier( self.classifier )

    def makeClassifierInline( self ) :
        map = self.classifier.map
        self.classifierInline = {}
        self.classifierRevInline = {}
        for element in map.iterator() :
            key = '{}/cat:{}'.format(element['lang'],element['id'])
            display = 'cat:{}'.format(element['url'])
            self.classifierInline[key] = display
            self.classifierRevInline['{}/cat:{}'.format(element['lang'],element['url'])] = 'cat:{}'.format(element['id'])

    def queryAll( self, query, lang, limit ) :
        rt = []
        for project in self.projects :
            if project != 'classifier' :
                ( l, results ) = self.projects[project].query( query, lang )
                if ( l == lang ) and ( len(results) > 0 ) :
                    results.offsetLimit( 0, limit )
                    rt.append( [ project, list( 
                        map (
                            lambda res : res.id,
                            results
                        )
                    ), results.maxScore ] )
        return rt

    def startAll( self ) :
        for project in self.projects :
            for explorer in self.projects[project].explorers :
                explorer.start()

    def stopAll( self ) :
        for project in self.projects :
            for explorer in self.projects[project].explorers :
                explorer.stop()

    def saveAll( self ) :
        for project in self.projects :
            self.projects[project].save()

    def convertAll( self ) :
        for project in self.projects :
            self.projects[project].convert()

    def quit( self ) :
        for project in self.projects :
            self.projects[project].quit()

    def saveConfigs( self ) :
        for project in self.projects :
            self.projects[project].saveConfig()

    def getProject( self, project ) :
        return self.projects[project] if project in self.projects else False

    def debug( self ):
        for project in self.projects :
            print("=== Project = ",project)
            self.projects[project].debug()
