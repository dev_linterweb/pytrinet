#ifndef _COMPACT_LIST_CONVERT_H_
#define _COMPACT_LIST_CONVERT_H_

#include <iostream>
#include <math.h>
#include <algorithm>
#include "list.h"
#include "config.h"
#include "binaryIterator.h"
#include "shrinker.h"

template <class Tpointer> class compactListConvert;

/** It&eacute;rateurs sur les listes de type compactListConvert. Le parcours d'une compactListConvert se fait de la mani&egrave;re suivante : 
- r&eacute;cup&eacute;rer un it&eacute;rateur au moyen de la m&eacute;thode compactListConvert::iterator,
- tant que l'it&eacute;rateur n'est pas vide (m&eacute;thode compactIteratorConvert::remains),
- r&eacute;cup&eacute;rer l'&eacute;l&eacute;ment courrant et son score au moyen de la m&eacute;thode compactIteratorConvert::getElement,
- faire avancer l'it&eacute;rateur au moyen de son op&eacute;rateur ++
\remarks{Il est plus efficace de r&eacute;cup&eacute;rer un &eacute;l&eacute;ment et son score en une seule op&eacute;ration. Toutefois il existe des m&eacute;thodes s&eacute;par&eacute;es pour l'un et l'autre.}
*/

template <class Tpointer> class compactIteratorConvert : public lsBinaryIterator<Tpointer> {

public:
  compactIteratorConvert() : lsBinaryIterator<Tpointer>() {}
   ///< Cr&eacute;er un it&eacute;rateur vide
  compactIteratorConvert( guchar *base, guint16 wsz, guint16 sz );
   ///< Cr&eacute;er un it&eacute;rateur &agrave; partir d'une chaine d'atomes \sa \ref chunk

  intIndex element();
   ///< R&eacute;cup&eacute;rer l'&eacute;l&eacute;ment courant seul
  intIndex score();
   ///< R&eacute;cup&eacute;rer le score de l'&eacute;l&eacute;ment courant
  void     getElement( intIndex &elt, intIndex &score );
   /**< R&eacute;cup&eacute;rer l'&eacute;l&eacute;ment courant et son score
     @param elt Variable o&ugrave; placer l'&eacute;l&eacute;ment
     @param score Variable o&ugrave; placer le score
   */
 
  void     setElement( intIndex elt, intIndex score );
   ///< Ecrire &agrave; la position courante de l'it&eacute;rateur l'&eacute;l&eacute;ment <elt> avec un score <score>.

  void     setElementNext( intIndex elt, intIndex score );
   ///< Ecrire à la position courante de l'itérateur et positionner sur l'élément suivant

  void     setScore( intIndex score );
   /**< Ecrit les 2 bits de score à la position courrante. Changer les scores sans toucher aux éléments
    ni à la longueur de la compactListConvert est la seule opération qui n'oblige pas à tout réallouer et
    peut être fait durant un parcours en lecture **/

  compactIteratorConvert<Tpointer>& operator ++();
   ///< Op&eacute;rateur d'incr&eacute;mentation pr&eacute;fixe : passer l'it&eacute;rateur &agrave; l'&eacute;l&eacute;ment suivant

   compactIteratorConvert<Tpointer>& dicoCutOver( intIndex idx );
   /**< Positionne l'it&eacute;rateur sur le prochain &eacute;l&eacute;ment plus grand ou &eacute;gal &agrave; <idx> s'il existe, sinon sur le dernier &eacute;l&eacute;ment de la liste */

protected:
  guint    getnBits(); ///< Déterminer le nombre de bits de l'élément courrant

  typedef  lsBinaryIterator<Tpointer> Tparent;
  using    Tparent::iLen;
  using    Tparent::bCurs;
  using    Tparent::next;
  using    Tparent::read;
  using    Tparent::bitRead;
  using    Tparent::write;
  using    Tparent::writeBit;

  intIndex lastElement; // Element à la position précédente de l'itérateur)
  intIndex currentElement; // Element courrant si écrit ou lu
  friend class compactListConvert<Tpointer>;
};

/** Liste compacte. \sa \ref listvect

Une compactListConvert est une liste d'entiers pond&eacute;r&eacute;s stock&eacute;e de mani&egrave;re optimale en m&eacute;moire.
L'acc&egrave;s au contenu d'une compactListConvert est r&eacute;alis&eacute; au moyen d'un it&eacute;rateur (compactIteratorConvert).
*/

template <class Tpointer>
class compactListConvert {

public:
  compactListConvert() { wsz = sz = 0; } ///< Cr&eacute;ation d'une listCompact de longueur 0
  compactListConvert( listIndex l ) {
     alloc(wSize(l), l.length());  } ///< Cr&eacute;ation d'une listCompact prete à contenir l

  intIndex length() { return sz; } ///< Longueur de cette compactListConvert (nombre d'indices)
  compactIteratorConvert<Tpointer> iterator() { return compactIteratorConvert<Tpointer>(base,wsz,sz); }
   ///< R&eacute;cup&egrave;re un it&eacute;rateur sur cette compactListConvert positionn&eacute; sur le premier &eacute;l&eacute;ment
  operator compactIteratorConvert<Tpointer>(void) { return compactIteratorConvert<Tpointer>(base,wsz,sz); }
   ///< Op&eacute;rateur de conversion en compactIteratorConvert. Identique &agrave; iterator()

  bool loadWithPrefix( std::istream & in, guint16 version );
   ///< Charger cette compactListConvert depuis le flux <in>. La longueur est d&eacute;termin&eacute;e par le flux.
  intIndex saveLength();
   ///< Nombre d'octets du flux de sauvegarde de cette compactListConvert
  intIndex memorySize();
   ///< Number of bytes allocated by this compactListConvert
  void     saveWithPrefix( std::ostream & out );
   ///< Sauvegarder cette compactListConvert dans le flux <out>. La longueur est stock&eacute;e dans le flux.
  void     free() { base.free(); wsz = sz = 0;  }
   ///< Lib&eacute;rer la m&eacute;moire utilis&eacute;e par cette compactListConvert.
  void     fromList( listIndex &l );
   /**< Remplir cette compactListConvert avec les donn&eacute;es contenues dans la liste listIndex. 
        La longueur de cette compactListConvert doit &ecirc;tre suffisante. */
  int     fromListCheckChanges( listIndex &l );
   /**< Idem, retourne le nombre d'&eacute;l&eacute;ments qui ont &eacute;t&eacute; 
        modifi&eacute;s par rapport aux anciennes donn&eacute;es. */
  void     toListCutUnderScore( listIndex &l, intIndex minScore );
   ///< Remplit la liste <l> avec les éléments de score supérieur à <minScore>
  compactListConvert dup();
   ///< Retourne un clone de cette compactListConvert
  void     dup( compactListConvert *l );
   /**< Remplit cette compactListConvert avec le contenu de <l>. La longueur de cette compactListConvert doit
        &ecirc;tre suffisante */
  void     debug();
   ///< Fonction de debuggage : sortie sur stderr
  void     loadOffset( std::istream & in );
   ///< Charge l'offset contenu dans le flux <in> en mode statique.
  void     addBaseToOffset( char *_base );
   ///< Ajoute le pointeur de base de l'index en mode statique une fois que l'offset a &eacute;t&eacute; d&eacute;fini.
  listIndex& getTitleIndexes( listIndex& l );
   ///< Retourne la liste des &eacute;l&eacute;ments de score 3 (les mots du titres)
  void     checkOverflow( intIndex sz );
   ///< Fonction de debuggage testant la coh&eacute;rence des num&eacute;ros contenus dans la liste
  int     nTitleContained( listIndex l );
   ///< Compte le nombre d'éléments de l présents dans la listCompact. l doit être triée
  void     allocFromListTitleFusion( compactListConvert<Tpointer> titleSource, listIndex source );
   /**< Génère la liste compacte contenant les éléments de source + les éléments de titre de 
        titleSource **/
  bool checkStructure( intIndex maxTarget );
  void     wordCount( intIndex nWords, intIndex* tWords );
  void     shrink( lsShrinker shrinker );
  int     compare( compactListConvert l );
   /**< The distance between this and l : ranges in 0..100
        100 = one included in the other, 0 = no correlation **/
   bool checkDuplicate( compactListConvert &l );
   ///< return true if there is no clue that this compactListConvert is from the same page as l

  void     markAsTitle( listIndex l );
   /**< Demande à ce que les éléments présents dans cette compactListConvert et dans l soient marqués comme
    mots du titre (score 3) **/
   
   void rdebug() {
    std::cout <<  sz << " " << wsz << std::endl;
   }

    static     guint16 wSize( listIndex l );

protected:
  void     alloc( guint16 _wsz, guint16 _sz ) {
      sz = _sz; wsz = _wsz;
      base.alloc( wsz ); }
   ///< Alloue la chaine d'atomes
   ///< Calcule le nombre de mots nécessaires au stockage de la liste l
  bool chunkLoad( std::istream & in );
 ///< Fonction it&eacute;rative pour charger chaque atome depuis un fichier
  void chunkSave( std::ostream & out ); ///< ... sauvegarder chaque ...
  void checkOverload( intIndex nId ); ///< Fonction de debuggage

  Tpointer    base;
  guint16     wsz,sz; // Nombre de mots alloués, nombre d'entrées
  friend class compactIteratorConvert<Tpointer>;
};





template <class Tpointer>
inline compactIteratorConvert<Tpointer>::compactIteratorConvert( guchar *base, guint16 wsz, guint16 sz )
: lsBinaryIterator<Tpointer>( base, wsz, sz ) {

  lastElement = intIndexInvalid;
}

template <class Tpointer>
inline guint  compactIteratorConvert<Tpointer>::getnBits() {

  if ( bitRead( bCurs+2 ) ) {
   if ( bitRead( bCurs+3 ) ) return BYTE_4_C;
   return BYTE_2_C;
  }
  if ( bitRead( bCurs+3 ) ) return BYTE_3_C;
  return BYTE_1_C;
}

#define EXPAND_ELEMENT_C( nBits, elt ) elt++; \
  if ( nBits >= BYTE_2_C ) { elt += 1<<BYTE_1_C; \
   if ( nBits >= BYTE_3_C ) { elt += 1<<BYTE_2_C; \
    if ( nBits == BYTE_4_C ) { elt += 1<<BYTE_3_C; }}}

template <class Tpointer>
inline void compactIteratorConvert<Tpointer>::getElement( intIndex &elt, intIndex &score ) {

  score = read( bCurs, 2 );
  guint nBits = getnBits();
  currentElement = read( bCurs + 4, nBits ) + lastElement;
  EXPAND_ELEMENT_C( nBits, currentElement );
  elt = currentElement;
}

template <class Tpointer>
inline intIndex compactIteratorConvert<Tpointer>::element() {

  guint nBits = getnBits();
  currentElement = read( bCurs + 4, nBits ) + lastElement;
  EXPAND_ELEMENT_C( nBits, currentElement );
  return currentElement;
}

template <class Tpointer>
inline intIndex compactIteratorConvert<Tpointer>::score() {

  return read( bCurs, 2 );
}

template <class Tpointer>
inline compactIteratorConvert<Tpointer>& compactIteratorConvert<Tpointer>::operator ++() { 

  guint nBits = getnBits();
  lastElement = currentElement;
  next( nBits + 4 );
  return *this;
}

template <class Tpointer>
inline void  compactIteratorConvert<Tpointer>::setElement( intIndex elt, intIndex score ) {

//  cout << "set " << elt << " , " << score << endl;
  currentElement = elt;
  guint16 nBits, codBits;
  elt -= lastElement+1;
  if ( elt >= (1<<BYTE_1_C) ) {
   elt -= 1<<BYTE_1_C;
   if ( elt >= (1<<BYTE_2_C) ) {
    elt -= 1<<BYTE_2_C;
    if ( elt >= (1<<BYTE_3_C) ) {
     elt -= 1<<BYTE_3_C;
     nBits = BYTE_4_C; codBits = 0xC;
    } else { nBits = BYTE_3_C; codBits = 8; }
   } else { nBits = BYTE_2_C; codBits = 4; }
  } else { nBits = BYTE_1_C; codBits = 0; }
  write( bCurs, nBits+4, ( elt<<4 )| codBits | score );
}

template <class Tpointer>
inline void  compactIteratorConvert<Tpointer>::setElementNext( intIndex elt, intIndex score ) {

  setElement( elt, score );
  ++*this;
}

template <class Tpointer>
inline void  compactIteratorConvert<Tpointer>::setScore( intIndex score ) {

  writeBit( bCurs, score & 1 );
  writeBit( bCurs+1, score & 2 );
}

template <class Tpointer>
inline compactIteratorConvert<Tpointer>& compactIteratorConvert<Tpointer>::dicoCutOver( intIndex idx ) {
  // jump to the first element greater than or equal to idx
  // ... or the last element if none

  while ( (iLen>1) && (element() < idx ) ) ++*this;
  return *this;
}


template <class Tpointer>
guint16 compactListConvert<Tpointer>::wSize( listIndex l ) {

 intIndex bsz = 0;
 intIndex lastElement = intIndexInvalid;
 for ( intIndex i = 0 ; i < l.length() ; i++ ) {
  intIndex diff = l.element(i) - lastElement - 1;
  lastElement = l.element(i);

  if ( diff < (1<<BYTE_1_C) ) bsz += BYTE_1_C + 4;
  else if ( diff < (1<<BYTE_2_C)+(1<<BYTE_1_C) ) bsz += BYTE_2_C + 4;
  else if ( diff < (1<<BYTE_3_C)+(1<<BYTE_2_C)+(1<<BYTE_1_C) ) bsz += BYTE_3_C + 4;
  else bsz += BYTE_4_C + 4;
 }
 if ( bsz % (INDEX_CODE_SIZE*8) ) return bsz/(INDEX_CODE_SIZE*8) + 1;
 else return bsz/(INDEX_CODE_SIZE*8);
}

/**
 * @brief Create a compact list with elements in source. All elements in source
 * that where title words (score 3) in titleSource, will be kept at score 3 in
 * the resulting compact list.
 * 
 * @param titleSource 
 * @param source 
 */
template <class Tpointer>
void  compactListConvert<Tpointer>::allocFromListTitleFusion( compactListConvert<Tpointer> titleSource, listIndex source ) {
 
 compactIteratorConvert<Tpointer> it = titleSource.iterator();
 intIndex elt = intIndexInvalid, score = 0;
 for ( int i = 0 ; i < source.length() ; i++ ) {
  intIndex selt = source.element(i);
  while ( it.remains() &&( it.getElement( elt, score ), elt < selt) ) ++it;
  if (( elt == selt )&&( score == 3 )) source.setCount( i, 3 );
 }
 alloc( wSize(source), source.length() );
 fromList( source );
}

template <class Tpointer>
void compactListConvert<Tpointer>::fromList( listIndex &l ) {

  compactIteratorConvert<Tpointer> it = iterator();
  for ( intIndex i = 0 ; i < l.length() ; i++ )
    it.setElementNext( l.element(i), l.count(i) );
}

template <class Tpointer>
int compactListConvert<Tpointer>::fromListCheckChanges( listIndex &l ) {

  compactIteratorConvert<Tpointer> it = iterator();
  int ct = 0;
  for ( intIndex i = 0 ; i < l.length() ; i++ ) {

    intIndex lc = it.element(); 
    intIndex ll = l.element(i);
    if ( lc <= ll ) ++it;
    if ( lc >= ll ) i++;
    if ( lc != ll ) ct++;
  }
  it = iterator();
  for ( intIndex i = 0 ; i < l.length() ; i++ )
    it.setElementNext( l.element(i), l.count(i) );
  return ct;
}

template <class Tpointer>
void compactListConvert<Tpointer>::loadOffset( std::istream & in ) { 
  // Cannot be instantiated with Tpointer = dyPointer
  
  guchar *p = NULL;
  if ( in ) {
    in.read( (char*)&p, sizeof(intIndex) );
  }
  base = Tpointer( p, 0 );
}

template <class Tpointer>
void compactListConvert<Tpointer>::addBaseToOffset( char *_base ) {
  // Cannot be instantiated with Tpointer = dyPointer

  base.addBaseToOffset( _base, wsz, sz );
}

template <class Tpointer>
bool compactListConvert<Tpointer>::chunkLoad( std::istream & in ) {

  Tpointer curs; curs.init( base, wsz );
  while ( curs.chunkLength() ) {
    if ( in ) {
      in.read( curs, curs.chunkLength()*INDEX_CODE_SIZE );
    }
    if ( !in ) return false;
    curs.nextChunk();
  }
  return true;
}

template <class Tpointer>
bool compactListConvert<Tpointer>::loadWithPrefix( std::istream & in, guint16 version ) {

   intIndex _wsz = 0, _sz = 0;
   sz = wsz = 0;
   if ( in ) {
    in.read( (char*)&_wsz, INDEX_CODE_SIZE );
    in.read( (char*)&_sz, INDEX_CODE_SIZE );
	 }
   if ( !in ) return false;
   wsz = _wsz; sz = _sz; // cast to 16 bits
   alloc( wsz, sz );
   return chunkLoad( in );
}

template <class Tpointer>
intIndex compactListConvert<Tpointer>::saveLength() {

  return (2+wsz)*INDEX_CODE_SIZE;
}

template <class Tpointer>
intIndex compactListConvert<Tpointer>::memorySize() {
 
  return wsz*INDEX_CODE_SIZE;
}

template <class Tpointer>
void compactListConvert<Tpointer>::chunkSave( std::ostream & out ) {

  Tpointer curs; curs.init( base, wsz );
  while ( curs.chunkLength() ) {
    out.write( curs, curs.chunkLength()*INDEX_CODE_SIZE );
    curs.nextChunk();
  }
}

template <class Tpointer>
void compactListConvert<Tpointer>::saveWithPrefix( std::ostream & out ) {

  intIndex _wsz = wsz; // Cast to 32 bits
  intIndex _sz = sz;
  out.write( (const char*)&_wsz, INDEX_CODE_SIZE );
  out.write( (const char*)&_sz, INDEX_CODE_SIZE );
  chunkSave( out );
}

template <class Tpointer>
bool compactListConvert<Tpointer>::checkStructure( intIndex maxTarget ) {

  compactIteratorConvert<Tpointer> it = iterator();
  for ( ; it.remains() ; ++it ) if ( it.element() >= maxTarget ) {
   intIndex e = it.element();
   return true;
  }
  return false;
}

template <class Tpointer>
void compactListConvert<Tpointer>::shrink( lsShrinker shrinker ) {

  listIndex newList( length() );
  compactIteratorConvert<Tpointer> it = iterator();
  for ( ; it.remains() ; ++it ) {
    intIndex e, score; it.getElement( e, score );
    intIndex new_e = shrinker.rewrite( e );
    if ( new_e != intIndexInvalid ) newList.append( new_e, score );
  }
  free();
  compactListConvert<Tpointer> l = compactListConvert<Tpointer>( newList );
  l.fromList( newList );
  newList.free();
  *this = l;
}

template <class Tpointer>
void compactListConvert<Tpointer>::wordCount( intIndex nWords, intIndex* tWords ) {

  compactIteratorConvert<Tpointer> it = iterator();
  for ( ; it.remains() ; ++it ) {
     intIndex id = it.element();
     if ( id < nWords ) tWords[id]++;
  }
}

template <class Tpointer>
void compactListConvert<Tpointer>::debug() {

 compactIteratorConvert<Tpointer> it = iterator();

 std::cout << it.length() << " : [ ";
 for ( ; it.remains() ; ++it ) 
   std::cout << "(" << it.element() << "," << it.score() << ") ";
 std::cout << " ]" << std::endl;
}

template <class Tpointer>
void compactListConvert<Tpointer>::dup( compactListConvert *l ) {

  Tpointer curs, lcurs;
  curs.init( base, wsz );
  lcurs.init( l->base, wsz );
  while ( curs.chunkLength() ) {
    memcpy( (guchar*)curs, (guchar*)lcurs, curs.chunkLength()*INDEX_CODE_SIZE );
    curs.nextChunk();
    lcurs.nextChunk();
  }
}

template <class Tpointer>
compactListConvert<Tpointer> compactListConvert<Tpointer>::dup() {

  compactListConvert<Tpointer> rt;
  rt.alloc( wsz, sz );
  rt.dup(this);
  return rt;
}

template <class Tpointer>
listIndex& compactListConvert<Tpointer>::getTitleIndexes( listIndex& l ) {

  l.clear();
  compactIteratorConvert<Tpointer> it = iterator();
  for ( ; ( it.remains() )&&( l.appendable() ) ; ++it )
    if ( it.score() == 3 ) l.append( it.element(), 1 );
  return l;
}

template <class Tpointer>
void  compactListConvert<Tpointer>::checkOverflow( intIndex sz ) {

  for ( compactIteratorConvert<Tpointer> it = iterator() ; it.remains() ; ++it ) DEBUG_CHECK( it.element() < sz );
}

template <class Tpointer>
int  compactListConvert<Tpointer>::nTitleContained( listIndex l ) {

 int n = 0, i = 0;
 compactIteratorConvert<Tpointer> it = iterator();
 for ( ; i < l.length() ; i++ ) {
  intIndex e = l.element(i);
  for ( ; it.remains() && ( it.element() < e ) ; ++it ) ;;
  if ( it.remains() ) {
   intIndex elt, score; it.getElement( elt, score );
   if (( elt == e )&&( score == 3 )) n++;
  }
 }
 return n;
}

template <class Tpointer>
void  compactListConvert<Tpointer>::toListCutUnderScore( listIndex &l, intIndex minScore ) {

  compactIteratorConvert<Tpointer> lit = iterator();
  for ( ; l.appendable() && lit.remains() ; ++lit ) {
    intIndex element, score; lit.getElement( element, score );
    if ( score >= minScore ) l.append( element, score );
  }
}

template <class Tpointer>
void  compactListConvert<Tpointer>::markAsTitle( listIndex l ) {

  compactIteratorConvert<Tpointer> it = iterator();
  intIndex i = 0;
  for ( ; it.remains() ; ++it ) {
   intIndex e, score; it.getElement( e, score );
   while (( i < l.length() )&&( l.element(i) < e )) i++;
   if (( i < l.length() )&&( l.element(i) == e )&&( score != 3 )) it.setScore(3);
  }
}

template <class Tpointer>
int  compactListConvert<Tpointer>::compare( compactListConvert l ) {

  intIndex MLen = max( length(), l.length() ); if ( !MLen ) return 0;
  compactIteratorConvert<Tpointer> ia = iterator();
  compactIteratorConvert<Tpointer> ib = l.iterator();
  int nIntersect = 0;
  intIndex ela, sa, elb, sb;
  if ( ia.remains() ) ia.getElement( ela, sa ); else ela =  intIndexInvalid;
  if ( ib.remains() ) ib.getElement( elb, sb ); else elb =  intIndexInvalid;
  while ( ( ela != intIndexInvalid )&&( elb != intIndexInvalid ) ) {
   if ( ela <= elb ) {
    if ( ela == elb ) { nIntersect+=3-abs((int)sa-(int)sb);
       ++ib; if ( ib.remains() ) ib.getElement( elb, sb ); else elb =  intIndexInvalid;
    }
    ++ia; if ( ia.remains() ) ia.getElement( ela, sa ); else ela =  intIndexInvalid;
   } else {
    ++ib; if ( ib.remains() ) ib.getElement( elb, sb ); else elb =  intIndexInvalid;
   }
  }
  return (nIntersect*100)/(3*MLen);
}

template <class Tpointer>
bool compactListConvert<Tpointer>::checkDuplicate( compactListConvert &l ) {

  compactListConvert<Tpointer> &lmin = ( l.length() > length() )?*this:l;
  compactListConvert<Tpointer> &lmax = ( l.length() > length() )?l:*this;
  if ( lmax.length() - lmin.length() > std::min( (intIndex)10, lmin.length() ) ) return true;
  
  compactIteratorConvert<Tpointer> mit = lmin.iterator();
  compactIteratorConvert<Tpointer> Mit = lmax.iterator();
  intIndex ctTitle = 0;
  int ctDiffAccept = lmin.length()/10;
  for ( ; mit.remains() ; ++mit ) {
    intIndex e,s; mit.getElement( e,s );
    intIndex le = intIndexInvalid, ls = 0;
    while ( Mit.remains() &&( Mit.getElement( le,ls ), le < e ) ) ++Mit;
    if ( s < 3 ) {
      if ( le != e ) ctDiffAccept -= s+1;
      else if ( ls < 3 ) ctDiffAccept -= abs( (int)ls-(int)s );
      if ( ctDiffAccept < 0 ) return true;
    } else ctTitle++;
  }
  return lmin.length() <= 2*ctTitle+2; // a bit more of no-title words we need to conclude
}

#endif
