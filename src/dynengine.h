#ifndef _TRINET_DYNAMIC_ENGINE_H_
#define _TRINET_DYNAMIC_ENGINE_H_

#include <Python.h>

#include <bits/stdc++.h>

#include "config.h"
#include "engine.h"
#include "dynengineArray.h"
#include "wordStream.h"
#include "list.h"
#include "index.h"
#include "dyCompactList.h"
#include "dynindex.h"

class lsDynEngine : public lsEngine<dyPointer> {

public:

  lsDynEngine() : lsEngine<dyPointer>() {
    bStatic = false;
    pageLimit = MAX_INDEX;
    classifier = NULL;
  }

  virtual ~lsDynEngine();

  void init( const char * _path, const char * _lang, lsDynEngineArray * array );

  std::string test( const char * key );

  /// @brief Sets the fraction of words that are dropped in inverted lists.
  /// 8 means keep all
  /// 4 means drop half
  /// @param _fractIndex intIndex between 4 and 8.
  void    setFractIndex( intIndex _fractIndex );

  lsDynEngineArray * array;

  lsEngine *         classifier;

  void     useClassifier( lsEngine * _classifier ) {
    classifier = _classifier;
  }
  void     selfClassify( listIndex &wordList );

  intIndex addUrl( lsWordStream &body, intIndex stamp = 0 );
  void     removeUrl( intIndex idUrl );
  void     updateUrl( intIndex idUrl, lsWordStream &body, intIndex stamp = 0  );
  void     updateUrl( intIndex idUrl, listIndex wordList, intIndex stamp = 0  );
  /** Keeps the same content in page idUrl, but adds words in wordList that are titles (score 3) in idUrl */
  void     fusionTitle( intIndex idUrl, listIndex wordList, intIndex stamp = 0 );
  
  intIndex storeNewUrl( listIndex wordList, intIndex stamp = 0 );

  /// @brief Takes title and body, as lists of words, and convert them into a weighted listIndex
  /// @param title 
  /// @param body 
  /// @param nWordInsert Maximal number of new words that will be inserted in word map
  /// @return 
  listIndex pageFlatList( lsWordStream &body, guint nWordInsert );
  guint   getnWordInsert();


  void     moveUrl( intIndex idUrl, const char *url, const char *title );
  void     regDelete( char* name, bool bRead );
  void     allReSynch() { ((wordIndexDynamic*)words)->allReSynch(); }
  listIndex pageQuery( lsWordStream &body );
  void     forceRedirect( char* url, char* title );
  void     save();
  void     convert();
  void     notifyQuit(); ///< Call before quitting to make the engine in a safe state for all threads being killed
 
  void shrink( lsShrinker aShrinker );
  bool flushExtension() { return ((wordIndexDynamic*)words)->flushExtension(); }
  void waitExtension() { ((wordIndexDynamic*)words)->waitExtension(); }
  void syncExtension() { ((wordIndexDynamic*)words)->syncExtension(); }


  void    setPageLimit( intIndex limit ) { pageLimit = limit; }
  intIndex checkDuplicate( intIndex id );

  articleIndexDynamic* dynArticles() { return (articleIndexDynamic*)articles; }
  wordIndexDynamic* dynWords() { return (wordIndexDynamic*)words; }
  wordMapDynamic* dynWordMap() { return (wordMapDynamic*)wordMap; }

  void lockAllRead();
  void lockAllWrite();
  void unlockAllRead();
  void unlockAllWrite();
  
  virtual long getNArticles();
  virtual long getNWords();
  virtual long getAsynch();

  void debugLArticle( intIndex id );
  void debugLArticleTop( intIndex id );
  void debugLWord( intIndex id );
  void debugMapWord( intIndex id );
  void debugIdWord( char * word );
  void debugExtension();

protected:
  void scoresRange( listIndex &wordList );
  
  static intIndex wordScoreRare( intIndex count, intIndex corpus );
  static intIndex wordScoreMid( intIndex count, intIndex corpus );
  
  long     fractIndex;
  long     maxIndexSize;
  time_t  lastAutoSave;
  lsLockable diskLock;
  intIndex    pageLimit;


};



#endif