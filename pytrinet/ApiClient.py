# Client for our own API ../api.py
# Used for instance by explorer/MastodonExplorer to forward external documents to another search engine that may
# be working on a remote API (or just here in the same process, but that's all the same)


import json
import requests


class ApiClient :

    def __init__( self, url ) :
        self.url = url.rstrip('/')

    def mkPath( self, project, action ) :
        return '{}/{}/{}'.format( self.url, project, action )

    def post( self, project, action, payload=None ) :
        path = self.mkPath( project, action )
        r = requests.post( path, json = payload ) if payload else requests.get( path )
        r.raise_for_status()
        return r.json()


"""         req = request.Request( path, method="POST" )
        req.add_header('Content-Type', 'application/json')
        r = request.urlopen(req, data=json.dumps(payload).encode('utf-8'))
 """


