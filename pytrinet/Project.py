from io import StringIO, TextIOBase
import os
import threading
import warnings
import trinet
import configparser
from pytrinet.DynamicEngine import SearchResults
from pytrinet.explorer.Explorer import Explorer
from pytrinet.explorer.MastodonExplorer import MastodonExplorer
from pytrinet.explorer.PassiveHttpExplorer import PassiveHttpExplorer
from pytrinet.explorer.WikimediaExplorer import WikimediaExplorer
from pytrinet.explorer.WikwixArchiveExplorer import WikwixArchiveExplorer
from pytrinet.map.MongoMap import MongoMap
from datetime import datetime

class Project :

    explorerClasses = {'Explorer':Explorer,'WikimediaExplorer':WikimediaExplorer,'MastodonExplorer':MastodonExplorer,'PassiveHttpExplorer':PassiveHttpExplorer}
    mapClasses = {'MongoMap':MongoMap,'WikwixArchiveExplorer':WikwixArchiveExplorer}
    
    def explorerSection( self, i ) :
        return 'Explorer'+( '' if not i else str(i) )

    def __init__( self, projectArray, name: str, path: str, externExplorerClasses: dict = False, externMapClasses: dict = False ):
        if externExplorerClasses :
            self.explorerClasses.update( externExplorerClasses )
        if externMapClasses :
            self.mapClasses.update( externMapClasses )

        self.name = name
        self.path = path
        self.projectArray = projectArray
        self.config = configparser.ConfigParser()
        config = self.config
        config.read(path+'/engine.ini')
        self.langs = config['Engine']['langs'].split(',') if 'Engine' in config else []
        self.explorers = list()
        self.explorerLock = threading.Lock() # To be locked when accessing ressources shared between different explorers


        i = 0
        while True :
            explorerSection = self.explorerSection(i)
            if explorerSection in config:

                if 'ExplorerCommon' in config :
                    for key in config['ExplorerCommon'] :
                        config[explorerSection][key] = config['ExplorerCommon'][key]

                if config[explorerSection]['Class'] in self.explorerClasses :
                    self.explorers.append( self.explorerClasses[config[explorerSection]['Class']]( self, config[explorerSection], i ) )
                else : raise Exception('Unknown explorer class {}'.format(config[explorerSection]['Class']))
            else : 
                if not i : raise Exception('Missing section Explorer in '+path+'/engine.ini')
                else : break
            i += 1

        if 'Map' in config:
            if config['Map']['Class'] == '__Explorer__' :
                self.map = self.explorers[0]
            elif config['Map']['Class'] in self.mapClasses :
                self.map = self.mapClasses[config['Map']['Class']]( config['Map'], name )
            else : raise Exception('Unknown map class {}'.format(config['Map']['Class']))
        else : raise Exception('Missing section Map in '+path+'/engine.ini')

        self.externalLogRotate = config['Engine']['logrotate'] == 'external' if 'Engine' in config and 'logrotate' in config['Engine'] else False
        self.metaWordListed = config['Engine']['metaWordListed'].split(',') if 'Engine' in config and 'metaWordListed' in config['Engine'] else []
        self.metaWordUnlisted = config['Engine']['metaWordUnlisted'].split(',') if 'Engine' in config and 'metaWordUnlisted' in config['Engine'] else []
        isClassifier = config['Engine']['isClassifier'].lower() == 'true' if 'Engine' in config and 'isClassifier' in config['Engine'] else False

        if not self.langs :
            raise UserWarning('No language found in '+path+'/engine.ini')

        self.engines = {}
        for lang in self.langs :
            print("Loading {}/{}".format(self.name,lang))
            self.engines[lang] = trinet._DynamicEngine( path, lang, self.projectArray.engineArray[lang], isClassifier )
        for lang in self.langs :
            self.engines[lang].joinLoad()
            print("Loaded {}/{}".format(self.name,lang))


    def useClassifier(self,project) :
        for lang in self.langs :
            if lang in project.langs :
                self.engines[lang].useClassifier(project.engines[lang])
        if not ( 'cat' in self.metaWordListed ) : self.metaWordListed.append('cat')

    def log(self,type,text) : 

        dir_path = '{}/log'.format( self.path )
        file_path = '{}/log/{}'.format( self.path, type )

        if not os.path.exists( dir_path ) :
            os.makedirs( dir_path )

        if not self.externalLogRotate :
            file_size = 0
            try:
                file_size = os.path.getsize( file_path )
            except OSError:
                pass # if file does not exist, consider size if 0
            if file_size > self.projectArray.kbLogsFileSize * 1024 :
                archive_path = '{}/log/{}.1'.format( self.path, type ) 
                os.replace( file_path, archive_path )

        with open( file_path, "a", encoding='utf-8') as handle:
            handle.write( datetime.now().strftime('%Y-%m-%d %H:%M:%S > ') )
            handle.write(text)
            handle.write('\n')

    def apiJSON(self) :
        rt = {
            'name':self.name,
            'exploring':self.explorers[0].started,
            'stopping':self.explorers[0].stopRequest,
            'explorer':self.explorers[0],
            'map':self.map,
            'langs':self.langs,
            'metaWordListed':self.metaWordListed,
            'metaWordUnlisted':self.metaWordUnlisted,
            'useTimestamps':self.explorers[0].useTimestamps
        }
        for i in range(1,len(self.explorers)) :
            rt['explorer'+str(i)] = self.explorers[i]

        return rt

    def getEngine(self,lang:str = None) :
        if len( self.langs ) == 1 : lang = self.langs[0]
        return self.engines[lang]

    def getsMap( self, lang, ids ) :
        results = self.map.gets( lang, ids )
        for id in results :
            results[id]['words'] = self.getMetaWords( id, lang )
        return [ results[id] if id in results else False for id in ids ]

    def getMap( self, lang, id ) :
        res = self.map.get( lang, id )
        res['words'] = self.getMetaWords( id, lang )
        return res
    
    def getMetaWords( self, id, lang ) :
        metawords = self.engines[lang].getMetaWords(int(id))
        if self.projectArray.classifierInline :
            metawords = [ self.convertCat(lang, word) for word in metawords ]
        return metawords

    # Takes a metaword word. If word starts with cat:, will convert the internal engine representation to human readable
    def convertCat( self, lang, word ) :
        if word.startswith('cat:') :
            convert = self.projectArray.classifierInline.get('{}/{}'.format(lang,word))
            return convert if convert else word
        return word

    def debug(self) :
        for lang in self.langs :
            print("==== Lang = ",lang)
            self.engines[lang].debug()

    def allCount(self,lang):
        return self.engines[lang].infos()[0]

    def saveConfig(self) :
        self.explorers[0].saveConfig(self.config['Explorer'])
        for i in range(1,len(self.explorers)) :
            self.explorers[i].saveConfig(self.config['Explorer'+str(i)])
        if self.explorers[0] != self.map :
            self.map.saveConfig(self.config['Map'])
        self.config.write(open(self.path+'/engine.ini','w'))

    def quit(self) :
        print('... stopping {}'.format(self.name))
        for explorer in self.explorers :
            explorer.stop()
        print('... clearing {}'.format(self.name))
        for engine in self.engines.values() :
            engine.notifyQuit()
        print('... {} ready to quit'.format(self.name))
        
    def save(self) :
        for engine in self.engines.values() :
            engine.save()
        self.saveConfig()

    def convert(self) :
        for lang in self.engines :
            print("Converting {} / {}".format(self.name,lang))
            self.engines[lang].convert()
        self.saveConfig()

    def syncExtension(self) :
        for engine in self.engines.values() :
            engine.syncExtension()

    def countDocumentsByMetawords( self, lang:str, prefix:str ) :
        if len( self.langs ) == 1 : lang = self.langs[0]
        if not lang in self.langs :
            return {}
        rt = self.engines[lang].countDocumentsByMetawords(prefix)

        if prefix == 'cat:' and self.projectArray.classifierInline :
            rtConv = {}
            for cat in rt :
                convert = self.projectArray.classifierInline.get('{}/{}'.format(lang,cat))
                if convert :
                    rtConv[convert] = rt[cat]
                else :
                    rtConv[cat] = rt[cat]
            rt = rtConv

        return rt
    
    # Takes metawords. Metawords starting with cat: will be converted : from explicit human readable classifier category to id classifier category
    # ex: cat:informatique may become cat:29
    def convertClassifierMetawords( self, lang:str, metaWords: tuple ) :
        return tuple( self.convertClassifierMetaword( lang, word ) for word in metaWords ) if metaWords else []
    
    def convertClassifierMetaword( self, lang:str, metaWord: str ) :
        key = '{}/{}'.format(lang,metaWord)
        return self.projectArray.classifierRevInline[key] if key in self.projectArray.classifierRevInline else metaWord

    def queryConnex( self, url: str, sortByStamp = False ) -> SearchResults :
        ( lang, id ) = self.map.getId( url )
        return ( lang, SearchResults( self.engines[lang].queryConnex( id, sortByStamp ), self.map, lang ) ) if lang else ( None, SearchResults( None, self.map, None ) )

    def query( self, phrase: str, lang:str = None, metaWords:tuple = None, sortByStamp = False ) -> SearchResults :
        self.log('query',"{} / lang={} / metaWords={}".format(phrase,lang,metaWords))
        if len( self.langs ) == 1 : lang = self.langs[0]
        metaWords = self.convertClassifierMetawords( lang, metaWords )
        self.log('query',"convMetaWords={}".format(metaWords))
        ( stream, _lang ) = self.projectArray.wordStreamer.getWordStreamWithMetawords( StringIO(phrase), isPhrase=True, forceLang=lang, metaWords=metaWords )
        if ( not _lang ) or ( not _lang in self.langs ) :
            return SearchResults( None, self.map, _lang ) 
        return ( lang, SearchResults( self.engines[_lang].query( stream.toString(), sortByStamp ), self.map, _lang ) )

    def _addDocument( self, body:TextIOBase, lang:str = None, metaWords:tuple = None, stamp = 0 ) -> tuple: # returns (lang,id)
        if len( self.langs ) == 1 : lang = self.langs[0]
        ( stream, _lang ) = self.projectArray.wordStreamer.getWordStreamWithMetawords( body, forceLang=lang, metaWords=metaWords )

        if ( not _lang ) or ( not _lang in self.langs ) :
            return (None,None)
        return ( _lang, self.engines[_lang].addUrl( stream, stamp ) )

    # Update a document identified by (id,lang). The id and lang will be kept unchanged.
    # That means that the caller needs to be sure the new document matches the language lang.
    def _updateDocument( self, id:int, title:TextIOBase, body:TextIOBase, lang:str = None, stamp = 0 ):
        return
        if len( self.langs ) == 1 : lang = self.langs[0]
        if ( not lang ) or ( not lang in self.langs ) :
            raise Exception('Language of the existing document must be specified in _updateDocument')
        ( stream, ) = self.projectArray.wordStreamer.getWordStream( body, forceLang=lang )
        self.engines[lang].updateUrl( id, stream )

    def _removeDocument( self, id:int, lang:str = None ):
        if len( self.langs ) == 1 : lang = self.langs[0]
        if ( not lang ) or ( not lang in self.langs ) :
            raise Exception('Language of the existing document must be specified in _removeDocument')
        self.engines[lang].removeUrl( id )

    # Only if you are sure url is not indexed yet. Use pushDocument if not sure
    def addDocument( self, url:str, body:TextIOBase, meta:dict = None, lang:str = None, metaWords:tuple = None, logger = None, stamp = 0 ) :
        (_lang,id) = self._addDocument( body, lang, metaWords=metaWords, stamp=stamp )
        if logger : logger.log("Add in {} : {} < ({}) {} ; {}".format(_lang, id, stamp, url, metaWords))
        print( "Add in {} : {} < ({}) {} ; {}".format(_lang, id, stamp, url, metaWords) )
        if id!=None and _lang : self.map.set( _lang, id, url, meta )

    # Push a document at url 'url'. 
    # lang may be specified if you want to force the language of the page.
    # If the document 'url' is already indexed under a given language and you force another language,
    # the document will actually be moved from an engin to another
    # @returns True if the operation has been an insertion. False if it was a replacement of an already existing document at this url.
    def pushDocument( self, url:str, body:TextIOBase, meta:dict = None, lang:str = None, metaWords:tuple = None, logger = None, stamp = 0, fusionTitle = False ) :
        (old_lang,id) = self.map.getId( url )
        if id == None :
            self.addDocument( url, body, meta, lang, metaWords, logger, stamp )
            return True
        else :
            ( stream, new_lang ) = self.projectArray.wordStreamer.getWordStreamWithMetawords( body, forceLang=lang, metaWords=metaWords )

            if new_lang == old_lang :
                if logger : logger.log("Update in {} : {} < ({}) {} ; {}".format(new_lang, id, stamp, url, metaWords))
                print( "Update in {} : {} < ({}) {} ; {}".format(new_lang, id, stamp, url, metaWords) )
                if not fusionTitle : 
                    self.engines[old_lang].updateUrl( id, stream, stamp )
                    self.map.set( old_lang, id, url, meta )
                else : 
                    self.engines[old_lang].fusionTitleUrl( id, stream, stamp )
            else :
                if not fusionTitle :
                    self.engines[old_lang].removeUrl( id )
                    if new_lang and new_lang in self.langs:
                        new_id = self.engines[new_lang].addUrl( stream, stamp )
                        if logger : logger.log("Move from {} to {} : {} < {}".format(old_lang, new_lang, new_id, url))
                        if new_id != None : self.map.set( new_lang, new_id, url, meta ) # This replaces old entry at url 'url'
                    else:
                        if logger : logger.log("Dropping url {}".format(url))
                        self.map.delete(url)
            return False

    def removeDocument( self, url:str ):
        (lang,id) = self.map.getId( url )
        if id :
            self._removeDocument( id, lang )
            self.map.delete( url )
        else:
            warnings.warn('Trying to remove a url that is not indexed')

    # removeOrphans : remove documents from the search engine that are not referred to by the url map.
    # If the search engine returns such an orphan document, it cannot be associated with any information except
    # it's numerical id, so it's pretty useless.
    def removeOrphans( self, lang=False ):
        if not lang :
            for _lang in self.langs :
                self.removeOrphans( _lang )
            return
        self.engines[lang].removeOrphans( lambda id, lang=lang : bool(self.map.getUrl( lang, id ) ) )
