import io

class TextIODiscardUntilPattern(io.TextIOBase):

    def __init__( self, text:io.TextIOBase, pattern:str ):
        self._text = text
        head = ''
        self._head = ''
        while True:
            tail = text.read(512)
            if not tail : break
            head = head + tail
            index = head.find(pattern)
            if index>=0 :
                self._head = head[index+len(pattern):]
                break
            head = head[len(head)-len(pattern)-1:]
        self._curs = 0

    def readable(self):
        return True

    def read(self, n=None):
        if self._curs >= len(self._head) :
            return self._text.read(n)

        if n == None :
            rt = self._head[ self._curs : ]
            self._curs = len(self._head)
            return rt
        
        head = self._head[ self._curs : self._curs+n ]
        self._curs += len(head)
        tail = self._text.read(n-len(head)) if n-len(head)>0 else ''
        return head+tail

    def readline(self): # unused, so not implemented. Head is lost if you use it.
        return self._text.readline()
