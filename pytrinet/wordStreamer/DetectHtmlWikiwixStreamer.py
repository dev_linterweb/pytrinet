
import io
from pytrinet.wordStreamer.BasicWordStream import FilterLength, FilterRegex, StemWordStream, TokenizerWordStream
from pytrinet.wordStreamer.LangDetectWordStream import LangDetectWordStream
from pytrinet.wordStreamer.TextIODelayed import TextIODelayed
from pytrinet.wordStreamer.WordStreamer import WordStream, WordStreamer


class DetectHtmlWikiwixWordStreamer(WordStreamer):

    parameters = (
        ('minLength',2),
        ('maxLength',24),
        ('langDetectLength',256),
        ('filterRegex','.*[0-9]{3,}.*|.*[0-9][^0-9]+[0-9].*'),
        ('htmlMethod','html')
    )
    # stemming should be in
    # ('arabic', 'danish', 'dutch', 'english', 'finnish', 'french', 'german', 'hungarian', 'italian', 'norwegian', 'porter', 'portuguese', 
    # 'romanian', 'russian', 'spanish', 'swedish')

    langToStemLang = {
        'ar':'arabic',
        'da':'danish',
        'nl':'dutch',
        'en':'english',
        'fi':'finnish',
        'fr':'french',
        'de':'german',
        'hu':'hungarian',
        'it':'italian',
        'no':'norwegian',
        'pt':'portuguese',
        'ro':'romanian',
        'ru':'russian',
        'es':'spanish',
        'sv':'swedish'
    }

    def __init__( self, config ):
        super().__init__( config )

    def getWordStream( self, text: io.TextIOBase, isPhrase: bool = False, forceLang: str = None ) -> tuple:

        if not isPhrase :
            stream = TextIODelayed( text )
            head = stream.getHead(64)
            if '/* WIKIWIX JARCHIVE' in head :
                stream = TokenizerWordStream( stream, 'wikiwix_archive' )
            elif '#PLAIN' in head :
                stream.dropHead(6)
                stream = TokenizerWordStream( stream, 'phrase' )
            else :
                stream = TokenizerWordStream( stream, self.htmlMethod )
        else :
            stream = TokenizerWordStream( text, 'phrase' )

        if self.minLength or self.maxLength :
            stream.addFilter( FilterLength( self.maxLength, self.minLength ) )
        if self.filterRegex :
            stream.addFilter( FilterRegex( self.filterRegex ) )
        if not forceLang :
            stream = LangDetectWordStream( stream, self.langDetectLength )
            lang = stream.lang
        else:
            lang = forceLang
        stemLang = self.langToStemLang.get(lang)
        if not stemLang : stemLang = 'porter'
        return ( StemWordStream( stream, stemLang ), lang )
