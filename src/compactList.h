#ifndef _COMPACT_LIST_H_
#define _COMPACT_LIST_H_

#include <iostream>
#include <math.h>
#include <algorithm>
#include "list.h"
#include "config.h"
#include "binaryIterator.h"
#include "shrinker.h"

/** extraIndex : Handy class to store an index number + extra information in a guint32.
 *  Indeed, due to the compactList compression, indexes are coded on less than BYTE_4+1 bits 
 *  (see lintersearch.h) - Extra value is currently 7-bit long.
 */
template <int idxBits> class TextraIndex {
  
  intIndex v;
  public:
  
    TextraIndex() { v = 0; }
    TextraIndex( intIndex idx ) { v = idx & (( 1<<idxBits )-1); }
    TextraIndex( intIndex idx, intIndex extra ) { v = idx | extra << idxBits; }
    
    intIndex value() const { return v &(( 1<<idxBits )-1); } // get index
    operator intIndex() const { return value(); }
    intIndex ex() const { return v>>idxBits; } // get extra value
    
    void setex( intIndex e ) { v |= ( e << idxBits ); } // set extra value
    void set( intIndex idx ) { v |= idx & (( 1<<idxBits )-1); } // set index
    
    bool operator < ( const TextraIndex &ei ) const { 
      return (( value() < ei.value() )||(( value() == ei.value() )&&( ex() < ei.ex() ))); }
    bool operator <= ( const TextraIndex &ei ) const { 
      return (( value() < ei.value() )||(( value() == ei.value() )&&( ex() <= ei.ex() ))); }
    bool operator > ( const TextraIndex &ei ) const { return !operator <= (ei); }
    bool operator >= ( const TextraIndex &ei ) const { return !operator < (ei); }
    bool operator == ( const TextraIndex &ei ) const {
      return ( value() == ei.value() )&&( ex() == ei.ex() ); }
};

typedef TextraIndex<BYTE_4+1> extraIndex;

/** It&eacute;rateurs sur les listes de type compactList. Le parcours d'une compactList se fait de la mani&egrave;re suivante : 
- r&eacute;cup&eacute;rer un it&eacute;rateur au moyen de la m&eacute;thode compactList::iterator,
- tant que l'it&eacute;rateur n'est pas vide (m&eacute;thode compactIterator::remains),
- r&eacute;cup&eacute;rer l'&eacute;l&eacute;ment courrant et son score au moyen de la m&eacute;thode compactIterator::getElement,
- faire avancer l'it&eacute;rateur au moyen de son op&eacute;rateur ++
\remarks{Il est plus efficace de r&eacute;cup&eacute;rer un &eacute;l&eacute;ment et son score en une seule op&eacute;ration. Toutefois il existe des m&eacute;thodes s&eacute;par&eacute;es pour l'un et l'autre.}
*/

template <class Tpointer> class compactIterator : public lsBinaryIterator<Tpointer> {

public:
  compactIterator() : lsBinaryIterator<Tpointer>() {}
   ///< Cr&eacute;er un it&eacute;rateur vide
  compactIterator( guchar *base, guint16 wsz, guint16 sz );
   ///< Cr&eacute;er un it&eacute;rateur &agrave; partir d'une chaine d'atomes \sa \ref chunk

  intIndex element();
   ///< R&eacute;cup&eacute;rer l'&eacute;l&eacute;ment courant seul
  intIndex score();
   ///< R&eacute;cup&eacute;rer le score de l'&eacute;l&eacute;ment courant
  void     getElement( intIndex &elt, intIndex &score );
   /**< R&eacute;cup&eacute;rer l'&eacute;l&eacute;ment courant et son score
     @param elt Variable o&ugrave; placer l'&eacute;l&eacute;ment
     @param score Variable o&ugrave; placer le score
   */
 
  void     setElement( intIndex elt, intIndex score );
   ///< Ecrire &agrave; la position courante de l'it&eacute;rateur l'&eacute;l&eacute;ment <elt> avec un score <score>.

  void     setElementNext( intIndex elt, intIndex score );
   ///< Ecrire à la position courante de l'itérateur et positionner sur l'élément suivant

  void     setScore( intIndex score );
   /**< Ecrit les 2 bits de score à la position courrante. Changer les scores sans toucher aux éléments
    ni à la longueur de la compactList est la seule opération qui n'oblige pas à tout réallouer et
    peut être fait durant un parcours en lecture **/

  compactIterator<Tpointer>& operator ++();
   ///< Op&eacute;rateur d'incr&eacute;mentation pr&eacute;fixe : passer l'it&eacute;rateur &agrave; l'&eacute;l&eacute;ment suivant

   compactIterator<Tpointer>& dicoCutOver( intIndex idx );
   /**< Positionne l'it&eacute;rateur sur le prochain &eacute;l&eacute;ment plus grand ou &eacute;gal &agrave; <idx> s'il existe, sinon sur le dernier &eacute;l&eacute;ment de la liste */

protected:
  guint    getnBits(); ///< Déterminer le nombre de bits de l'élément courrant

  typedef  lsBinaryIterator<Tpointer> Tparent;
  using    Tparent::iLen;
  using    Tparent::bCurs;
  using    Tparent::next;
  using    Tparent::read;
  using    Tparent::bitRead;
  using    Tparent::write;
  using    Tparent::writeBit;

  intIndex lastElement; // Element à la position précédente de l'itérateur)
  intIndex currentElement; // Element courrant si écrit ou lu
  friend class compactList<Tpointer>;
};

/** Liste compacte. \sa \ref listvect

Une compactList est une liste d'entiers pond&eacute;r&eacute;s stock&eacute;e de mani&egrave;re optimale en m&eacute;moire.
L'acc&egrave;s au contenu d'une compactList est r&eacute;alis&eacute; au moyen d'un it&eacute;rateur (compactIterator).
*/

template <class Tpointer>
class compactList {

public:
  compactList() { wsz = sz = 0; } ///< Cr&eacute;ation d'une listCompact de longueur 0
  compactList( listIndex l ) {
     alloc(wSize(l), l.length());  } ///< Cr&eacute;ation d'une listCompact prete à contenir l

  intIndex length() { return sz; } ///< Longueur de cette compactList (nombre d'indices)
  compactIterator<Tpointer> iterator() { return compactIterator<Tpointer>(base,wsz,sz); }
   ///< R&eacute;cup&egrave;re un it&eacute;rateur sur cette compactList positionn&eacute; sur le premier &eacute;l&eacute;ment
  operator compactIterator<Tpointer>(void) { return compactIterator<Tpointer>(base,wsz,sz); }
   ///< Op&eacute;rateur de conversion en compactIterator. Identique &agrave; iterator()

  bool loadWithPrefix( std::istream & in, guint16 version );
   ///< Charger cette compactList depuis le flux <in>. La longueur est d&eacute;termin&eacute;e par le flux.
  intIndex saveLength();
   ///< Nombre d'octets du flux de sauvegarde de cette compactList
  intIndex memorySize();
   ///< Number of bytes allocated by this compactList
  void     saveWithPrefix( std::ostream & out );
   ///< Sauvegarder cette compactList dans le flux <out>. La longueur est stock&eacute;e dans le flux.
  void     free() { base.free(); wsz = sz = 0;  }
   ///< Lib&eacute;rer la m&eacute;moire utilis&eacute;e par cette compactList.
  void     fromList( listIndex &l );
   /**< Remplir cette compactList avec les donn&eacute;es contenues dans la liste listIndex. 
        La longueur de cette compactList doit &ecirc;tre suffisante. */
  int     fromListCheckChanges( listIndex &l );
   /**< Idem, retourne le nombre d'&eacute;l&eacute;ments qui ont &eacute;t&eacute; 
        modifi&eacute;s par rapport aux anciennes donn&eacute;es. */
  void     toListCutUnderScore( listIndex &l, intIndex minScore );
   ///< Remplit la liste <l> avec les éléments de score supérieur à <minScore>
  void     toList( listIndex &l );
   ///< Remplit la listIndex <l> avec les éléments de cette compactList. La capacité de l est augmentée si nécessaire pour tout contenir.
  compactList dup();
   ///< Retourne un clone de cette compactList
  void     dup( compactList *l );
   /**< Remplit cette compactList avec le contenu de <l>. La longueur de cette compactList doit
        &ecirc;tre suffisante */
  void     debug();
   ///< Fonction de debuggage : sortie sur stderr
  void     loadOffset( std::istream & in );
   ///< Charge l'offset contenu dans le flux <in> en mode statique.
  void     addBaseToOffset( char *_base );
   ///< Ajoute le pointeur de base de l'index en mode statique une fois que l'offset a &eacute;t&eacute; d&eacute;fini.
  listIndex& getTitleIndexes( listIndex& l );
   ///< Retourne la liste des &eacute;l&eacute;ments de score 3 (les mots du titres)
  void     checkOverflow( intIndex sz );
   ///< Fonction de debuggage testant la coh&eacute;rence des num&eacute;ros contenus dans la liste
  int     nTitleContained( listIndex l );
   ///< Compte le nombre d'éléments de l présents dans la listCompact. l doit être triée
  void     allocFromListTitleFusion( compactList<Tpointer> titleSource, listIndex source );
   /**< Génère la liste compacte contenant les éléments de source + les éléments de titre de 
        titleSource **/

  void allocFromFusionTitle( compactList<Tpointer> titleSource, listIndex source );
  /**< Génère la list compacte contenant les anciens éléments de titleSoucre + les éléments titres de source */
  bool checkStructure( intIndex maxTarget );
  void     wordCount( intIndex nWords, intIndex* tWords );
  void     shrink( lsShrinker shrinker );
  int     compare( compactList l );
   /**< The distance between this and l : ranges in 0..100
        100 = one included in the other, 0 = no correlation **/
   bool checkDuplicate( compactList &l );
   ///< return true if there is no clue that this compactList is from the same page as l

  void     markAsTitle( listIndex l );
   /**< Demande à ce que les éléments présents dans cette compactList et dans l soient marqués comme
    mots du titre (score 3) **/
   
   void rdebug() {
    std::cout <<  sz << " " << wsz << std::endl;
   }

    static     guint16 wSize( listIndex l );

protected:
  void     alloc( guint16 _wsz, guint16 _sz ) {
      sz = _sz; wsz = _wsz;
      base.alloc( wsz ); }
   ///< Alloue la chaine d'atomes
   ///< Calcule le nombre de mots nécessaires au stockage de la liste l
  bool chunkLoad( std::istream & in );
 ///< Fonction it&eacute;rative pour charger chaque atome depuis un fichier
  void chunkSave( std::ostream & out ); ///< ... sauvegarder chaque ...
  void checkOverload( intIndex nId ); ///< Fonction de debuggage

  Tpointer    base;
  guint16     wsz,sz; // Nombre de mots alloués, nombre d'entrées
  friend class compactIterator<Tpointer>;
};

template <class Tpointer>
inline compactIterator<Tpointer>::compactIterator( guchar *base, guint16 wsz, guint16 sz )
: lsBinaryIterator<Tpointer>( base, wsz, sz ) {

  lastElement = intIndexInvalid;
}

template <class Tpointer>
inline guint  compactIterator<Tpointer>::getnBits() {

  if ( bitRead( bCurs+2 ) ) {
   if ( bitRead( bCurs+3 ) ) return BYTE_4;
   return BYTE_2;
  }
  if ( bitRead( bCurs+3 ) ) return BYTE_3;
  return BYTE_1;
}

#define EXPAND_ELEMENT( nBits, elt ) elt++; \
  if ( nBits >= BYTE_2 ) { elt += 1<<BYTE_1; \
   if ( nBits >= BYTE_3 ) { elt += 1<<BYTE_2; \
    if ( nBits == BYTE_4 ) { elt += 1<<BYTE_3; }}}

template <class Tpointer>
inline void compactIterator<Tpointer>::getElement( intIndex &elt, intIndex &score ) {

  score = read( bCurs, 2 );
  guint nBits = getnBits();
  currentElement = read( bCurs + 4, nBits ) + lastElement;
  EXPAND_ELEMENT( nBits, currentElement );
  elt = currentElement;
}

template <class Tpointer>
inline intIndex compactIterator<Tpointer>::element() {

  guint nBits = getnBits();
  currentElement = read( bCurs + 4, nBits ) + lastElement;
  EXPAND_ELEMENT( nBits, currentElement );
  return currentElement;
}

template <class Tpointer>
inline intIndex compactIterator<Tpointer>::score() {

  return read( bCurs, 2 );
}

template <class Tpointer>
inline compactIterator<Tpointer>& compactIterator<Tpointer>::operator ++() { 

  guint nBits = getnBits();
  lastElement = currentElement;
  next( nBits + 4 );
  return *this;
}

template <class Tpointer>
inline void  compactIterator<Tpointer>::setElement( intIndex elt, intIndex score ) {

//  cout << "set " << elt << " , " << score << endl;
  currentElement = elt;
  guint16 nBits, codBits;
  elt -= lastElement+1;
  if ( elt >= (1<<BYTE_1) ) {
   elt -= 1<<BYTE_1;
   if ( elt >= (1<<BYTE_2) ) {
    elt -= 1<<BYTE_2;
    if ( elt >= (1<<BYTE_3) ) {
     elt -= 1<<BYTE_3;
     nBits = BYTE_4; codBits = 0xC;
    } else { nBits = BYTE_3; codBits = 8; }
   } else { nBits = BYTE_2; codBits = 4; }
  } else { nBits = BYTE_1; codBits = 0; }
  write( bCurs, nBits+4, ( elt<<4 )| codBits | score );
}

template <class Tpointer>
inline void  compactIterator<Tpointer>::setElementNext( intIndex elt, intIndex score ) {

  setElement( elt, score );
  ++*this;
}

template <class Tpointer>
inline void  compactIterator<Tpointer>::setScore( intIndex score ) {

  writeBit( bCurs, score & 1 );
  writeBit( bCurs+1, score & 2 );
}

template <class Tpointer>
inline compactIterator<Tpointer>& compactIterator<Tpointer>::dicoCutOver( intIndex idx ) {
  // jump to the first element greater than or equal to idx
  // ... or the last element if none

  while ( (iLen>1) && (element() < idx ) ) ++*this;
  return *this;
}


template <class Tpointer>
guint16 compactList<Tpointer>::wSize( listIndex l ) {

 intIndex bsz = 0;
 intIndex lastElement = intIndexInvalid;
 for ( intIndex i = 0 ; i < l.length() ; i++ ) {
  intIndex diff = l.element(i) - lastElement - 1;
  lastElement = l.element(i);

  if ( diff < (1<<BYTE_1) ) bsz += BYTE_1 + 4;
  else if ( diff < (1<<BYTE_2)+(1<<BYTE_1) ) bsz += BYTE_2 + 4;
  else if ( diff < (1<<BYTE_3)+(1<<BYTE_2)+(1<<BYTE_1) ) bsz += BYTE_3 + 4;
  else bsz += BYTE_4 + 4;
 }
 if ( bsz % (INDEX_CODE_SIZE*8) ) return bsz/(INDEX_CODE_SIZE*8) + 1;
 else return bsz/(INDEX_CODE_SIZE*8);
}

/**
 * @brief Create a compact list with elements in source. All elements in source
 * that where title words (score 3) in titleSource, will be kept at score 3 in
 * the resulting compact list.
 * 
 * @param titleSource 
 * @param source 
 */
template <class Tpointer>
void  compactList<Tpointer>::allocFromListTitleFusion( compactList<Tpointer> titleSource, listIndex source ) {
 
 compactIterator<Tpointer> it = titleSource.iterator();
 intIndex elt = intIndexInvalid, score = 0;
 for ( int i = 0 ; i < source.length() ; i++ ) {
  intIndex selt = source.element(i);
  while ( it.remains() &&( it.getElement( elt, score ), elt < selt) ) ++it;
  if (( elt == selt )&&( score == 3 )) source.setCount( i, 3 );
 }
 alloc( wSize(source), source.length() );
 fromList( source );
}

/** Ajoute les éléments titres de source à titleSource
 */
template <class Tpointer>
void  compactList<Tpointer>::allocFromFusionTitle( compactList<Tpointer> titleSource, listIndex source ) {
 
 compactIterator<Tpointer> it = titleSource.iterator();
 intIndex elt = intIndexInvalid, score = 0;
 listIndex newSource( titleSource.length() + source.length() );
 int i = 0;
 for ( ; it.remains() ; ++it ) {
  it.getElement( elt, score );
  bool gotElt = false;
  while (( i < source.length() )&&( source.element(i) <= elt )) {
    if ( source.count(i) == 3 ) {
      newSource.append( source.element(i), source.count(i) );
      if ( source.element(i) == elt ) gotElt = true;
    }
    i++;
  }
  if ( !gotElt ) newSource.append( elt, score );
 }
 while ( i < source.length() ) {
  if ( source.count(i) == 3 ) {
    newSource.append( source.element(i), source.count(i) );
  }
  i++;
 }

 alloc( wSize(newSource), newSource.length() );
 fromList( newSource );
 newSource.free();
}

template <class Tpointer>
void compactList<Tpointer>::fromList( listIndex &l ) {

  compactIterator<Tpointer> it = iterator();
  for ( intIndex i = 0 ; i < l.length() ; i++ )
    it.setElementNext( l.element(i), l.count(i) );
}

template <class Tpointer>
int compactList<Tpointer>::fromListCheckChanges( listIndex &l ) {

  compactIterator<Tpointer> it = iterator();
  int ct = 0;
  for ( intIndex i = 0 ; i < l.length() ; i++ ) {

    intIndex lc = it.element(); 
    intIndex ll = l.element(i);
    if ( lc <= ll ) ++it;
    if ( lc >= ll ) i++;
    if ( lc != ll ) ct++;
  }
  it = iterator();
  for ( intIndex i = 0 ; i < l.length() ; i++ )
    it.setElementNext( l.element(i), l.count(i) );
  return ct;
}

template <class Tpointer>
void compactList<Tpointer>::loadOffset( std::istream & in ) { 
  // Cannot be instantiated with Tpointer = dyPointer
  
  guchar *p = NULL;
  if ( in ) {
    in.read( (char*)&p, sizeof(intIndex) );
  }
  base = Tpointer( p, 0 );
}

template <class Tpointer>
void compactList<Tpointer>::addBaseToOffset( char *_base ) {
  // Cannot be instantiated with Tpointer = dyPointer

  base.addBaseToOffset( _base, wsz, sz );
}

template <class Tpointer>
bool compactList<Tpointer>::chunkLoad( std::istream & in ) {

  Tpointer curs; curs.init( base, wsz );
  while ( curs.chunkLength() ) {
    if ( in ) {
      in.read( curs, curs.chunkLength()*INDEX_CODE_SIZE );
    }
    if ( !in ) return false;
    curs.nextChunk();
  }
  return true;
}

template <class Tpointer>
bool compactList<Tpointer>::loadWithPrefix( std::istream & in, guint16 version ) {

   intIndex _wsz = 0, _sz = 0;
   sz = wsz = 0;
   if ( in ) {
    in.read( (char*)&_wsz, INDEX_CODE_SIZE );
    in.read( (char*)&_sz, INDEX_CODE_SIZE );
	 }
   if ( !in ) return false;
   wsz = _wsz; sz = _sz; // cast to 16 bits
   alloc( wsz, sz );
   return chunkLoad( in );
}

template <class Tpointer>
intIndex compactList<Tpointer>::saveLength() {

  return (2+wsz)*INDEX_CODE_SIZE;
}

template <class Tpointer>
intIndex compactList<Tpointer>::memorySize() {
 
  return wsz*INDEX_CODE_SIZE;
}

template <class Tpointer>
void compactList<Tpointer>::chunkSave( std::ostream & out ) {

  Tpointer curs; curs.init( base, wsz );
  while ( curs.chunkLength() ) {
    out.write( curs, curs.chunkLength()*INDEX_CODE_SIZE );
    curs.nextChunk();
  }
}

template <class Tpointer>
void compactList<Tpointer>::saveWithPrefix( std::ostream & out ) {

  intIndex _wsz = wsz; // Cast to 32 bits
  intIndex _sz = sz;
  out.write( (const char*)&_wsz, INDEX_CODE_SIZE );
  out.write( (const char*)&_sz, INDEX_CODE_SIZE );
  chunkSave( out );
}

template <class Tpointer>
bool compactList<Tpointer>::checkStructure( intIndex maxTarget ) {

  compactIterator<Tpointer> it = iterator();
  for ( ; it.remains() ; ++it ) if ( it.element() >= maxTarget ) {
   intIndex e = it.element();
   return true;
  }
  return false;
}

template <class Tpointer>
void compactList<Tpointer>::shrink( lsShrinker shrinker ) {

  listIndex newList( length() );
  compactIterator<Tpointer> it = iterator();
  for ( ; it.remains() ; ++it ) {
    intIndex e, score; it.getElement( e, score );
    intIndex new_e = shrinker.rewrite( e );
    if ( new_e != intIndexInvalid ) newList.append( new_e, score );
  }
  free();
  compactList<Tpointer> l = compactList<Tpointer>( newList );
  l.fromList( newList );
  newList.free();
  *this = l;
}

template <class Tpointer>
void compactList<Tpointer>::wordCount( intIndex nWords, intIndex* tWords ) {

  compactIterator<Tpointer> it = iterator();
  for ( ; it.remains() ; ++it ) {
     intIndex id = it.element();
     if ( id < nWords ) tWords[id]++;
  }
}

template <class Tpointer>
void compactList<Tpointer>::debug() {

 compactIterator<Tpointer> it = iterator();

 std::cout << it.length() << " : [ ";
 for ( ; it.remains() ; ++it ) 
   std::cout << "(" << it.element() << "," << it.score() << ") ";
 std::cout << " ]" << std::endl;
}

template <class Tpointer>
void compactList<Tpointer>::dup( compactList *l ) {

  Tpointer curs, lcurs;
  curs.init( base, wsz );
  lcurs.init( l->base, wsz );
  while ( curs.chunkLength() ) {
    memcpy( (guchar*)curs, (guchar*)lcurs, curs.chunkLength()*INDEX_CODE_SIZE );
    curs.nextChunk();
    lcurs.nextChunk();
  }
}

template <class Tpointer>
compactList<Tpointer> compactList<Tpointer>::dup() {

  compactList<Tpointer> rt;
  rt.alloc( wsz, sz );
  rt.dup(this);
  return rt;
}

template <class Tpointer>
listIndex& compactList<Tpointer>::getTitleIndexes( listIndex& l ) {

  l.clear();
  compactIterator<Tpointer> it = iterator();
  for ( ; ( it.remains() )&&( l.appendable() ) ; ++it )
    if ( it.score() == 3 ) l.append( it.element(), 1 );
  return l;
}

template <class Tpointer>
void  compactList<Tpointer>::checkOverflow( intIndex sz ) {

  for ( compactIterator<Tpointer> it = iterator() ; it.remains() ; ++it ) DEBUG_CHECK( it.element() < sz );
}

template <class Tpointer>
int  compactList<Tpointer>::nTitleContained( listIndex l ) {

 int n = 0, i = 0;
 compactIterator<Tpointer> it = iterator();
 for ( ; i < l.length() ; i++ ) {
  intIndex e = l.element(i);
  for ( ; it.remains() && ( it.element() < e ) ; ++it ) ;;
  if ( it.remains() ) {
   intIndex elt, score; it.getElement( elt, score );
   if (( elt == e )&&( score == 3 )) n++;
  }
 }
 return n;
}

template <class Tpointer>
void  compactList<Tpointer>::toList( listIndex &l ) {

  l.clear();
  l.grow( 2*sz );
  compactIterator<Tpointer> lit = iterator();
  for ( ; l.appendable() && lit.remains() ; ++lit ) {
    intIndex element, score; lit.getElement( element, score );
    l.append( element, score );
  }
}

template <class Tpointer>
void  compactList<Tpointer>::toListCutUnderScore( listIndex &l, intIndex minScore ) {

  compactIterator<Tpointer> lit = iterator();
  for ( ; l.appendable() && lit.remains() ; ++lit ) {
    intIndex element, score; lit.getElement( element, score );
    if ( score >= minScore ) l.append( element, score );
  }
}

template <class Tpointer>
void  compactList<Tpointer>::markAsTitle( listIndex l ) {

  compactIterator<Tpointer> it = iterator();
  intIndex i = 0;
  for ( ; it.remains() ; ++it ) {
   intIndex e, score; it.getElement( e, score );
   while (( i < l.length() )&&( l.element(i) < e )) i++;
   if (( i < l.length() )&&( l.element(i) == e )&&( score != 3 )) it.setScore(3);
  }
}

template <class Tpointer>
int  compactList<Tpointer>::compare( compactList l ) {

  intIndex MLen = max( length(), l.length() ); if ( !MLen ) return 0;
  compactIterator<Tpointer> ia = iterator();
  compactIterator<Tpointer> ib = l.iterator();
  int nIntersect = 0;
  intIndex ela, sa, elb, sb;
  if ( ia.remains() ) ia.getElement( ela, sa ); else ela =  intIndexInvalid;
  if ( ib.remains() ) ib.getElement( elb, sb ); else elb =  intIndexInvalid;
  while ( ( ela != intIndexInvalid )&&( elb != intIndexInvalid ) ) {
   if ( ela <= elb ) {
    if ( ela == elb ) { nIntersect+=3-abs((int)sa-(int)sb);
       ++ib; if ( ib.remains() ) ib.getElement( elb, sb ); else elb =  intIndexInvalid;
    }
    ++ia; if ( ia.remains() ) ia.getElement( ela, sa ); else ela =  intIndexInvalid;
   } else {
    ++ib; if ( ib.remains() ) ib.getElement( elb, sb ); else elb =  intIndexInvalid;
   }
  }
  return (nIntersect*100)/(3*MLen);
}

template <class Tpointer>
bool compactList<Tpointer>::checkDuplicate( compactList &l ) {

  compactList<Tpointer> &lmin = ( l.length() > length() )?*this:l;
  compactList<Tpointer> &lmax = ( l.length() > length() )?l:*this;
  if ( lmax.length() - lmin.length() > std::min( (intIndex)10, lmin.length() ) ) return true;
  
  compactIterator<Tpointer> mit = lmin.iterator();
  compactIterator<Tpointer> Mit = lmax.iterator();
  intIndex ctTitle = 0;
  int ctDiffAccept = lmin.length()/10;
  for ( ; mit.remains() ; ++mit ) {
    intIndex e,s; mit.getElement( e,s );
    intIndex le = intIndexInvalid, ls = 0;
    while ( Mit.remains() &&( Mit.getElement( le,ls ), le < e ) ) ++Mit;
    if ( s < 3 ) {
      if ( le != e ) ctDiffAccept -= s+1;
      else if ( ls < 3 ) ctDiffAccept -= abs( (int)ls-(int)s );
      if ( ctDiffAccept < 0 ) return true;
    } else ctTitle++;
  }
  return lmin.length() <= 2*ctTitle+2; // a bit more of no-title words we need to conclude
}

#endif
