/*
PyTrinet - The Trinet search engine in a python module
Copyright (C) 2007-2022 Linterweb (France)
Licensed under the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
*/
#ifndef _LINTERSEARCH_H_
#define _LINTERSEARCH_H_

#include <string.h>
#include <stdint.h>
#include <algorithm>

/*
#define BYTE_1 7
#define BYTE_2 9
#define BYTE_3 13
#define BYTE_4 24
*/

#define BYTE_1_C 7
#define BYTE_2_C 10
#define BYTE_3_C 15
#define BYTE_4_C 27

#define BYTE_1 7
#define BYTE_2 10
#define BYTE_3 15
#define BYTE_4 27


#define MAX_INDEX ((intIndex)((1<<BYTE_4)+(1<<BYTE_3)+(1<<BYTE_2)+(1<<BYTE_1)-1))

#define CACHE_BUF_SIZE 600000
#define DY_CHUNK_SIZE (1<<21)

#define DEADTIME_SOCKET 60

#define WORD_INDEX_MAGIC "WILI"
#define ARTICLE_INDEX_MAGIC "AILI"

/* define _DEBUG_ to have console reports of what happens wrong */
#define _DEBUG_

#define DEG_DIV 3600

/* byte size of indexes in files */
#define INDEX_CODE_SIZE 4
#define LIST_INDEX_MAX_SIZE 200000
#define ARTICLE_MAP_ENTRY_MAX_SIZE 1024
#define WORD_MAP_HACK_MAX_SIZE 1024*1024*512
 // 512 Mo

/* file structure version */
#define VERSION_CURRENT 3
#define VERSION_CURRENT_MAP 1

/* control dynamic reallocation of dynamic lists */
#define DYLIST_MIN_ALLOC 500
#define DYLIST_RATIO_ALLOC 1.3

/* Maximal number of words returned by lsEngine::countDocumentsByMetawords */
#define MAX_META_WORD_COUNT 1024

/* Number of records in articleExtension */
#define ARTICLE_EXTENSION_SIZE 400

/* Over RARE_WORD_SCORE, a word of project mayhem, has a name */
#define RARE_WORD_SCORE 0.4

/* Number of entries in hack table of words */
#define WORD_HACK_SIZE 8209 

/* Bonus points to a word for being in the title */
#define INTITLE_BONUS 2

/* Maximal ratio of words indexed for one article */
#define ARTICLE_MAX_WORD_VOCAB_RATIO 2

/* Maximal number of distinct words in an article. Used in precalculation */
#define ARTICLE_MAX_WORD 10000

/* Maximal number of articles indexed for one word */
#define WORD_MAX_ARTICLE 100000

/* A title word is duplicated TITLE_FACTOR times */
#define TITLE_FACTOR 100

/* Words appearing in more then MAX_WORD_ARTICLE_OCCURENCE of the corpus are not indexed */
#define MAX_WORD_ARTICLE_OCCURENCE 0.8

/* Size of specific vocabulary calculated during a query */
#define SPECIFIC_VOCAB_SIZE 25

/* Nombre d'occurences maximal. Utilisé pour coder le ratio du nombre d'occurences d'un mot sur une page. Ce nombre doit borner à un facteur petit près (2 par exemple) le nombre total d'occurences d'un mot dans le corpus. */
#define BASE_OCCURENCE (1<<28)

#define WORD_INDEX_FILENAME "word.index"
#define WORD_MAP_FILENAME "word.map"
#define ARTICLE_INDEX_FILENAME "article.index"
#define ARTICLE_MAP_FILENAME "article.map"

typedef uint32_t intIndex;
typedef unsigned char guchar;
typedef uint32_t guint;
typedef uint16_t guint16;
typedef uint64_t gulong;
typedef uint64_t guint64;
typedef uint64_t guint48;
typedef size_t gsize;

#define intIndexInvalid 0xFFFFFFFF
#define guint16invalid 0xFFFF

// In-stack string fomatting macro ; guarranted malloc free
#define STR_FORMAT(name,format,...) \
    int size_##name = std::snprintf( nullptr, 0, format, ##__VA_ARGS__ ); \
    char name[size_##name+1]; \
    std::snprintf( name, size_##name+1, format, ##__VA_ARGS__ );

inline void g_strlcpy( char* dest, const char* src, size_t bufsize ) {
  size_t n = std::min( strlen(src), bufsize-1 );
  memcpy( dest, src, n ); dest[n] = 0;
}  

#define DEBUG_LOG lsConfig::defaultConfig->log("debug")

#if defined(_DEBUG_)
#define DEBUG_CHECK(cond) if (!(cond)) { DEBUG_LOG << "Assertion " << #cond << " failed at " << \
 __LINE__ << ":" << __FILE__; } 
#else
#define DEBUG_CHECK(cond) ;;
#endif

#define DEBUG_BREAK asm( "int $3" );

#define binaryOut(val) { intIndex v = val; \
  for ( long i = 0 ; i < 32 ; i++ ) { cout << ( v&1 ? '1' : '0' ); v >>= 1; }}
#define DEBUG_MARK cerr << __FILE__ << " : " << __LINE__ << endl;

#endif 

/** 
\mainpage Page d'index du projet Lintersearch

\section presentation Idee de depart

Le moteur de recherche est bas&eacute; sur une double indexation des pages : 
- l'indexation directe : qui permet partant d'une page de r&eacute;cup&eacute;rer la liste des mots formant cette page, ( classes articleIndexDynamic et articleIndexStatic )
- l'indexation invers&eacute;e : qui permet partant d'un mot de r&eacute;cup&eacute;rer le liste des pages contenant de mot ( classes wordIndexDynamic et wordIndexStatic )

\section conceptslist Developpement des concepts generaux
 \ref listvect

 \ref idmap

 \ref algorecherche

 \ref threads

\page listvect Les listes d'identifiants

 Ce que nous appellerons une <liste> est en fait une liste d'entiers pond&eacute;r&eacute;s par des scores. \sa \ref scores

 Les entiers peuvent soit
correspondre a des num&eacute;ros de mots (idMots), ou a des num&eacute;ros d'urls (idUrls). 
\sa \ref idmap

 Les listes form&eacute;es de numeros de mots servent a recuperer, pour une url donnee, la liste des mots qui 
lui sont associ&eacute;s. C'est ce que nous appelons une liste directe : partant d'une page, on d&eacute;crit son contenu. Ces listes sont stock&eacute;es par la classe articleIndexDynamic.
 Les listes form&eacute;es  de num&eacute;ros d'urls servent a r&eacute;cup&eacute;rer, pour un mot donn&eacute;, la liste des pages qui le contiennent. C'est ce que nous appelons une liste invers&eacute;e : elles sont de fait obtenues en inversant
les listes directes. Ces listes sont stock&eacute;es par la classe wordIndexDynamic.

 Il existe deux types de listes : 
 - compactList : de taille minimale en m&eacute;moire mais difficilement manipulable et tr&egrave;s statiques, une compactList peut en fait etre &eacute;clat&eacute;e &agrave; differents emplacements de la m&eacute;moire syst&egrave;me a la maniere d'un fichier sur un disque dur. \sa \ref chunks
 - listIndex : ce sont les "copies de travail" des listCompact, sur lesquelles de nombreuses
   op&eacute;rations sont disponibles.

 On peut acc&eacute;der instantanement &agrave; n'importe quel element d'une liste de type listIndex au moyen des 
m&eacute;thodes listIndex::element et listIndex::count. Par contre, le parcours d'une liste de type compactList
n&eacute;cessite l'utilisation d'un iterateur : compactIterator. La methode compactList::iterator permet
de r&eacute;cuperer un iterateur positionn&eacute; sur le premier &eacute;lement de la liste.

 \page scores Systeme de scores

 Dans une liste de mots (liste directe) ainsi que dans une liste d'articles (liste invers&eacute;e), chaque mot ou article est pond&eacute;r&eacute; par un score qui indique l'importance du mot dans la page. 
\sa \ref listvect

Dans une compactList, il existe seulement 4 codes de scores :

0 : indique que le mot est de faible importance (3eme sixière de la page)

1 : indique que le mot est de moyenne importance (2eme sixième de la page)

2 : indique que le mot est d'importance &eacute;lev&eacute;e (1er sixième de la page)

3 : indique que le mot est un mot du titre.

Dans une listIndex, le score d'un &eacute;l&eacute;ment est un num&eacute;ro proportionnel &agrave; son importance. Lors de la conversion d'une compactList en une listIndex, on utilise une scoreMap pour convertir chacun des 4 codes de scores en un score num&eacute;rique. Une scoreMap est un pointeur sur 4 entiers, correspondant respectivement aux scores associ&eacute;s &agrave; 0,1,2,3. Il existe 4 scoreMaps

scoreMapNull : conserver les codes de scores initiaux.

scoreMapArticleVocSpe : d&eacute;finit les scores des articles d&eacute;duits du vocabulaire sp&eacute;cifique.

scoreMapQuery : d&eacute;finit les scores des articles d&eacute;duits directement des mots de la requ&egrave;te.

scoreMapVocSpe : d&eacute;finit les scores de mots lors du calcul du vocabulaire sp&eacute;cifique

\sa \ref algorecherche

\page chunks Atomes memoire

Les listes directes et invers&eacute;es sont stock&eacute;es en m&eacute;moire de mani&egrave;re optimis&eacute;e. 
\sa \ref listvect

Cette page ne concerne que les personnes souhaitant comprendre le m&eacute;canisme d'optimisation et le fonctionnement interne de la classe compactList.

L'allocation m&eacute;moire d'une compactList est r&eacute;alis&eacute;e de mani&egrave;re non standard par la classe statique dyCompactList. Sa m&eacute;thode alloc prend en param&egrave;tre la taille de la compactList &agrave; allouer et retourne une chaine d'atomes m&eacute;moires r&eacute;parties dans des chunks g&eacute;r&eacute;s manuellement.

Les atomes sont de taille normalisee. Ils peuvent etre soit :
 - de taille inferieure a 2^hiMin, l'atome est alors dans la zone d'allocation inferieure
(delimitee par lowMax).
 - de taille 2^k avec k compris entre hiMin et hiMax, l'atome est alors dans la zone d'allocation 
superieure.

Le codage et le fonctionnement d'une chaine d'atomes est particuli&egrave;rement bas niveau. Il est en g&eacute;n&eacute;ral inutile de s'interesser en profondeur &agrave; leur fonctionnement. Il faut juste retenir ceci du point de vue de l'utilisateur :

Une fois allou&eacute;e, une chaine d'atome est un pointeur de type char*. Il faut de plus m&eacute;moriser la taille de la chaine (c'est &agrave; dire le param&egrave;tre fourni &agrave; dyCompactList::alloc).

Parcourir le contenu d'une chaine d'atomes se fait au moyen d'un it&eacute;rateur. Un iterateur sur une chaine d'atomes est un quadruplet ( ptr, sz, thisLen, n ) o&ugrave; :
 - ptr est un pointeur (char*) sur l'element en cours,
 - sz, thisLen et n sont utilisees en interne pour l'iteration.


\page algorecherche Algorithme de recherche
 
La recherche en elle-m&ecirc;me est r&eacute;alis&eacute;e par la classe lsEngine et sa m&eacute;thode lsEngine::query.

- La phrase recherch&eacute;e est convertie en liste d'idMots (\c wQuery) via la m&eacute;thode wordMap::getIndexes.
\code  listIndex wQuery = wordMap->getIndexes( phrase ); \endcode
- Chaque mot \c w est pond&eacute;r&eacute; par son nombre d'occurence \c occ(w) dans le corpus. On calcule le nombre d'occurence moyen avScore et le nombre d'occurence total allScore sur les mots de la requ&egrave;te. Les mots tels que \c occ(w) > 2*\c avScore sont &eacute;limin&eacute;e.
\code   for ( guint i = 0 ; i < wQuery.length() ; i++ ) {
    intIndex nCorpus = wordMap->getNCorpus( wQuery.element(i) );
    if ( nCorpus > 1<<(8*sizeof(intIndex)-3) ) {
       nCorpus = 0; // prevent overloads on allScore
       zeroCounts++;
    }
    allScore += nCorpus;
    wQuery.setCount( i, nCorpus );
  }
  unlockWordMap();
  intIndex avScore = allScore ? allScore / (wQuery.length()-zeroCounts) : 0;
    // wQuery.length()-zeroCounts is non-zero if allScore is non-zero
  for ( guint i = 0 ; i < wQuery.length() ; i++ ) {
    intIndex thisScore = wQuery.count( i );
    if ( thisScore > 2*avScore ) {
      allScore -= thisScore;
      wQuery.setCount( i, 0 );
    }
  }
  wQuery.removeZero();\endcode
- Chaque mot w est pond&eacute;r&eacute; par score(w) = allScore / occ(w).
\code   for ( guint i = 0 ; i < wQuery.length() ; i++ ) {
    wQuery.setCount( i, allScore / wQuery.count(i) ); // Remove 0's before doing this
  } \endcode
- Pour calculer le corpus de vocabulaire sp&eacute;cifique, on r&eacute;alise l'"intersection faible" des listes d'articles associ&eacute;s aux mots pond&eacute;r&eacute;s retenus. vocSpePageMax articles sont retenus.
\code     pages = unionList( wQuery, words ).weakIntersect( scoreMapQuery, wQuery, 0, allScore, 50 );
    pages.averageCut(vocSpePageMax); \endcode
- Pour calculer le vocabulaire sp&eacute;cifique, on r&eacute;alise l'union des listes de mots associ&eacute;s aux articles du corpus de vocabulaire sp&eacute;cifique. Cette union est pond&eacute;r&eacute; par la map de scores \c scoreMapVocSpe. Les mots
\sa \ref scores
\code   listIndex vocSpe = unionList( pages, articles ).reunion( scoreMapVocSpe, listMax ); \endcode

- On calcule la liste de r&eacute;sultats en r&eacute;alisant l'union ou l'intersection des listes de pages associ&eacute;es aux mots de la requ&egrave;te, pond&eacute;r&eacute;e par la map de scores \c scoreMapQuery
\code   if ( bIntersect ) {
    articlesQuery = unionList( wQuery, words ).intersect( scoreMapQuery );
    if ( articlesQuery.length() < 6 ) 
      articlesQuery = unionList( wQuery, words ).reunion( scoreMapQuery, listMax );
  } else
    articlesQuery = unionList( wQuery, words ).reunion( scoreMapQuery, listMax ); \endcode
- La liste de retour est calcul&eacute;e en r&eacute;alisant une union pond&eacute;r&eacute;e des listes de pages associ&eacute;es aux mots du vocabulaire sp&eacute;cifique, pond&eacute;r&eacute;e par la map de scores \c scoreMapArticleVocSpe, cette union &eacute;tant restreinte aux articles de la liste de r&eacute;sultats.
\code   listIndex articlesVocSpe = unionList( vocSpe, words )
      .reunionIntersectWithScoreAdd( scoreMapArticleVocSpe, articlesQuery ); \endcode
- Pour chaque article de la liste de retour de score suffisant, si la liste des mots de la requ&egrave;te initiale est une sous-chaine de la liste des mots du titre, on rajoute &agrave; l'article un score de max(400-((800*(rt-rq))/rt), 0), rt d&eacute;signant le nombre de mots du titre, rq d&eacute;signant le nombre de mots de la requ&egrave;te.
\code   for ( intIndex i = 0 ; i < nArticleToTest ; i++ ) {
    intIndex artId = articlesVocSpe.element(i);
    if (( articleMap->getUrl( artId ) )&&
       ( (titleCods = articleMap->getTitleCods( artId ))
                     .contains( wFullQuery )) ) {
      long rt = titleCods.length();
      long rq = wFullQuery.length();
      articlesVocSpe.setCount(i, articlesVocSpe.count(i) + max(400-((800*(rt-rq))/rt), 0) );
      bExactMatch = true;
    }
  } \endcode

\page idmap Les maps : association texte-identifiants
Le moteur de recherche manipule des liste de mots (listes directes) et des listes de pages (listes invers&eacute;es).
\sa \ref listvect

Ces listes ne contiennent pas de texte mais des num&eacute;ros (idMots et idUrls) codant pour des mots ou des urls. Il nous faut pouvoir convertir ces num&eacute;ros en mots ou en urls lisibles, et inversement il nous faut, partant d'un mot sous forme de chaine de caract&egrave;res, pouvoir d&eacute;terminer son num&eacute;ro (idMot) associ&eacute;.

Cette t&acirc;che est r&eacute;alis&eacute;e par les classes mapWord et mapArticle et leurs descendants. 

\page threads Shema de threads

\section sthreads Threads

\subsection tmajeures Threads majeures et leurs attributions
- Thread m&egrave;re : &eacute;coute le r&eacute;seau et g&eacute;n&egrave;re les threads principales.
- Threads principales : chaque thread principale est associ&eacute;e &agrave; un client via une socket. Elles ex&eacute;cutent les diff&eacute;rentes requ&egrave;tes : recherche, op&eacute;rations directes sur le moteur.
- Thread d'exploration : cette thread est rattach&eacute;e &agrave; l'objet lsExlorer. Elle r&eacute;alise le crawling et l'insertion des nouvelles pages dans le moteur.
- Thread de synchronisation : cette thread est rattach&eacute;e &agrave; l'objet wordIndexDynamic. Elle r&eacute;alise la synchronisation de l'index de mots wordIndexDynamic avec l'index d'articles articleIndexDynamic.

\subsection tmineures Threads mineures
- Thread d'atlas : interne &agrave; mapArticle. Calcule l'atlas.

\section smutex Mutex

Les classes prot&eacute;g&eacute;es par mutex fournissent une m&eacute;thode lock() et unlock(). Tout acc&egrave;s en &eacute;criture &agrave; un objet doit &ecirc;tre prot&eacute;g&eacute; dans un bloc objet->lock(); ... objet->unlock();
L'acc&egrave;s en lecture doit en g&eacute;n&eacute;ral &ecirc;tre lui aussi prot&eacute;g&eacute;, toutefois, certaines classes sont associ&eacute;es &agrave; une thread ma&icirc;tre. La thread ma&icirc;tre est la seule autoris&eacute;e &agrave; r&eacute;aliser des op&eacute;rations d'&eacute;criture sur l'objet. Une thread ma&icirc;tre peut donc lire un objet sans protection. Elle doit par contre prot&eacute;ger tout m&eacute;canisme d'&eacute;criture pour &eacute;viter un conflit avec une op&eacute;ration de lecture concurrente.

\subsection sassocmaitre Liste des classes protegee par thread maitre
- thread de synchronisation : wordIndexDynamic::syncMutex, wordIndexDynamic
- thread d'exploration : articleIndexDynamic, wordMapDynamic, articleMapDynamic
- sans ma&icirc;tre : lsDynEngine

\subsection smutexportee Portee de la protection
- wordIndexDynamic::syncMutex : wordIndexDynamic::extension (tampon de communication entre wordIndexDynamic et articleIndexDynamic), interdiction de lecture et &eacute;criture.
- wordIndexDynamic, articleIndexDynamic, wordMapDynamic, articleMapDynamic : tous les champs de l'objet, y compris les listes (compactList), interdiction de lecture et &eacute;criture.
- lsDynEngine : Le blocage de lsDynEngine interdit l'&eacute;criture sur tous les objets lui appartenant, y compris les pr&eacute;c&eacute;dents. Il n'interdit aucune op&eacute;ration de lecture. Son blocage intervient lors de la sauvegarde et de la fermeture.


**/
