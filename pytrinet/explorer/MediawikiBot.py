import random
import re
from threading import Thread
import threading
import time
import irc.bot

class MediawikiBot(irc.bot.SingleServerIRCBot):

    def __init__(self):
        self.generateRandomNick()
        self.rowSearch = re.compile('\[\[\x0307([^\]]+)\x0314\]\]')
        self.targetSearch = re.compile('#([a-z]+)\.([a-z]+)')
        self._channels = []
        self.welcomed = False
        self.listeners = {}
        self.lock = threading.Lock()
        irc.bot.SingleServerIRCBot.__init__(self, [('irc.wikimedia.org', 6667)], self.nickname, 'https://archive.wikiwix.com/trinet/#wikipedia/')

    def addProject(self,lang,project):
        print('Adding project {}.{}'.format(lang,project))
        channel = '#{}.{}'.format(lang,project)
        if not self.welcomed :
            self._channels.append(channel)
        else :
            self.connection.join(channel)

    def startListening( self, lang, project, listener ) :
        with self.lock :
            self.addProject(lang,project)
            target = '#{}.{}'.format(lang,project)
            listeners = self.listeners.get( target, [] )
            listeners.append(listener)
            self.listeners[target] = listeners

    def generateRandomNick(self) :
        self.nickname = 'wikiwix_'+str(random.randint(100,999))

    def on_welcome(self, c, e):
        self.welcomed = True
        for channel in self._channels :
            c.join(channel)

    def on_pubmsg(self, c, e):
        target = e.target
        row = e.arguments[0]
        res = self.rowSearch.search(row)
        title = res.group(1) if res else None
        if title:
            self.push(target,title)
    
    def push(self,target,title):
        listeners = None
        with self.lock :
            listeners = self.listeners.get(target,[])
        for listener in listeners :
            getattr(listener,'ircPush')(title)

    staticLock = threading.Lock()
    instance = False

    @staticmethod
    def getInstance():
        with MediawikiBot.staticLock:
            if not MediawikiBot.instance :
                MediawikiBot.instance = MediawikiBot()
                thread = Thread(target=MediawikiBot.start, args=(MediawikiBot.instance,))
                thread.start()

        return MediawikiBot.instance

def main():
    bot = MediawikiBot()
    bot.addProject('fr','wikipedia')
    thread = Thread(target=MediawikiBot.start, args=(bot,))
    thread.start()
    time.sleep(5)
    bot.addProject('de','wikipedia')
    time.sleep(5)
    bot.addProject('en','wikipedia')
    time.sleep(500)


if __name__ == "__main__":
    main()
