PyTrinet is a Python library that encapsulates the C++ core of the search engine Trinet. It also comes with a set of python classes 
intending to help manage the organization of core search engines, and other tasks like crawling, preparing data, storing meta-data with documents, providing an API to interact with engine arrays ...

Copyright (C) 2007-2022 Linterweb (France)

PyTrinet is licensed under the DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE. See section Licence.

PyTrinet is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

== What is Trinet ==

Trinet is a highly efficient search engine developed by Linterweb in C++, approximately at the same time Lucene was developed. 
It relies on a classical inverted list index weighted by tf/idf. Its main assets are:
* Highly compressed RAM indexes. Millions of pages can be indexed on a few GB of RAM.
At the time trinet (wikiwix.com) was a major search engine in Wikipedia, we were able to index the whole content of Wikipedia and Wikimedia projects on one or two servers (late 2000's servers).
* Very fast reply with very low CPU usage. On the same late 2000's servers, we used to reply to more than 20 queries per seconds.
* A Request-extension algorithm that improves the search engine accuracy, and can be used to help user choose between different meanings of his query.

== Install ==

Run ''python3 setup.py build'' to build or ''python3 setup.py install'' to install the PyTrinet Python extension on your system.

== Launch the API ==

 python3 api.py --path=/path_to_your_trinet_directory

The API is then listening to port 5050

== Proxying ==

You may want to use apache to serve as a proxy to reach the API.

Install proxy modules:

```
sudo a2enmod proxy
sudo a2enmod proxy_http
```

In the virtual host configuration (or directory section), add:

```
ProxyPass "/trinet"  "http://127.0.0.1:5050"
ProxyPassReverse "/trinet"  "http://127.0.0.1:5050"
```
Then the API will be accessible at your_site/trinet.

== Licence ==

        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.




