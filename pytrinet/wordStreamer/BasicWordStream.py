from curses.ascii import isalnum
import io
from nltk.tokenize import RegexpTokenizer
from typing import Callable
from html.parser import HTMLParser
from nltk.stem.snowball import *
from pytrinet.wordStreamer.WikiwixArchiveTokenizer import WikiwixArchiveTokenizer
import unidecode
import html

from pytrinet.wordStreamer.WordStreamer import WordStreamer, WordStream
from pytrinet.wordStreamer.LangDetectWordStream import LangDetectWordStream

import trafilatura

class FilterLength:

    def __init__( self, maxLength, minLength=0 ):
        self.maxLength = maxLength
        self.minLength = minLength

    def __call__( self, word: str ):
        return word if ( len(word) < self.maxLength ) and ( self.minLength <= len(word) ) else False

# Removes words matching a regex

class FilterRegex:

    def __init__( self, regex ):
        self.regex = re.compile(regex)

    def __call__( self, word: str ):
        return False if self.regex.match(word) else word

class BasicWordStreamer(WordStreamer):

    parameters = (('minLength',2),('maxLength',24),('tokenizer','html'),('langDetectLength',256),('filterRegex','.*[0-9]{3,}.*|.*[0-9][^0-9]+[0-9].*'))
    # stemming should be in
    # ('arabic', 'danish', 'dutch', 'english', 'finnish', 'french', 'german', 'hungarian', 'italian', 'norwegian', 'porter', 'portuguese', 
    # 'romanian', 'russian', 'spanish', 'swedish')

    langToStemLang = {
        'ar':'arabic',
        'da':'danish',
        'nl':'dutch',
        'en':'english',
        'fi':'finnish',
        'fr':'french',
        'de':'german',
        'hu':'hungarian',
        'it':'italian',
        'no':'norwegian',
        'pt':'portuguese',
        'ro':'romanian',
        'ru':'russian',
        'es':'spanish',
        'sv':'swedish'
    }

    def __init__( self, config ):
        super().__init__( config )

    def getWordStream( self, text: io.TextIOBase, isPhrase: bool = False, forceLang: str = None ) -> WordStream:

        stream = TokenizerWordStream( text, 'phrase' if isPhrase else self.tokenizer )
        if self.minLength or self.maxLength :
            stream.addFilter( FilterLength( self.maxLength, self.minLength ) )
        if self.filterRegex :
            stream.addFilter( FilterRegex( self.filterRegex ) )
        if not forceLang :
            stream = LangDetectWordStream( stream, self.langDetectLength )
            lang = stream.lang
        else:
            lang = forceLang
        stemLang = self.langToStemLang.get(lang)
        if not stemLang : stemLang = 'porter'
        return ( StemWordStream( stream, stemLang ), lang )


class HtmlTokenizer(HTMLParser):

    def __init__( self ):
        self.score = 0
        self.words = []
        self.tagLevels = {'script':0,'body':0,'p':0,'title':0,'style':0}
        self.tokenizer = RegexpTokenizer(r'[^\W_]+')
        super().__init__()

    def handle_starttag(self, tag, attrs):
        if tag in self.tagLevels :
            self.tagLevels[tag] += 1

    def handle_endtag(self, tag):
        if tag in self.tagLevels :
            self.tagLevels[tag] -= 1

    def handle_startendtag(self, tag, attrs):
        pass

    def handle_data(self, data):
        if self.tagLevels['script'] : self.score = 0
        elif self.tagLevels['style'] : self.score = 0
        elif self.tagLevels['title'] : self.score = -1 # Special score for title words
        elif self.tagLevels['body'] : 
            self.score = 1
            if self.tagLevels['p'] : self.score = 3

        if self.score :
            for token in self.tokenizer.tokenize(data) :
                self.words.append( (token,self.score) )

    def tokenize( self, text: str, flush: bool ): # flush means we're done, no more text is coming afterwards
        self.words = []
        self.feed(text)
        return self.words

class PhraseTokenizer :

    def __init__( self ):
        self.words = []
        self.tokenizer = RegexpTokenizer(r'[^\W_]+')
        self.tail = ''
    
    def tokenize( self, text: str, flush: bool ): # flush means we're done, no more text is coming afterwards
        self.words = []
        text = self.tail + text
        self.tail = ''
        if not flush : # If we expect more text, put the tail of text (part that touches the end and is a word, that hence might be incomplete) into self.tail
            i = len(text)-1
            while i>=0 and text[i].isalnum() : 
                i -= 1
            if i>=0 :
                self.tail = text[ i : len(text) ]
                text = text[ 0 : i ]
        for token in self.tokenizer.tokenize(text) :
            self.words.append( (token,1) )
        return self.words

class TokenizerWordStream(WordStream):

    tokenizers = {'html':HtmlTokenizer,'phrase':PhraseTokenizer,'wikiwix_archive':WikiwixArchiveTokenizer,'trafilatura':PhraseTokenizer}
    
    def __init__( self, text: io.TextIOBase, tokenizer: str ):
        self.chunk = 32*1024
        self.text = text
        self.words = []
        self.curs = 0
        self.tokenizer = self.tokenizers[tokenizer]()
        self.filters = []
        self.titleWords = False

        if tokenizer == 'trafilatura' :
            
            raw = self.text.readall()

            m = re.search(r'<\s*title\s*>([^<]+)<\s*/\s*title\s*>',raw)
            if m : 
                self.titleWords = map( lambda word: (unidecode.unidecode(word[0]),-1) , self.tokenizer.tokenize( html.unescape(m.group(1)), True ) )
                      
            content = trafilatura.extract( raw )
            self.text = io.StringIO( content )

        self.readSome()

    def addFilter( self, filter: Callable[[str],str] ):
        self.filters.append( filter )

    def readSome( self ) :
        while True:
            line = self.text.read( self.chunk )
            if not line:
                line = ''
            toContinue = len(line) == self.chunk
            self.words = self.tokenizer.tokenize(line, not toContinue)
            self.curs = 0
            if ( len(self.words) or not toContinue ): break

    def __call__( self ) -> tuple:

        if self.titleWords :
            try:
                weightedWord = next(self.titleWords)
                print(weightedWord)
            except StopIteration:
                self.titleWords = False

        while True:
            if self.curs >= len(self.words):
                self.readSome()
            if self.curs >= len(self.words):
                return None
            scored_word = self.words[self.curs]
            word = scored_word[0]
            self.curs += 1
            for filter in self.filters:
                word = filter(word)
                if not word:
                    break
            if word:
                word = unidecode.unidecode(word)
                return (word,scored_word[1])

class StemWordStream(WordStream) :

    def languages():
        return SnowballStemmer.languages

    def __init__( self, sourceStream: WordStream, language: str ) :
        self.stemmer = SnowballStemmer( language )
        self.sourceStream = sourceStream

    def __call__( self ):
        token = self.sourceStream()
        return ( self.stemmer.stem(token[0]), token[1] ) if token else None
