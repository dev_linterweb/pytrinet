//#include <boost/asio/local/stream_protocol.hpp>
#include <boost/asio.hpp>
#include <iostream>

using namespace boost::asio;

static io_context context;

std::string read_(local::stream_protocol::socket & socket) {
       streambuf buf;
       read_until( socket, buf, "\n" );
       std::string data = buffer_cast<const char*>(buf.data());
       return data;
}
void send_(local::stream_protocol::socket & socket, const std::string& message) {
       const std::string msg = message + "\n";
       boost::asio::write( socket, boost::asio::buffer(message) );
}

int main() {

    ::unlink("/tmp/foobar"); // Remove previous binding.
    local::stream_protocol::endpoint ep("/tmp/foobar");
    local::stream_protocol::acceptor acceptor(context, ep);

    while ( true ) {
        local::stream_protocol::socket socket(context);
        acceptor.accept(socket);

        std::string message = read_(socket);
        std::cout << message << std::endl;

        send_(socket, "Hello From Server!");
    }

    return 0;
}