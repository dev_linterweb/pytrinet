import React from 'react';

class ControlledInput extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {
        const container = this.props.container
        const property = this.props.property
        const originValue = this.props.originProperty ? container.state[this.props.originProperty] : false
        const onSubmit = this.props.onSubmit

        let saveButton = this.props.withSaveButton ? 
            <img src="../client/img/arrow-right-s-fill.png"
            style={{display:(this.props.originProperty&&(originValue==container.state[property]))?'none':'inline'}}
            onClick={onSubmit}/> : ''

        return <div className="controlled_input">
            <input 
                type={this.props.type} 
                value={container.state[property]} 
                name={property} 
                placeholder={this.props.placeholder}
                onChange={event=>{
                    container.setState({[property]:event.target.value})
                }}
                onKeyUp={e=>{
                    if (e.key === 'Enter' || e.keyCode === 13) {
                        if ( onSubmit ) onSubmit()
                    }
            }}/>{saveButton}</div>
    }
}

export default ControlledInput;